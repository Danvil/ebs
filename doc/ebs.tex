\documentclass[11pt,a4paper]{article}

\usepackage{pslatex,palatino,avant,graphicx,color}
\usepackage[margin=2cm]{geometry}
\usepackage{amsmath,amsfonts}

\begin{document}
\title{Event-based stereo matching}
\author{David Weikersdorfer}
\date{2014/2015}
\maketitle

\section{Function documentation}


\subsection{General remarks}

A few conventions are used in the following:
\begin{itemize}
	\item A \"patch\" is a rectangular 2D lattice $L = \{1,\ldots,n\} \times \{1,\ldots,m\}$. In most cases $n=m$, and both are small, i.e. 3 to 7.
	\item A location $(i,j) \in L$ on the lattice is called a \"pixel\".
	\item Coordinates $(u,v)$ indicate a location relative to the center of the patch: $u = j - \tfrac{m}{2}$ and $v = i - \tfrac{n}{2}$
	\item $t_0$ indicates the time at the center of the patch. We assume that this is also the largest time, as computations are always executed for the latest event.
\end{itemize}


\subsection{ComputeEventFlow}

This function computes the \"event flow\" for an event.
The event flow is the gradient of the event time field and describes how fast event time changes in screen coordinates.
A large gradient indicates slow event generation as a large time interval elapses until an event is generated in a neighbouring pixel.
On the other hand a small gradient indicates fast event generation.

If a an event happens at pixel $p_1 \in \mathbb{R}^2$ at time $t_1$ and the event-flow at that location is $f \in \mathbb{R}^2$, than the next event at a neighbouring location $p_2 \in \mathbb{R}^2$ is expected to appear at time $t_2 = t_1 + f \circ (p_2 - p_1)$.

The computation of the gradient is challenged by the fact that most of the times only around half of the patch should be considered for flow computation. Consider an edge which moves from left to right over the screen.
Only event times on pixels on the left side of the edge should be considered for flow computation.
Events time on the right side are from older motions and have nothing to do with the current motion. 

This function computes the gradient of the time field using a weighted plane fit over a small patch in the event time field.
It is currently not clear what the best method for weight computation is. Possibilities:
\begin{itemize}
	\item Choose only the pixel with the most recent time stamp to compute the flow (see Conradt et al.).
	\item Compute the median time difference over the patch and select the half of pixels with lower time differences.
	\item  For each pixel compute a weight based on the (previously computed) flow of that pixel:
\begin{equation}
	w(p) = \mathcal{N}\left(t(p) - t_0 \,|\, f(p) \circ (p - p_0), \sigma\right)
\end{equation}
Here $\mathcal{N}(\cdot|\mu,\sigma)$ is a normal distribution with mean $\mu$ and standard deviation $\sigma$.
\end{itemize}

Currently the third method is used.


\subsection{WeightedPlaneFitLattice}

This section describes how a plane is fitted through a weighted height map defined over a patch.

Assume a value function $v : L \rightarrow \mathbb{R}$ and a weight function $w : L \rightarrow \mathbb{R}^{+}$.
We would like to fit a plane of the form:
\begin{equation}
	f : L \rightarrow \mathbb{R}, \; (i,j) \mapsto a + b\,u + c\,v
\end{equation}
In a weighted, least-squares fashion the error function is:
\begin{equation}
	E(a,b,c) = \sum_{i,j \in L}{ w(i,j) \left\| f(ij|a,b,c) - v(ij) \right\|^2}
\end{equation}
The gradient of $E$ with respect to $(a,b,c)$ is:
\begin{equation}
      \nabla E(a,b,c) = 2 \sum_{i,j \in L}{ w(i,j) \, \left(a + b\,u + c\,v - v(i,j)\right) \, \begin{pmatrix} 1 \\ i \\ j \end{pmatrix} }
\end{equation}
Setting $\nabla E=0$ results in a system of linear equations $A \, (a,b,c)^T = b$ with
\begin{equation}
A = \sum_{i,j \in L}{ w(i,j)
	\begin{pmatrix}
	1 & u & v \\
	u & u^2 & u v \\
	v & u v & v^2
	\end{pmatrix}}
\text{ and }
b = \sum_{i,j \in L}{ w(i,j) \, v(i,j) \begin{pmatrix} 1 \\ u \\ v \end{pmatrix}}
\end{equation}
Solving the linear system gives the parameters of the desired plane fit.


\subsection{ComputeDisparity}

First the patch is tested with the Inclination function for good vertical features.
If not, disparity is not computed as it would be highly ambiguous.

If the test passes the patch is slided horizontally over the event field of the other camera.
For each possible horizontal distance the patch error is computed and the distance with smallest patch error is returned as the disparity.

Additionally the confidence of the match is computed as
\begin{equation}
	c = \frac{\exp(-E_\text{min}) - \exp(-E_\text{max})}{\lambda_\text{parity} \lambda_\text{time}} 
\end{equation}
$E_\text{min}$ resp. $E_\text{max}$ is the minimal and maximal patch error.
$\lambda_\text{parity}$ resp. $\lambda_\text{time}$ constant correction factors.

Details: Theses correction factors are the maximal probabilities which could be achieved if a certain percentage of pixels have random parity resp. time values.
This assumption is again justified by the fact that in the most common case events are generated along edges.
Theses edges move from one side to the other and all pixels which are on the other side of the edge can not be used for patch matching.


\subsection{PatchError}

Here to patches are compared and the matching error between them is computed.
The error function is the sum of the time and the parity error over all pixels.
The time error is the square difference between time values normalized wrt to an expected delta time.
The parity error is 0 if parities are equal and 1 otherwise.


\subsection{Inclination}

For disparity computation a patch from the left image is slided horizontally over the right image and the offset where the comparison error is smallest gives the disparity.
This only works if patches have distinct vertical features.

This function fits a line through all points which are near the time of the center event and measures its angle relative to the horizontal axis.

First each pixel in the patch is weighted with
\begin{equation}
	w(i,j) = 1 - \min\left(\frac{t_0 - t(i,j)}{T}, 1\right)
\end{equation}
to select pixels with recent events.
Then a principal component analysis of the weighted pixel locations is computed.
The eigenvector of the largest eigenvalue of the matrix
\begin{equation}
	\sum_{i,j \in L} w(i,j) \begin{pmatrix}
		u^2 & u\,v \\
		u\,v & v^2
	\end{pmatrix}
\end{equation}
denotes the dominant axis of the feature.

Remark: The currently used weighting scheme does not have a real justification -- perhaps a more intuitive method can be found.
Additionally the test should be enhanced by using a technique similar to Lowe's feature transform to better deal with cases where both eigenvalues are similar.


\end{document}
