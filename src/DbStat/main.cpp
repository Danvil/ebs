#include <Edvs/EventIO.hpp>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <boost/progress.hpp>
#include <iostream>
#include <fstream>
#include <map>
#include <string>

struct Histogram
{
	float bin_width;
	std::map<float,unsigned> data;
	Histogram(float bw=1.0f)
	: bin_width(bw) {}
	void push(float a)
	{
		data[std::floor(a / bin_width) * bin_width]++;
	}
	void push(const Histogram& h)
	{
		for(const auto& p : h.data) {
			data[p.first] += p.second;
		}
	}
};

struct Statistics
{
	std::map<std::string, Histogram> data;
	void add(const Statistics& s)
	{
		for(auto& p : s.data) {
			data[p.first].push(p.second);
		}
	}
};

struct Event
{
	uint64_t t;
	float x, y;
	uint8_t polarity;
	uint8_t id;
	float depth;
	float disparity;
};

std::vector<Event> Load(const std::string& fn)
{
	std::vector<Event> u;
	u.reserve(1000000);
	std::ifstream ifs(fn);
	while(true) {
		Event e;
		ifs >> e.t >> e.x >> e.y >> e.polarity >> e.id >> e.depth >> e.disparity;
		if(!ifs.eof()) {
			u.push_back(e);
		}
		else {
			break;
		}
	}
	return u;
}

Statistics Gather(const std::vector<Event>& u)
{
	// prepare
	Histogram events_per_sec = Histogram(10000.0f);
	Histogram depth = Histogram(0.05f);
	Histogram length_sec = Histogram(2.0f);
	Histogram length_events = Histogram(250000.0f);
	Histogram invalid_percent = Histogram(2.5f);
	// gather
	unsigned num_invalid = 0;
	uint64_t sect = u.front().t;
	unsigned num_sect = 0;
	for(const auto& e : u) {
		if(e.depth == 0) {
			num_invalid ++;
		}
		else {
			depth.push(e.depth);
		}
		num_sect ++;
		if(e.t > sect + 1000000) {
			sect = e.t;
			events_per_sec.push(num_sect);
			num_sect = 0;
		}
	}
	length_sec.push((u.back().t - u.front().t)/1000000.0f);
	length_events.push(u.size());
	invalid_percent.push(100.0f*static_cast<float>(num_invalid)/u.size());
	// return
	Statistics stats;
	stats.data["eventsPerSec"] = events_per_sec;
	stats.data["depth"] = depth;
	stats.data["lengthSec"] = length_sec;
	stats.data["lengthEvents"] = length_events;
	stats.data["invalidPercent"] = invalid_percent;
	return stats;
}

std::vector<std::string> iterate_over_directories(const boost::filesystem::path& dir_path)
{
	using namespace boost::filesystem;
	std::vector<std::string> dirs;
	if(exists(dir_path)) {
		directory_iterator end_itr; // default construction yields past-the-end
		for(directory_iterator itr(dir_path); itr!=end_itr; ++itr) {
			if(is_directory(itr->status())) {
				//... here you have a directory
				std::cout << *itr << std::endl;
				dirs.push_back(itr->path().string());
			}
		}
	}
	return dirs;
}

std::ostream& operator<<(std::ostream& os, const Histogram& h)
{
	os << std::fixed;
	for(auto it=h.data.begin(); it!=h.data.end(); ++it) {
		os << it->first << " -> " << it->second;
		if(std::next(it) != h.data.end()) {
			os << ", ";
		}
	}
	return os;
}

std::ostream& operator<<(std::ostream& os, const Statistics& s)
{
	for(const auto& p : s.data) {
		os << p.first << " = {" << p.second << "};" << std::endl;
	}
	return os;
}

int main(int argc, char *argv[])
{
	std::vector<std::string> p_dirs = {"/home/david/Desktop/ebcc_ds/20140708"};
	std::string p_stat = "/tmp/stats.txt";

	namespace po = boost::program_options;
	// Declare the supported options.
	po::options_description desc("Allowed options");
	desc.add_options()
		("help", "produce help message")
		("dirs", po::value(&p_dirs)->multitoken(), "db dir")
		("stat", po::value(&p_stat), "out stat")
	;

	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);

	if(vm.count("help")) {
		std::cout << desc << std::endl;
		return 0;
	}

	Statistics stats;
	for(const std::string& dir : p_dirs) {
		std::cout << "Directory: " << dir << std::endl;
		auto takes = iterate_over_directories(dir);
		boost::progress_display show_progress(takes.size());
	  	for(const std::string& d : takes) {
	//		std::cout << "Processing '" << d << "'..." << std::endl;
			auto s = Gather(Load(d + "/events-gt.tsv"));
			stats.add(s);
			++show_progress;
		}
	}
	std::ofstream ofs(p_stat);
	ofs << stats;
	std::cout << stats << std::endl;

	return 0;
}
