#include "WdgtEbsLive.h"
#include <Edvs/EventIO.hpp>
#include <QtGui>
#include <QApplication>
#include <boost/program_options.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <iostream>

int main(int argc, char *argv[])
{
	std::vector<std::string> p_vuri;///dev/ttyUSB0?baudrate=4000000";
	std::string p_props_fn;
	bool p_silent = false;
	std::string p_fn_out;

	// get command line parameters
	namespace po = boost::program_options;
	po::options_description desc("Allowed options");
	desc.add_options()
		("help", "produce help message")
		("uris", po::value(&p_vuri)->multitoken(), "URI(s) to event stream(s)")
		("props", po::value(&p_props_fn), "Filename to ebs algorithm options")
		("silent", po::value(&p_silent), "Enable console mode without gui")
		("out", po::value(&p_fn_out), "Name of file where to write computed event disparities")
	;
	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);
	if(vm.count("help")) {
		std::cout << desc << std::endl;
		return 1;
	}

	auto translator = ebs::CreatePropertyTranslator();

	// load events
	auto stream = Edvs::OpenEventStream(p_vuri);

	// properties
	ebs::Properties props = ebs::DefaultProperties();
	if(!p_props_fn.empty()) {
		boost::property_tree::ptree pt;
		boost::property_tree::read_xml(p_props_fn, pt);
		props = translator.fromPropertyTree(pt);
		std::cout << "Read properties from file '" << p_props_fn << "': " << std::endl;
	}
	else {
		std::cout << "Using default properties: " << std::endl;
	}
	translator.print(std::cout, props);
	std::cout << std::endl;

	if(p_silent) {
		ebs::Core core;
		core.properties() = props;
		core.SetDisparityComputation(true);
		core.SetEbsEventTransformation(stream->is_live());
		while(!stream->eos()) {
			core.pushEvents(stream->read());
		}
		std::ofstream ofs(p_fn_out);
		std::cout << core.events().size() << " / " << core.stereoEvents().size() << std::endl;
		for(const auto& e : core.stereoEvents()) {
			if(e.valid) {
				ofs << e.disparity << "\n";
			}
			else {
				ofs << "0" << "\n";
			}
		}
		return 0;
	}
	else {

		// start gui
		QApplication a(argc, argv);
		WdgtEbsLive w(stream, props);
		w.show();
		int res = a.exec();
		// save properties
		{
			boost::property_tree::ptree pt = translator.toPropertyTree(w.properties());
			const std::string fn = "/tmp/ebs_props.xml";
			boost::property_tree::write_xml(fn, pt);
			std::cout << "Final properties written to '" << fn << "'" << std::endl;
		}
		return res;
	}
}
