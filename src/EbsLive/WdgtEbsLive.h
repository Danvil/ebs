#ifndef EBSVIS_WDGTEBSLIVE_H
#define EBSVIS_WDGTEBSLIVE_H

#include "ui_WdgtEbsLive.h"
#include <ebs/Core.hpp>
#include <ebs/Visualization.hpp>
#include <qs/VisualizationPipeline.hpp>
#include <Edvs/EventStream.hpp>
#include <da/synchronized.hpp>
#include <da/PropertyWidget.hpp>
#include <QtGui/QWidget>
#include <QtGui/QImage>
#include <QtCore/QTimer>
#include <memory>

class WdgtEbsLive : public QWidget
{
	Q_OBJECT

public:
	WdgtEbsLive(const std::shared_ptr<Edvs::IEventStream>& stream, const ebs::Properties& props, QWidget* parent = 0);
	~WdgtEbsLive();

	ebs::Properties properties() const
	{ return props_wdgt_->properties(); }

	void closeEvent(QCloseEvent* event);

protected Q_SLOTS:
	void TickCore();
	void TickVis();

private:
	void OnParameterChanged(const ebs::Option& key, const boost::any& value);

private:
	Ui::WdgtEbsLiveClass ui;

	da::synchronized<ebs::Core> core_;

	std::shared_ptr<Edvs::IEventStream> stream_;

	QTimer timer_core_;
	QTimer timer_vis_;

	std::unique_ptr<da::PropertyWidget<ebs::Option,ebs::Properties>> props_wdgt_;
	da::PropertyTranslator<ebs::Option,ebs::Properties> props_translator_;

	std::unique_ptr<VisualizationPipeline> ebs_gui_vis_;

	std::unique_ptr<QWidget> gl_wdgt_;
};

#endif
