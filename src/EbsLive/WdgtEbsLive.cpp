#include <ebs/Disparity.hpp>
#include <ebs/Calibration.hpp>
#include <ebs/QtHelpers.hpp>
#include "WdgtEbsLive.h"
#include <QPainter>
#include <QMouseEvent>
#include <slimage/qt.hpp>
#include <slimage/io.hpp>
#ifndef MINIMAL_MODE
	#include <Candy/System/GLSystemQtWindow.h>
	#include <Candy.h>
#endif
#include <sstream>

constexpr unsigned UPDATE_DT_CORE = 0;
constexpr unsigned UPDATE_DT_VIS = 50;
constexpr bool SHOW_3D_POINTS = true;
constexpr uint64_t VIS_DELTA = 200000;
constexpr uint64_t VIS_RENDER_DELTA = 200000;

std::unique_ptr<ebs::Visualization> g_vis_;
bool g_vis_running_;
std::thread g_vis_thread_;

#ifndef MINIMAL_MODE
void Render(const da::locked_ptr<ebs::Core>& core)
{
	std::vector<ebs::StereoEvent> ves;
	{
		ves = ebs::CloneEventRange(core->stereoEvents(), VIS_RENDER_DELTA);
	}
	glBegin(GL_POINTS);
	for(const ebs::StereoEvent& se : ves) {
		if(se.id == 0) {
			Eigen::Vector3f p = ebs::Triangulize(se);
			glVertex3f(p[0],p[1],p[2]);
		}
	}
	glEnd();
}
#endif

WdgtEbsLive::WdgtEbsLive(const std::shared_ptr<Edvs::IEventStream>& stream, const ebs::Properties& props, QWidget* parent)
:	stream_(stream)
{
	ui.setupUi(this);

	core_.lock()->SetDisparityComputation(true);
	core_.lock()->SetEbsEventTransformation(stream->is_live());

	props_translator_ = ebs::CreatePropertyTranslator();

	props_wdgt_.reset(new std::decay<decltype(*props_wdgt_)>::type{});
	props_wdgt_->setTranslator(props_translator_);

	// spec begin
	
	props_wdgt_->specifyInteger(ebs::Option::GradientPatchRadius,
		"Patch radius for flow computation", 1, 9);
	props_wdgt_->specifyReal(ebs::Option::GradientTimeSigma,
		"Gradient time tolerance sigma", 0.01, 1.00);

	props_wdgt_->specifyInteger(ebs::Option::LocalDisparityPatchRadius,
		"Patch radius for local disparity computation", 1, 9);
	props_wdgt_->specifyInteger(ebs::Option::LocalDisparityMax,
		"Maximum checked disparity", 1, 99);
	props_wdgt_->specifyReal(ebs::Option::LocalDisparityInclinationLimitCos,
		"LocalDisparityInclinationLimitCos", 0.00, 1.00);
	props_wdgt_->specifyReal(ebs::Option::LocalDisparityTimeStddev,
		"LocalDisparityTimeStddev", 0.00, 1.00);
	props_wdgt_->specifyReal(ebs::Option::LocalDisparityTimeStddevMultMax,
		"LocalDisparityTimeStddevMultMax", 1.00, 9.00);
	props_wdgt_->specifyReal(ebs::Option::LocalDisparityParityErrorProp,
		"LocalDisparityParityErrorProp", 0.00, 1.00);
	props_wdgt_->specifyReal(ebs::Option::LocalDisparityPatchTrashShare,
		"LocalDisparityPatchTrashShare", 0.00, 1.00);
	
	props_wdgt_->specifyInteger(ebs::Option::DisparityGlobalPatchRadius,
		"Patch radius for global disparity computation", 1, 9);

	// spec end
	props_wdgt_->setPropertyCallback(
		std::bind(&WdgtEbsLive::OnParameterChanged, this, std::placeholders::_1, std::placeholders::_2));
	props_wdgt_->setProperties(props);
	props_wdgt_->show();

	QObject::connect(&timer_core_, SIGNAL(timeout()), this, SLOT(TickCore()));
	timer_core_.setInterval(UPDATE_DT_CORE);
	timer_core_.start();

	QObject::connect(&timer_vis_, SIGNAL(timeout()), this, SLOT(TickVis()));
	timer_vis_.setInterval(UPDATE_DT_VIS);
	timer_vis_.start();

	g_vis_.reset(new ebs::Visualization());
	g_vis_->add("raw 0", ui.labelLeft);
	g_vis_->add("raw 1", ui.labelRight);
	g_vis_->add("gradient_abs_inv", ui.labelGradientAbs);
	g_vis_->add("gradient_arg", ui.labelGradientDir);
	g_vis_->add("disp_local", ui.labelDisparityLocal);
	g_vis_->add("disp_global", ui.labelDisparity);
	g_vis_->add("ConfidenceLocal", ui.labelConfidenceLocal);
	g_vis_->add("TimeField 0", ui.labelTimeField0);
	g_vis_->add("TimeField 1", ui.labelTimeField1);
	g_vis_->add("GradientField", ui.labelGradientField);
	g_vis_->add("DisparityFieldLocal", ui.labelDisparityFieldLocal);
	g_vis_->add("DisparityField", ui.labelDisparityField);
	g_vis_->add("ConfidenceFieldLocal", ui.labelConfidenceFieldLocal);

	g_vis_running_ = true;
	g_vis_thread_ = std::thread([this]() {
		std::vector<Edvs::Event> events;
		std::vector<ebs::StereoEvent> stereoevents;
		ebs::EventField event_field_0, event_field_1;
		while(g_vis_running_) {
			{
				auto core = core_.lock();
				events = ebs::CloneEventRange(core->events(), VIS_DELTA);
				stereoevents = ebs::CloneEventRange(core->stereoEvents(), VIS_DELTA);
				event_field_0 = core->eventFieldLeft();
				event_field_1 = core->eventFieldRight();
			}
			g_vis_->plotEvents("raw 0", events, 0);
			g_vis_->plotEvents("raw 1", events, 1);
			g_vis_->plotStereoEvents("gradient_abs_inv", stereoevents, ebs::VisMode::GradientAbsInv);
			g_vis_->plotStereoEvents("gradient_arg", stereoevents, ebs::VisMode::Gradient);
			g_vis_->plotStereoEvents("disp_local", stereoevents, ebs::VisMode::DisparityLocal);
			g_vis_->plotStereoEvents("disp_global", stereoevents, ebs::VisMode::DisparityGlobal);
			g_vis_->plotStereoEvents("ConfidenceLocal", stereoevents, ebs::VisMode::ConfidenceLocal);
			g_vis_->plotEventField("TimeField 0", event_field_0, ebs::VisMode::Time);
			g_vis_->plotEventField("TimeField 1", event_field_1, ebs::VisMode::Time);
			g_vis_->plotEventField("GradientField", event_field_0, ebs::VisMode::Gradient);
			g_vis_->plotEventField("DisparityFieldLocal", event_field_0, ebs::VisMode::DisparityLocal);
			g_vis_->plotEventField("DisparityField", event_field_0, ebs::VisMode::DisparityGlobal);
			g_vis_->plotEventField("ConfidenceFieldLocal", event_field_0, ebs::VisMode::ConfidenceLocal);
			std::this_thread::sleep_for(std::chrono::milliseconds(40));
		}
	});

#ifndef MINIMAL_MODE
	if(SHOW_3D_POINTS) {
		// CANDY

		PTR(Candy::View) view_;
		PTR(Candy::Scene) scene_;
		PTR(Candy::Engine) engine_;
		Candy::GLSystemQtWindow* gl_wdgt;

		std::cout << "Creating OpenGL Widget ..." << std::endl;

		view_ = Candy::View::FactorDefaultPerspectiveView();
		scene_ = view_->getScene();

		boost::shared_ptr<Candy::DirectionalLight> light1(new Candy::DirectionalLight(Candy::Vec3f(+1.0f, +1.0f, -1.0f)));
		light1->setDiffuse(Candy::Colorf(1.0f, 1.0f, 1.0f));
		scene_->addLight(light1);
		boost::shared_ptr<Candy::DirectionalLight> light2(new Candy::DirectionalLight(Candy::Vec3f(-1.0f, -1.0f, -1.0f)));
		light2->setDiffuse(Candy::Colorf(1.0f, 1.0f, 1.0f));
		scene_->addLight(light2);

		engine_.reset(new Candy::Engine(view_));
		engine_->setClearColor(Candy::Colorf::Grey);

		scene_->setShowCoordinateCross(true);

		gl_wdgt = new Candy::GLSystemQtWindow(0, engine_);
		gl_wdgt->setAutoUpdateInterval(20);
		gl_wdgt->show();
		//miv_->addCustomWidget(gl_wdgt, "3D");
		gl_wdgt_.reset(gl_wdgt);

		boost::shared_ptr<Candy::IRenderable> dasp_renderling(
				new Candy::ObjectRenderling([this](){ Render(core_.lock()); }));
		scene_->addItem(dasp_renderling);

		// CANDY
	}
#endif

}

WdgtEbsLive::~WdgtEbsLive()
{
	g_vis_running_ = false;
	g_vis_thread_.join();
}

void WdgtEbsLive::closeEvent(QCloseEvent* event)
{
	props_wdgt_->close();
	gl_wdgt_->close();
}

void WdgtEbsLive::TickCore()
{
	core_.lock()->pushEvents(stream_->read());
}

void WdgtEbsLive::TickVis()
{
	g_vis_->display();
}

void WdgtEbsLive::OnParameterChanged(const ebs::Option& key, const boost::any& value)
{
	core_.lock()->properties() = props_wdgt_->properties();
//	props_translator_.print(std::cout, core_.properties());
//	std::cout << std::endl;
}
