#include <ebs/Disparity.hpp>
#include <slimage/image.hpp>
#include <Eigen/Dense>
#include <iostream>
#include <vector>
#include <limits>
#include <fstream>

namespace ebs
{

inline
constexpr float Square(float x)
{ return x*x; }

template<typename Matrix>
float Inclination(const Matrix& ainp)
{
//	std::cout << "     " << std::endl;
//	std::cout << "     " << std::endl;
//	std::cout << "     " << std::endl;
	auto amax = ainp.array().maxCoeff();
	Eigen::MatrixXf a(ainp.rows(), ainp.cols());
	// conversion and center
	Eigen::Vector2f center = Eigen::Vector2f::Zero();
	float asum = 0.0f;
	for(int i=0; i<a.rows(); i++) {
		for(int j=0; j<a.cols(); j++) {
			float w = 1.0f - std::min<float>((amax - ainp(i,j)) / 0.1f, 1.0f);
			a(i,j) = w;
			center += w * Eigen::Vector2f{i,j};
			asum += w;
		}
	}
//	std::cout << a << std::endl;
//	std::cout << asum << std::endl;
	if(asum == 0.0f) {
		return 0.0f;
	}
	center /= asum;
//	std::cout << center.transpose() << std::endl;
	// principal component analysis
	float axx = 0.0f, axy = 0.0f, ayy = 0.0f;
	for(int i=0; i<a.rows(); i++) {
		for(int j=0; j<a.cols(); j++) {
			float w = a(i,j);
			float x = w*(static_cast<float>(i) - center[0]);
			float y = w*(static_cast<float>(j) - center[1]);
			axx += x*x;
			axy += x*y;
			ayy += y*y;
		}
	}
	Eigen::Matrix2f mat;
	mat << axx, axy, axy, ayy;
	mat /= asum;
	// Compute eigenvectors and return the angle of the eigenvector with larger eigenvalue
	// towards the horizontal axis.
	Eigen::SelfAdjointEigenSolver<Eigen::Matrix2f> es(mat);
	return es.eigenvectors()(0,1);
}

template<typename Matrix>
bool PatchCheckInclination(const Matrix& patch) {
	const float cosa = Inclination(patch);
	return std::abs(Inclination(patch)) < 0.3;
}

template<typename M1>
unsigned Area(const M1& m1)
{ return m1.rows()*m1.cols(); }

template<typename M1, typename M2>
float PatchError(const M1& m1, const M2& m2,
	float time_stddev, float time_stddev_mult_max_sq, float parity_factor_eq, float parity_factor_not_eq)
{
//	static std::ofstream ofs("/tmp/patcherr.tsv");
	// integrate pixel time error and compute number of pixels with equal parity
	float error_t = 0.0f;
	unsigned num_equal_parity = 0;
	for(unsigned i=0; i<m1.rows(); i++) {
		for(unsigned j=0; j<m1.cols(); j++) {
			const auto& a = m1(i,j);
			const auto& b = m2(i,j);
			float dt = std::min(a.flow.norm(), time_stddev); // TIME_STDDEV
			error_t += std::min(Square((a.time - b.time)/dt), time_stddev_mult_max_sq);
			num_equal_parity += (a.parity == b.parity);
		}
	}
//	ofs << error_t << "\t" << num_equal_parity << std::endl;
	// number of compared pixels
	unsigned area = Area(m1);
	// compute final time error
	error_t = error_t/2.0f/static_cast<float>(area);
	// compute final parity error
	// we assume that parity can be wrong with a given percentage
	float parity_perc = static_cast<float>(num_equal_parity) / static_cast<float>(area);
	float error_b = parity_perc*parity_factor_eq + (1.0f-parity_perc)*parity_factor_not_eq;
	// total error
	float error_total = error_t + error_b;
//	std::cout << num_equal_parity << "/" << area << " -> " << error_b << " | " << error_t << " | " << error_total << std::endl;
//	ofs << error_t << "\t" << error_b << std::endl;
	return error_total;
}

int MinPatchError(
	const EventField& tf1, const EventField& tf2,
	int x, int y,
	const ParametersLocalDisparity& params,
	float* conf
) {
	const unsigned c_rad = params.patch_rad;
	const unsigned c_disp_max = params.disp_max;
	const float c_time_stddev = params.time_stddev;
	const float c_time_stddev_mult_max_sq = Square(params.time_stddev_mult_max);
	// we compare if parities of two pixels are equal, so we need to use the probability
	// that they are in fact not equal given we observe they are equal
	const float c_parity_err_prop_eq = 2.0f*params.parity_error_prop*(1.0f - params.parity_error_prop);
	const float c_parity_factor_eq = -std::log(1.0f - c_parity_err_prop_eq);
	const float c_parity_factor_not_eq = -std::log(c_parity_err_prop_eq);
	const float c_patch_trash_share = params.patch_trash_share;


	if(x-static_cast<int>(c_rad + c_disp_max) < 0) return -1;
	// if(x+static_cast<int>(c_rad+c_disp_max) >= tf2.cols()) return -1;
	int best_d = -1;
	float err_min = std::numeric_limits<float>::max();
	float err_max = std::numeric_limits<float>::min();
	int dmax = static_cast<int>(c_disp_max);
	for(int d=0; d<dmax; d++) {

		// // time error
		// float err_t = (tf1.patchTime(x,y,c_rad) - tf2.patchTime(x-d,y,c_rad)).array().abs().sum();

		// error
		float err = PatchError(tf1.patch(x,y,c_rad), tf2.patch(x-d,y,c_rad),
			c_time_stddev, c_time_stddev_mult_max_sq, c_parity_factor_eq, c_parity_factor_not_eq);

		// update min/max
		if(err < err_min) {
			err_min = err;
			best_d = d;
		}
		if(err > err_max) {
			err_max = err;
		}
	}
	if(conf) {
		// difference between maximal and minimal probability
		float delta = std::exp(-err_min) - std::exp(-err_max);
		// rescale to compensate for the fact that 1 and 0 are never achieved
		// maximal probability is reduced by the fact that for edges half of the pixels are trash
		// first line is the max possible probability from parity error (assuming trash)
		float pmax = (c_patch_trash_share * c_parity_err_prop_eq + (1.0f - c_patch_trash_share)*(1.0f - c_parity_err_prop_eq))
		// second line is the max possible probability from time error (assuming trash)
				   * std::exp(-0.5f*c_patch_trash_share*c_time_stddev_mult_max_sq);
		// minimal probability is almost 0
		*conf = std::min(1.0f, delta / pmax);
	}
//	std::cout << err_min << " / " << err_max << " => " << *conf << std::endl;
	return best_d;
}

template<typename M>
float MinPatchErrorSubpixel(const M& src, const EventField& tf2, int x, int y, unsigned R, unsigned disp_max) {

	throw 0; // FIXME need to change direction of search
	
	float best_d = -1;
	float best_err = 0.0f;
	int dmax = std::min<int>(x-static_cast<int>(R)+1, disp_max);
	for(int d=0; d<dmax-1; d++) {
		auto dst_a = tf2.patchTime(x-d,y,R);
		auto dst_b = tf2.patchTime(x-d-1,y,R);
		Eigen::VectorXf dsd = (src - dst_a).array();
		Eigen::VectorXf ddd = (dst_b - dst_a).array();
		float p = (dsd*ddd).sum() / ddd.array().square().sum();
		if(0 <= p && p <= 1) {
			float err = (p * ddd - dsd).array().square().sum();
			if(best_d < 0 || err < best_err) {
				best_err = err;
				best_d = static_cast<float>(d) + p;
			}
		}
	}
	return best_d;
}

float ComputeDisparity(const Edvs::Event& e, const EventField& tf1, const EventField& tf2, const ParametersLocalDisparity& params, float* conf)
{
	const unsigned RAD = params.patch_rad;
	if(e.x < RAD || tf1.cols() <= e.x+RAD || e.y < RAD || tf1.rows() <= e.y+RAD) {
		return -1;
	}
	if(std::abs(Inclination(tf1.patchTime(e.x,e.y,RAD))) < params.inclination_limit_cos) {
		return -10;
	}
	return MinPatchError(tf1, tf2, e.x, e.y, params, conf);
}

float ComputeDisparitySubpixel(const Edvs::Event& e, const EventField& tf1, const EventField& tf2, const ParametersLocalDisparity& params)
{
	const unsigned RAD = params.patch_rad;
	if(e.x < RAD || tf1.cols() <= e.x+RAD || e.y < RAD || tf1.rows() <= e.y+RAD) {
		return -1;
	}
	auto src = tf1.patchTime(e.x,e.y,RAD);
	if(std::abs(Inclination(src)) < params.inclination_limit_cos) {
		return -10;
	}
	return MinPatchErrorSubpixel(src, tf2, e.x, e.y, RAD, params.disp_max);
}

float Similarity(const EventFieldElement& center, const EventFieldElement& a, int i, int j, int RAD)
{
	if(a.disparity_local < 0) {
		return 0.0f;
	}
	const float SIGMA_POSPX = static_cast<float>(2*RAD);
	constexpr float SIGMA_TIME = 0.5f;
	constexpr float SIGMA_DISPARITY = 3.0f;
	constexpr float SIGMA_FLOW = 0.1f;
	Eigen::Vector2f delta_pos{j-RAD, i-RAD};
	float delta_time = center.time - a.time;
	float dp = delta_pos.squaredNorm() / Square(SIGMA_POSPX);
//	float dt = Square(delta_time / SIGMA_TIME);
	float dd = (center.disparity_local < 0) ? 0 : Square((a.disparity_local - center.disparity_local) / SIGMA_DISPARITY);
	float df = Square((a.flow.dot(delta_pos) - delta_time) / SIGMA_FLOW);
	return std::exp(-0.5f*(dp + dd + df)) * a.confidence_local;
}

float ComputeDisparityGlobal(const Edvs::Event& e, const EventField& tf1, unsigned RAD)
{
	const int SIZE = 2*RAD + 1;

	if(e.x < RAD || tf1.cols() <= e.x+RAD || e.y < RAD || tf1.rows() <= e.y+RAD) {
		return tf1.at(e.x,e.y).disparity_local;
	}

	auto src = tf1.patch(e.x, e.y, RAD);
	const auto& srcMid = src(RAD,RAD);

	Eigen::MatrixXf weights{SIZE,SIZE};
	iterate(src, [RAD,&srcMid,&weights](int i, int j, const EventFieldElement& a) {
		weights(i,j) = Similarity(srcMid, a, i, j, RAD);
	});
//	weights(RAD,RAD) = 1.0f;

	// if(60 <= e.x && e.x <= 68 && 60 <= e.y && e.y <= 68) {
	// 	std::cout << weights << std::endl;
	// }

	float disp_sum = 0.0f;
	float weight_sum = 0.0f;
	iterate(src, [&weights,&weight_sum,&disp_sum](int i, int j, const EventFieldElement& a) {
		float w = weights(i,j);
		weight_sum += w;
		disp_sum += w * a.disparity_local;
	});

	if(weight_sum == 0.0f) {
		return srcMid.disparity_local;
	}

	return disp_sum / weight_sum;
}

}

