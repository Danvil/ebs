#include <ebs/Core.hpp>
#include <ebs/Flow.hpp>
#include <ebs/Disparity.hpp>
#include <iostream>

namespace ebs
{
	Properties DefaultProperties()
	{
		return Properties{};
	}

	da::PropertyTranslator<Option,Properties> CreatePropertyTranslator()
	{
		da::PropertyTranslator<Option,Properties> pt;

#define ADD_PROP(X) pt.set(Option::X, #X, &Properties::X);
#define ADD_PROP_LOCDISP_I(X,Y) pt.set<int>(Option::X, #X, \
		[](const Properties& p) { return p.p_local_disp.Y; }, \
		[](Properties& p, int v) { p.p_local_disp.Y = v; });
#define ADD_PROP_LOCDISP_F(X,Y) pt.set<double>(Option::X, #X, \
		[](const Properties& p) { return p.p_local_disp.Y; }, \
		[](Properties& p, double v) { p.p_local_disp.Y = v; });

		ADD_PROP(GradientPatchRadius)
		ADD_PROP(GradientTimeSigma)

		ADD_PROP_LOCDISP_I(LocalDisparityPatchRadius, patch_rad);
		ADD_PROP_LOCDISP_I(LocalDisparityMax, disp_max);
		ADD_PROP_LOCDISP_F(LocalDisparityInclinationLimitCos, inclination_limit_cos);
		ADD_PROP_LOCDISP_F(LocalDisparityTimeStddev, time_stddev);
		ADD_PROP_LOCDISP_F(LocalDisparityTimeStddevMultMax, time_stddev_mult_max);
		ADD_PROP_LOCDISP_F(LocalDisparityParityErrorProp, parity_error_prop);
		ADD_PROP_LOCDISP_F(LocalDisparityPatchTrashShare, patch_trash_share);

		ADD_PROP(DisparityGlobalPatchRadius)

#undef ADD_PROP
		return pt;
	}

	Core::Core(int idl, int idr)
	: ebs_event_transf_(true), is_disparity_computation_(true), id_left_(idl), id_right_(idr)
	{
		properties_ = DefaultProperties();
	}

	void Core::pushEvents(const std::vector<Edvs::Event>& events)
	{
		const int FLOW_RAD = properties_.GradientPatchRadius;
		const double FLOW_SIGMA = properties_.GradientTimeSigma;
		const int DISP_GLOBAL_RAD = properties_.DisparityGlobalPatchRadius;
		// process events
		events_.reserve(events_.size() + events.size());
		std::vector<StereoEvent> vse;
		vse.reserve(events.size());
		for(Edvs::Event e : events) {
			// event transform for basic alignment
			uint16_t ex = e.x;
			uint16_t ey = e.y;
			if(ebs_event_transf_) {
				e.x = 127 - ey;
				e.y = 127 - ex;
			}
			// correct id
			if(e.id == id_left_) {
				e.id = 0;
			}
			else if(e.id == id_right_) {
				e.id = 1;
			}
			else {
				std::cerr << "Invalid event source ID" << std::endl;
				continue;
			}
			// save event
			events_.push_back(e);
			// store event in event field
			tf_[e.id].push(e);
			// compute flow/disparity (only for left eye)
			StereoEvent se;
			static_cast<Edvs::Event&>(se) = e;
			se.valid = false;
			if(e.id == 0) {
				auto& efe = tf_[0].at(e.x,e.y);
				// time field gradient
				Eigen::Vector2f flow = ComputeEventFlow(e, tf_[0], FLOW_RAD, FLOW_SIGMA);
				efe.flow = flow;
				// local disparity
				float disparity_local = 0.0f;
				float confidence_local = 0.0f;
				if(is_disparity_computation_) {
					disparity_local = ComputeDisparity(e, tf_[0], tf_[1], properties_.p_local_disp, &confidence_local);
				}
				efe.disparity_local = disparity_local;
				efe.confidence_local = confidence_local;
				// global disparity
				float disparity = 0.0f;
				if(is_disparity_computation_) {
					disparity = ComputeDisparityGlobal(e, tf_[0], DISP_GLOBAL_RAD);
				}
				efe.disparity = disparity;
				// add event
				se.gradient = flow;
				se.disparity_local = disparity_local;
				se.confidence_local = confidence_local;
				se.disparity = disparity;
				se.valid = true;
			}
			vse.push_back(se);
		}
		// compute ground truth depth
		if(depth_gt_fnc_) {
			depth_gt_fnc_(vse);
		}
		// store stereo events
		stereo_events_.insert(stereo_events_.end(), vse.begin(), vse.end());
	}

}
