#include <ebs/Calibration.hpp>
#include <Edvs/Event.hpp>
#include <slimage/algorithm.hpp>

#define SLIMAGE_IO_OPENCV // hack to get conversion functions
#include <slimage/opencv.hpp>
#include <slimage/io.hpp> // hack to get conversion functions
//#include <slimage/detail/OpenCv.hpp>

#include <opencv2/core/core.hpp>
#include <opencv2/calib3d/calib3d.hpp>

#include <Eigen/Geometry>
#include <boost/iterator/counting_iterator.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/split_free.hpp>
#include <iostream>
#include <fstream>
#include <limits>
#include <random>

#define DEBUG_EBS_CHECKERBOARD

/** Finds minimum of f over the given range. Returns value and result. */
template<typename It, typename F>
auto minimize(It begin, It end, F f) -> std::pair<decltype(begin),decltype(f(*begin))> {
	auto best = std::make_pair(begin, f(*begin));
	for(; begin!=end; ++begin) {
		auto score = f(*begin);
		if(score < best.second) {
			best = std::make_pair(begin, score);
		}
	}
	return best;
}

/** Finds maximum of f over the given range. Returns value and result. */
template<typename It, typename F>
auto maximize(It begin, It end, F f) -> std::pair<decltype(begin),decltype(f(*begin))> {
	auto best = std::make_pair(begin, f(*begin));
	for(; begin!=end; ++begin) {
		auto score = f(*begin);
		if(score > best.second) {
			best = std::make_pair(begin, score);
		}
	}
	return best;
}

/** Checks if a condition is true for any index [-R|+R]^2 */
template<typename F>
bool any_box2(unsigned r, F f)
{
	const int R = static_cast<int>(r);
	for(int i=-R; i<=+R; i++) {
		for(int j=-R; j<=+R; j++) {
			if(f(i,j)) return true;
		}
	}
	return false;
}

/** Iterates over all elements in a matrix like structure */
template<typename M, typename F>
void for_each(const M& u, F f, unsigned r=0)
{
	unsigned imax = (u.rows() < r) ? 0 : u.rows() - r;
	unsigned jmax = (u.cols() < r) ? 0 : u.cols() - r;
	for(unsigned i=r; i<imax; i++) {
		for(unsigned j=r; j<jmax; j++) {
			f(i, j, u(i,j));
		}
	}
}

/** Iterates over all elements in a matrix like structure */
template<typename M, typename F>
void for_each(M& u, F f, unsigned r=0)
{
	unsigned imax = (u.rows() < r) ? 0 : u.rows() - r;
	unsigned jmax = (u.cols() < r) ? 0 : u.cols() - r;
	for(unsigned i=r; i<imax; i++) {
		for(unsigned j=r; j<jmax; j++) {
			f(i, j, u(i,j));
		}
	}
}

// float save_acos(float v)
// {
// 	if(v >= +1.0f) return 0.0f;
// 	if(v <= -1.0f) return std::acos(-1.0f);
// 	return std::acos(v);
// }

// /** Computes the score for one angle */
// float AngleScore(const ebs::EventField& u, float angle)
// {
// 	constexpr float PiHalf = 1.570796327f;
// 	constexpr float T_TOLERANCE = 0.5f;
// 	if(u.num_events_ == 0) return 0.0f;
// 	float tnow = u.last().time;
// 	float angle2 = angle + PiHalf;
// 	Eigen::Vector2f n1{std::cos(angle ), std::sin(angle )};
// 	Eigen::Vector2f n2{std::cos(angle2), std::sin(angle2)};
// 	float v_sum = 0.0f;
// 	float w_sum = 0.0f;
// 	for_each(u,
// 		[n1,n2,tnow,&v_sum,&w_sum](unsigned i, unsigned j, const ebs::EventFieldElement& q) {
// 			const auto& flow = q.flow;
// 			float lenSq = flow.squaredNorm();
// 			if(lenSq == 0.0f) return;
// 			float s1 = std::abs(n1.dot(flow));
// 			float s2 = std::abs(n2.dot(flow));
// 			float dt = tnow - q.time; 
// 			float w = std::exp(-0.5f*dt/T_TOLERANCE);
// 			// float v = save_acos(std::max(s1,s2) / std::sqrt(lenSq));
// 			float v = std::max(s1,s2) / std::sqrt(lenSq); // almost the same as above ;)
// 			v_sum += w*v;
// 			w_sum += w;
// 		});
// 	return w_sum == 0.0f ? 0.0f : v_sum / w_sum;
// }

// float DominantRotationMaxScore(const ebs::EventField& u)
// {
// 	constexpr unsigned STEPS = 64;
// 	constexpr float PiHalf = 1.570796327f;
// 	// find angle with highest score
// 	// TODO this is primitive minimization
// 	auto res = maximize(
// 		boost::counting_iterator<int>(0),
// 		boost::counting_iterator<int>(STEPS),
// 		[&u](int k){
// 			float angle = static_cast<float>(k) / static_cast<float>(STEPS) * PiHalf;
// 			return AngleScore(u, angle);
// 		});

// #ifdef DEBUG_EBS_CHECKERBOARD
// 	{
// 		static std::ofstream ofs("/tmp/angle.tsv");
// 		std::vector<float> vals{STEPS};
// 		for(unsigned k=0; k<STEPS; k++) {
// 			float angle = static_cast<float>(k) / static_cast<float>(STEPS) * PiHalf;
// 			ofs << AngleScore(u, angle) << ", ";
// 		}
// 		ofs << std::endl;
// 	}
// #endif

// 	return *res.first;
// }

std::pair<unsigned,float> histogram_index_cyclic(float x, float max, unsigned num_bins)
{
	float p = std::fmod(x, max);
	if(p < 0) p += max;
	p /= max;
	float a = p * static_cast<float>(num_bins);
	unsigned i = static_cast<unsigned>(a);
	if(i >= num_bins) i = num_bins - 1;
	float q = a - static_cast<float>(i);
	return {i,q};
}

void histogram_add(std::vector<float>& h, float x, float max, float w)
{
	auto p = histogram_index_cyclic(x, max, h.size());
	h[p.first] += (1.0f - p.second) * w;
	h[(p.first + 1)%h.size()] += p.second * w;
}

float DominantRotationHistogram(const ebs::EventField& u)
{
	constexpr float PiHalf = 1.570796327f;
	constexpr unsigned BINS = 64;
	constexpr float T_TOLERANCE = 0.3f;
	if(u.num_events_ == 0) return 0.0f;
	float tnow = u.last().time;
	std::vector<float> bins(BINS, 0);	
	for_each(u,
		[&bins,tnow](unsigned i, unsigned j, const ebs::EventFieldElement& q) {
			float fx = q.flow[0];
			float fy = q.flow[1];
			if(fx == 0.0f && fy == 0.0f) return;
			float arg = std::atan2(fy, fx);
			float dt = tnow - q.time; 
			float w = std::exp(-0.5f*dt/T_TOLERANCE);
			histogram_add(bins, arg, PiHalf, w);
		});

#ifdef DEBUG_EBS_CHECKERBOARD
	{
		static std::ofstream ofs("/tmp/angle.tsv");
		for(auto q : bins) {
			ofs << q << " ";
		}
		ofs << std::endl;
	}
#endif

	unsigned imax = std::distance(bins.begin(), std::max_element(bins.begin(), bins.end()));
	return static_cast<float>(imax) / static_cast<float>(BINS) * PiHalf;
}


/** Computes the dominant rotation */
float DominantRotation(const ebs::EventField& u)
{
	// return DominantRotationMaxScore(u);
	return DominantRotationHistogram(u);
}

typedef std::pair<float,float> IntRes;

constexpr float DT_SIGMA = 0.05f;
constexpr float MAX_SIGMA = 4.0f;

IntRes IntegrateEdge(const ebs::EventField& u, const Eigen::Vector2f& center, const Eigen::Vector2f& dir, float tnow)
{
	constexpr unsigned N = 8;
	unsigned total = 0;
	float err = 0.0f;
	unsigned pone = 0;
	Eigen::Vector2f p = center;
	for(unsigned k=1; k<N; k++, p+=dir) {
		int pi = static_cast<int>(p.x() + 0.5f); // round
		int pj = static_cast<int>(p.y() + 0.5f);
		if(0 <= pi && pi < u.rows() && 0 <= pj && pj < u.cols()) {
			const auto& e = u.at(pi, pj);
			total += 1;
			float dt = std::min(tnow - e.time, MAX_SIGMA*DT_SIGMA);
			err += dt*dt;
			pone += e.parity;
		}
	}
	if(total == 0) {
		return { MAX_SIGMA*MAX_SIGMA*DT_SIGMA*DT_SIGMA, 0.5f };
	}
	return {
		err / static_cast<float>(total),
		static_cast<float>(pone)/static_cast<float>(total)
	};
}

float EdgeLogProbabilityPolarity(const IntRes& a, unsigned pol)
{
	constexpr float ALPHA = 0.95f;
	constexpr float LOG_ALPHA_0 = std::log(ALPHA);
	constexpr float LOG_ALPHA_1 = std::log(1.0f - ALPHA);
	if(pol == 1) {
		return a.second*LOG_ALPHA_0 + (1.0f - a.second)*LOG_ALPHA_1;
	}
	else {
		return a.second*LOG_ALPHA_1 + (1.0f - a.second)*LOG_ALPHA_0;
	}
}

float EdgeLogProbabilityTime(const IntRes& a)
{
	return -0.5f*a.first/(DT_SIGMA*DT_SIGMA);
}

Eigen::Vector2f Dir(float phi)
{ return Eigen::Vector2f{std::cos(phi), std::sin(phi)}; }

struct Directions
{
	static constexpr unsigned NUM_DIRS = 8;

	Directions()
	: dirs(NUM_DIRS), delta(NUM_DIRS), angles(NUM_DIRS)
	{
		constexpr float ANGLE = 1.0f / static_cast<float>(NUM_DIRS) * 1.5708f;
		constexpr float DELTA = 1.0f * ANGLE;
		for(size_t k=0; k<dirs.size(); k++) {
			float phi = static_cast<float>(k) * ANGLE;
			dirs[k] = Dir(phi);
			delta[k] = { Dir(phi-DELTA), Dir(phi+DELTA) };
			angles[k] = phi;
		}
	}

	size_t num() const
	{ return NUM_DIRS; }

	float angle(unsigned k) const 
	{ return angles[k]; }

	const Eigen::Vector2f& dir(unsigned k) const
	{ return dirs[k]; }

	const Eigen::Vector2f& delta_m(unsigned k) const
	{ return delta[k].first; }

	const Eigen::Vector2f& delta_p(unsigned k) const
	{ return delta[k].second; }

	std::vector<Eigen::Vector2f> dirs;
	std::vector<std::pair<Eigen::Vector2f,Eigen::Vector2f>> delta;
	std::vector<float> angles;
};

struct IntResDelta
{
	IntRes c;
	IntRes m;
	IntRes p;
};

Directions s_dirs;

float DetectOpposingEdgePolarity(const IntResDelta& a, const IntResDelta& b)
{
	float pm0 = EdgeLogProbabilityPolarity(a.m, 0) + EdgeLogProbabilityPolarity(b.m, 0);
	float pm1 = EdgeLogProbabilityPolarity(a.m, 1) + EdgeLogProbabilityPolarity(b.m, 1);
	float pp0 = EdgeLogProbabilityPolarity(a.p, 0) + EdgeLogProbabilityPolarity(b.p, 0);
	float pp1 = EdgeLogProbabilityPolarity(a.p, 1) + EdgeLogProbabilityPolarity(b.p, 1);
	// one delta should be 1, the other one should be 0
	// try out both permutations and take max
	return std::max(pm0 + pp1, pm1 + pp0);
}

float DetectOpposingEdgeTime(const IntResDelta& a, const IntResDelta& b)
{
	return EdgeLogProbabilityTime(a.c) + EdgeLogProbabilityTime(b.c);
}

float DetectOpposingEdge(const IntResDelta& a, const IntResDelta& b)
{
	return 1*DetectOpposingEdgeTime(a,b) + 1*DetectOpposingEdgePolarity(a,b);
}

IntResDelta IntegrateEdgeDelta(const ebs::EventField& u, const Eigen::Vector2f& center,
	const Eigen::Vector2f& dc, const Eigen::Vector2f& dm, const Eigen::Vector2f& dp,
	float tnow)
{
	return {
		IntegrateEdge(u,center,dc,tnow),
		IntegrateEdge(u,center,dm,tnow),
		IntegrateEdge(u,center,dp,tnow)
	};
}

Eigen::Vector2f Rotate90(const Eigen::Vector2f& u)
{ return Eigen::Vector2f{-u[1], +u[0]}; }

float DetectCorner(const ebs::EventField& u, unsigned i, unsigned j, unsigned k, float tnow)
{
	Eigen::Vector2f center{static_cast<float>(i)+0.5f, static_cast<float>(j)+0.5f};
	auto dc1 = s_dirs.dir(k);
	auto dm1 = s_dirs.delta_m(k);
	auto dp1 = s_dirs.delta_p(k);
	auto a11 = IntegrateEdgeDelta(u, center,  dc1,  dm1,  dp1, tnow);
	auto a12 = IntegrateEdgeDelta(u, center, -dc1, -dm1, -dp1, tnow);
	auto dc2 = Rotate90(s_dirs.dir(k));
	auto dm2 = Rotate90(s_dirs.delta_m(k));
	auto dp2 = Rotate90(s_dirs.delta_p(k));
	auto a21 = IntegrateEdgeDelta(u, center,  dc2,  dm2,  dp2, tnow);
	auto a22 = IntegrateEdgeDelta(u, center, -dc2, -dm2, -dp2, tnow);
	return DetectOpposingEdge(a11,a12) + DetectOpposingEdge(a21,a22);
}

std::pair<float,unsigned> DetectCorner(const ebs::EventField& u, unsigned i, unsigned j, float tnow)
{
	float vmax = std::numeric_limits<float>::lowest();
	float amax = 0.0f;
	for(size_t k=0; k<s_dirs.num(); k++) {
		float q = DetectCorner(u, i, j, k, tnow);
		if(q > vmax) {
			vmax = q;
			amax = k;
		}
	}
	return {vmax,amax};
}

Eigen::MatrixXf CrossDetection(const ebs::EventField& u)
{
	const float tnow = u.last().time;
	Eigen::MatrixXf mat{u.rows(), u.cols()};
	for_each(mat,
		[&u,tnow](unsigned i, unsigned j, float& v) {
			auto q = DetectCorner(u,i,j,tnow);
			v = q.first;
		}
	);
	return mat;
}

void CrossDetectionDebug(const ebs::EventField& u, std::vector<Eigen::MatrixXf>& mats)
{
	mats.resize(6, Eigen::MatrixXf{u.rows(), u.cols()});
	const float tnow = u.last().time;
	for(unsigned i=0; i<u.rows(); i++) {
		for(unsigned j=0; j<u.cols(); j++) {
			// find max
			auto q = DetectCorner(u,i,j,tnow);
			mats[0](i,j) = q.first;
			mats[1](i,j) = s_dirs.angle(q.second);
			// other debug
			Eigen::Vector2f center{static_cast<float>(i)+0.5f, static_cast<float>(j)+0.5f};
			unsigned k = q.second;
			auto dc1 = s_dirs.dir(k);
			auto dm1 = s_dirs.delta_m(k);
			auto dp1 = s_dirs.delta_p(k);
			auto a11 = IntegrateEdgeDelta(u, center,  dc1,  dm1,  dp1, tnow);
			auto a12 = IntegrateEdgeDelta(u, center, -dc1, -dm1, -dp1, tnow);
			auto dc2 = Rotate90(s_dirs.dir(k));
			auto dm2 = Rotate90(s_dirs.delta_m(k));
			auto dp2 = Rotate90(s_dirs.delta_p(k));
			auto a21 = IntegrateEdgeDelta(u, center,  dc2,  dm2,  dp2, tnow);
			auto a22 = IntegrateEdgeDelta(u, center, -dc2, -dm2, -dp2, tnow);
			mats[2](i,j) = std::max(DetectOpposingEdge(a11,a12), DetectOpposingEdge(a21,a22));
			mats[3](i,j) = DetectOpposingEdgeTime(a11,a12) + DetectOpposingEdgeTime(a21,a22);
			mats[4](i,j) = DetectOpposingEdgePolarity(a11,a12) + DetectOpposingEdgePolarity(a21,a22);
			mats[5](i,j) = 0;
		}
	}
}

/** Computes local minima */
std::vector<Eigen::Vector2f> LocalMaxima(const Eigen::MatrixXf& u)
{
	constexpr int R = 5;
	constexpr unsigned NUM = 20;
	std::vector<Eigen::Vector2f> v;
	// find all local minima
	// check only points with a minimal threshold
	for_each(u,
		[&u,&v](unsigned i, unsigned j, float val) {
			// if(val > THRESHOLD) {
			// 	return;
			// }
			if(any_box2(R, [i,j,val,&u](int ii, int jj) {
					return !(ii==0 && jj==0) && u(i+ii,j+jj) >= val;
				})
			) {
				return;
			}
			v.emplace_back(i,j);
		}, R);
	// find num best
	std::sort(v.begin(), v.end(),
		[&u](const Eigen::Vector2f& a, const Eigen::Vector2f& b) {
			return u(a.x(), a.y()) > u(b.x(), b.y()); 
		});
	v.resize(std::min<std::size_t>(v.size(), NUM));

#ifdef DEBUG_EBS_CHECKERBOARD
	{
		static std::ofstream ofs("/tmp/corners.tsv");
		for(const auto& c : v) {
			ofs << c.x() << ", " << c.y() << ", ";
		}
		ofs << std::endl;
	}
#endif

	return v;
}

float ComputeSpacing(const std::vector<Eigen::Vector2f>& corners)
{
	const unsigned n = corners.size();
	float sum = 0.0f;
	for(unsigned i=0; i<n; i++) {
		std::vector<float> dist(n);
		for(unsigned j=0; j<n; j++) {
			dist[j] = (corners[i] - corners[j]).norm();
		}
		std::sort(dist.begin(), dist.end());
		sum += dist[1] + dist[2] + dist[3] + dist[4]; // dist[0] is 0 as the distance to the point itself is 0
	}
	return (sum/4.0f - 1.4142f) / static_cast<float>(n);
	// the sqrt(2) is a correction factor to compensate for the corners
}

size_t FindClosest(const std::vector<Eigen::Vector2f>& corners, const Eigen::Vector2f& p)
{
	// std::cout << "CCC" << std::endl;
	// for(auto c : corners) {
	// 	std::cout << (c-p).norm() << ", ";
	// }
	// std::cout << "CCC" << std::endl;
	return std::distance(corners.begin(),
		minimize(corners.begin(), corners.end(),
			[p](const Eigen::Vector2f& a) { return (a - p).squaredNorm(); }).first);
}

std::vector<size_t> FindPointsOneDirection(const std::vector<Eigen::Vector2f>& corners, const Eigen::Vector2f& start, const Eigen::Vector2f& dir)
{
	const float THRESHOLD = 1.0f + 0.25f*dir.norm();
	std::vector<size_t> r;
	Eigen::Vector2f current = start;
	while(true) {
		size_t i = FindClosest(corners, current);
		float dist = (corners[i] - current).norm();
		// std::cout << current.transpose() << " -> " << dist << " " << i << std::endl;
		if(dist >= THRESHOLD) {
			break;
		}
		r.push_back(i);
		current = corners[i] + dir;
	}
	return r;
}

std::vector<size_t> FindPoints(const std::vector<Eigen::Vector2f>& corners, const Eigen::Vector2f& start, const Eigen::Vector2f& dir)
{
	// std::cout << "CCC" << std::endl;
	// for(auto c : corners) {
	// 	std::cout << c.transpose() << ", ";
	// }
	// std::cout << "CCC" << std::endl;
	std::vector<size_t> b = FindPointsOneDirection(corners, start, -dir);
	// std::cout << b.size() << std::endl;
	std::reverse(b.begin(), b.end());
	b.pop_back();
	std::vector<size_t> a = FindPointsOneDirection(corners, start, dir);
	// std::cout << a.size() << std::endl;
	b.insert(b.end(), a.begin(), a.end());
	return b;
}

void SortCheckerboardCornersOld(ebs::CheckerboardEvent& cb)
{
	cb.success = false;
	if(cb.corners.size() != cb.rows*cb.cols) {
		return;
	}
	// spacing
	float spacing = ComputeSpacing(cb.corners);
//	std::cout << "Spacing =" << spacing << std::endl;
	// find local coordinate frame
	Eigen::Vector2f dir1{spacing*std::cos(cb.angle), spacing*std::sin(cb.angle)};
	Eigen::Vector2f dir2{-dir1[1], dir1[0]};
	// find top corner
	Eigen::Vector2f p = *std::max_element(cb.corners.begin(), cb.corners.end(),
		[](const Eigen::Vector2f& a, const Eigen::Vector2f& b) { return a[1] < b[1]; });
	// find points in direction 1/2
	std::vector<size_t> pnts1 = FindPoints(cb.corners, p, dir1);
	std::vector<size_t> pnts2 = FindPoints(cb.corners, p, dir2);
	// check number of points found
	// std::cout << pnts1.size() << " / " << pnts2.size() << std::endl;
	if(pnts1.size() == cb.cols && pnts2.size() == cb.rows)
	{}
	else if(pnts1.size() == cb.rows && pnts2.size() == cb.cols)
	{
		std::swap(dir1, dir2);
		std::swap(pnts1, pnts2);
	}
	else {
		return;
	}
//	std::cout << pnts1.size() << " / " << pnts2.size() << std::endl;
	if(dir1[0] > 0) dir1 *= -1;
	// scan rows
	std::vector<Eigen::Vector2f> corners_new;
	for(unsigned i=0; i<pnts2.size(); i++) {
		std::vector<size_t> pnts = FindPoints(cb.corners, cb.corners[pnts2[i]], dir1);
		if(pnts.size() != cb.cols) {
			return;
		}
		for(size_t j : pnts) {
			corners_new.push_back(cb.corners[j]);
		}
	}
	// create "correct" order for poor men
	// this asserts that the first points are below
	// it assumes that the checkerboard is mounted s.t. the white corners are right
	// it only works if the rotation is not too great
	{
		float scrFront = std::accumulate(corners_new.begin(), corners_new.begin() + cb.cols,
			0.0f, [](float a, const Eigen::Vector2f& p) { return a + p[1]; });
		float scrBack = std::accumulate(corners_new.rbegin(), corners_new.rbegin() + cb.cols,
			0.0f, [](float a, const Eigen::Vector2f& p) { return a + p[1]; });
		if(scrFront < scrBack) {
			auto tmp = corners_new;
			for(unsigned i=0; i<cb.rows; i++) {
				for(unsigned j=0; j<cb.cols; j++) {
					corners_new[i*cb.cols+j] = tmp[(cb.rows-1-i)*cb.cols+j];
				}
			}
		}
	}
	// success!
	cb.corners = corners_new;
	cb.success = true;
}

int FindInCone(const std::vector<Eigen::Vector2f>& corners, const Eigen::Vector2f& p, const Eigen::Vector2f& dir)
{
	// std::cout << "CCC" << std::endl;
	// for(auto c : corners) {
	// 	std::cout << (c-p).norm() << ", ";
	// }
	// std::cout << "CCC" << std::endl;
	constexpr float ANGLE = 20.0f * 0.0174533f;
	constexpr float COS_ANGLE = std::cos(ANGLE);
	int best = -1;
	float best_dist = 0.0f;
	for(int i=0; i<corners.size(); i++) {
		Eigen::Vector2f d = corners[i] - p;
		float dnorm = d.norm();
		if(dnorm <= 0.001f) {
			continue;
		}
		float cosangle = d.dot(dir) / dnorm;
		if(cosangle >= COS_ANGLE && (dnorm < best_dist || best == -1)) {
			best = i;
			best_dist = dnorm;
		}
	}
	return best;
}

struct Node
{
	Eigen::Vector2f pos;
	float angle;
	std::vector<unsigned> neighbours;
	bool isCorner() const { return neighbours.size() == 2; }
	bool isEdge() const { return neighbours.size() == 3; }
	bool isInner() const { return neighbours.size() == 4; }
	int mark = 0;
};

int FindNext(unsigned start, const std::vector<Node>& nodes)
{
	int best_i = -1;
	unsigned best_v = 10000;
	for(unsigned i : nodes[start].neighbours) {
		if(nodes[i].mark == 1 || nodes[i].neighbours.empty()) {
			continue;
		}
		unsigned sum = 0;
		for(unsigned j : nodes[i].neighbours) {
			sum += (nodes[j].mark == 0);
		}
		if(sum < best_v) {
			best_v = sum;
			best_i = i;
		}
	}
	return best_i;
}

void SortCheckerboardCorners(const ebs::EventField& u, ebs::CheckerboardEvent& cb)
{
	std::vector<Eigen::Vector2f> points = cb.corners;
	const float tnow = u.last().time;
	// find neighbours for each point
	std::vector<Node> nodes;
	cb.corner_angles.clear();
	cb.corner_neighbours.clear();
	for(const auto& p : points) {
		Node node;
		node.pos = p;
		node.angle = s_dirs.angle(DetectCorner(u,p[0],p[1],tnow).second);
		std::vector<Eigen::Vector2f> dirs = {
			Dir(node.angle),
			Dir(node.angle + 1.5708f),
			Dir(node.angle + 3.14159f),
			Dir(node.angle - 1.5708f)
		};
		for(const auto& d : dirs) {
			int idx = FindInCone(points, p, d);
			if(idx != -1) {
				node.neighbours.push_back(idx);
			}
		}
//		std::cout << node.neighbours.size() << " ";
		nodes.push_back(node);
		cb.corner_angles.push_back(node.angle);
		cb.corner_neighbours.push_back(node.neighbours);
	}
//	std::cout << std::endl;
	// sort neighbor by number of neighbours
	for(Node& node : nodes) {
		std::sort(node.neighbours.begin(), node.neighbours.end(),
			[nodes](unsigned a, unsigned b) {
				return nodes[a].neighbours.size() < nodes[b].neighbours.size();
			});
	}
	// find any corner
	int icurr = -1;
	{
		float pos_y_best;
		for(unsigned i=0; i<nodes.size(); i++) {
			if(nodes[i].isCorner()) {
				float v = nodes[i].pos[1];
				if(icurr == -1 || v < pos_y_best) {
					icurr = i;
					pos_y_best = v;
				}
			}
		}
	}
	if(icurr == -1) {
		cb.success = false;
		return;
	}
	// iterate corners in spiral form
	std::vector<unsigned> sorted;
	nodes[icurr].mark = 1;
	sorted.push_back(icurr);
	while(true) {
		icurr = FindNext(icurr, nodes);
		if(icurr == -1) {
			break;
		}
		nodes[icurr].mark = 1;
		sorted.push_back(icurr);
	}
	// check if successful
	if(sorted.size() == cb.rows*cb.cols) {
		assert(cb.rows == 5 && cb.cols == 4); // FIXME
		// rearrange to get pattern
		auto q = sorted;
		if(nodes[q[10]].isCorner() && nodes[q[3]].isCorner() && nodes[q[7]].isCorner()) {
			// short
			sorted = {
				q[0], q[13], q[12], q[11], q[10],
				q[1], q[14], q[19], q[18], q[ 9],
				q[2], q[15], q[16], q[17], q[ 8],
				q[3], q[ 4], q[ 5], q[ 6], q[ 7]
			};
		}
		else if(nodes[q[4]].isCorner() && nodes[q[11]].isCorner() && nodes[q[7]].isCorner()) {
			// long
			sorted = {
				q[ 0], q[ 1], q[ 2], q[ 3], q[ 4],
				q[13], q[14], q[19], q[18], q[ 5],
				q[12], q[15], q[16], q[17], q[ 6],
				q[11], q[10], q[ 9], q[ 8], q[ 7]
			};
		}
		else {
			cb.sorted = sorted;
			cb.success = false;
			return;
		}
		// flip if necessary
		if(nodes[q[0]].pos[0] > nodes[q[4]].pos[0]) {
			q = sorted;
			sorted = {
				q[ 4], q[ 3], q[ 2], q[ 1], q[ 0],
				q[ 9], q[ 8], q[ 7], q[ 6], q[ 5],
				q[14], q[13], q[12], q[11], q[10],
				q[19], q[18], q[17], q[16], q[15]
			};
		}
		// re-map
		std::vector<Eigen::Vector2f> corners_new;
		for(unsigned i=0; i<sorted.size(); i++) {
			corners_new.push_back(points[sorted[i]]);
		}
		cb.success = true;
		cb.corners = corners_new;
		cb.sorted = {};
		cb.corner_angles = {};
		cb.corner_neighbours = {};
		// cb.sorted = sorted;
		// cb.success = false;
	}
	else {
		cb.sorted = sorted;
		cb.success = false;
	}
}

constexpr unsigned CHECKERBOARD_ROWS = 4;
constexpr unsigned CHECKERBOARD_COLS = 5;

float MeanCornerAngle(const ebs::EventField& u, const std::vector<Eigen::Vector2f>& corners)
{
	float angle = 0.0f;
	for(const auto& p : corners) {
		const float tnow = u.last().time;
		auto q = DetectCorner(u,p[0],p[1],tnow);
		angle += s_dirs.angle(q.second);
	}
	return angle / static_cast<float>(corners.size());
}

ebs::CheckerboardEvent ebs::DetectCheckerboardEvent(const ebs::EventField& u, bool debug)
{
	CheckerboardEvent cb;
	cb.rows = CHECKERBOARD_ROWS;
	cb.cols = CHECKERBOARD_COLS;
	cb.width = u.cols();
	cb.height = u.rows();
	if(u.num_events_ < 100) {
		return cb;
	}

	// // find dominant rotation
	// cb.angle   = DominantRotation(u);
	// // compute cross matching error
	// cb.dbg_cme = CrossMatchingError(u, cb.angle);

	// find cross matching error (max over all directions)
	cb.dbg_cme = CrossDetection(u);

	// DEBUG
	if(debug) {
		CrossDetectionDebug(u, cb.dbg);
	}

	// minima selection
	cb.corners = LocalMaxima(cb.dbg_cme);

	// compute angle
	cb.angle = MeanCornerAngle(u, cb.corners);

	// sort corners
	SortCheckerboardCorners(u, cb);
	return cb;
}

void PlotCross(slimage::Image3ub& img, const Eigen::Vector2f& p, float angle, float radius, const slimage::Pixel3ub& col)
{
	float sina = std::sin(angle);
	float cosa = std::cos(angle);
	Eigen::Vector2f d1 = radius*Eigen::Vector2f{+cosa, +sina};
	Eigen::Vector2f d2 = radius*Eigen::Vector2f{-sina, +cosa};
	Eigen::Vector2f p11 = p - d1;
	Eigen::Vector2f p12 = p + d1;
	Eigen::Vector2f p21 = p - d2;
	Eigen::Vector2f p22 = p + d2;
	slimage::PaintLine(img, p11.x(), p11.y(), p12.x(), p12.y(), col);
	slimage::PaintLine(img, p21.x(), p21.y(), p22.x(), p22.y(), col);
}

slimage::Image3ub ebs::PlotCheckerboard(const ebs::CheckerboardEvent& cb)
{
	slimage::Image3ub img{cb.width, cb.height};
	slimage::Fill(img, slimage::Pixel3ub{0,0,0});
	ebs::PlotCheckerboard(img, cb);
	return img;
}

void ebs::PlotCheckerboard(slimage::Image3ub& img, const ebs::CheckerboardEvent& cb)
{
	const slimage::Pixel3ub COL_LINE = slimage::Pixel3ub{255,255,255};
	const slimage::Pixel3ub COL_RED = slimage::Pixel3ub{255,0,0};
	constexpr unsigned COL_CORNER_NUM = 7;
	const slimage::Pixel3ub COL_CORNER[COL_CORNER_NUM] = {
		slimage::Pixel3ub{255,  0,  0},
		slimage::Pixel3ub{255,128,  0},
		slimage::Pixel3ub{255,255,  0},
		slimage::Pixel3ub{  0,255,  0},
		slimage::Pixel3ub{  0,255,255},
		slimage::Pixel3ub{  0,  0,255},
		slimage::Pixel3ub{255,  0,255}
	};
	const float LINE_LEN = 5.0f;
	const float RAD = 2.0f;
	//PlotCross(img, 0.5f*Eigen::Vector2f{img.width(), img.height()}, cb.angle, 32.0f, COL_LINE);
	if(cb.success) {
		assert(cb.corners.size() == CHECKERBOARD_COLS*CHECKERBOARD_ROWS);
		for(unsigned k=0; k<cb.corners.size(); k++) {
			Eigen::Vector2f p = cb.corners[k] + Eigen::Vector2f{0.5f,0.5f};
			Eigen::Vector2f p2 = cb.corners[(k+1)%cb.corners.size()] + Eigen::Vector2f{0.5f,0.5f};
			slimage::Pixel3ub col = COL_CORNER[(k/CHECKERBOARD_COLS)%COL_CORNER_NUM];
			slimage::FillEllipse(img, p.x(), p.y(), RAD,0, 0,RAD, col);
			slimage::PaintLine(img, p.x(), p.y(), p2.x(), p2.y(), col);
		}
	}
	else {
		const slimage::Pixel3ub COL_CORNER_NN[5] = {
			slimage::Pixel3ub{255,255,255},
			slimage::Pixel3ub{255,  0,  0},
			slimage::Pixel3ub{255,255,  0},
			slimage::Pixel3ub{  0,255,  0},
			slimage::Pixel3ub{  0,  0,255}
		};
		// // plot lines to neighbours
		// for(unsigned i=0; i<cb.corners.size(); i++) {
		// 	const auto& p = cb.corners[i];
		// 	const auto& neighbours = cb.corner_neighbours[i];
		// 	unsigned n = std::min<unsigned>(neighbours.size(), 4);
		// 	for(const auto& j : neighbours) {
		// 		const auto& p2 = cb.corners[j];
		// 		slimage::PaintLine(img, p.x(), p.y(), p2.x(), p2.y(), COL_CORNER_NN[0]);
		// 	}
		// }
		// plot markers
		for(unsigned i=0; i<cb.corners.size(); i++) {
			Eigen::Vector2f p = cb.corners[i] + Eigen::Vector2f{0.5f,0.5f};
			const auto& neighbours = cb.corner_neighbours[i];
			unsigned n = std::min<unsigned>(neighbours.size(), 4);
			//slimage::FillEllipse(img, p.x(), p.y(), RAD,0, 0,RAD, COL_CORNER_NN[n]);
			PlotCross(img, p, cb.corner_angles[i], LINE_LEN, COL_RED);// COL_CORNER_NN[n]);
		}
		// connect
		// for(unsigned i=0; i+1<cb.sorted.size(); i++) {
		// 	const auto& p = cb.corners[cb.sorted[i]];
		// 	const auto& p2 = cb.corners[cb.sorted[i+1]];
		// 	slimage::PaintLine(img, p.x(), p.y(), p2.x(), p2.y(), slimage::Pixel3ub{255,0,255});
		// }
	}
}

slimage::Image3ub ebs::PlotCheckerboardCME(const ebs::CheckerboardEvent& cb)
{
	constexpr float MAX_LOG_PROP = 30.0f;
	slimage::Image3ub img{cb.width, cb.height};
	slimage::Fill(img, slimage::Pixel3ub{0,0,0});
	for_each(cb.dbg_cme,
		[&img](unsigned i, unsigned j, float v) {
			float q = std::max(MAX_LOG_PROP + v, 0.0f) / MAX_LOG_PROP;
			unsigned char g = static_cast<unsigned char>(255.0f * q);
			img(i,j) = slimage::Pixel3ub{g,g,g};
		});
	return img;
}

cv::Size gBoardSize{CHECKERBOARD_COLS, CHECKERBOARD_ROWS};
float gSquareSize = 0.035f; // FIXME

template<int ROWS, int COLS>
void convert(const cv::Mat& in, Eigen::Matrix<double,ROWS,COLS>& out)
{
	out = Eigen::Matrix<double,ROWS,COLS>(in.rows, in.cols);
	// unsigned out_rows = (ROWS == Eigen::Dynamic ? in.rows : out.rows());
	// unsigned out_cols = (COLS == Eigen::Dynamic ? in.cols : out.cols());
	// if(in.rows != out.rows() || in.cols != out.cols()) {
	// 	std::cerr << "Wrong dimensions" << std::endl;
	// 	return;
	// }
	for(unsigned i=0; i<in.rows; i++) {
		for(unsigned j=0; j<in.cols; j++) {
			out(i,j) = in.at<double>(i,j);
		}
	}
}

template<int ROWS, int COLS>
void convert(const Eigen::Matrix<double,ROWS,COLS>& in, cv::Mat& out)
{
	out = cv::Mat::zeros(in.rows(), in.cols(), CV_64F);
	for(unsigned i=0; i<in.rows(); i++) {
		for(unsigned j=0; j<in.cols(); j++) {
			out.at<double>(i,j) = in(i,j);
		}
	}
}

void convert(const cv::Point2f& in, Eigen::Vector2f& out)
{
	out[0] = in.x;
	out[1] = in.y;
}

void convert(const Eigen::Vector2f& in, cv::Point2f& out)
{
	out.x = in[0];
	out.y = in[1];
}

template<typename T1, typename T2>
void convert(const std::vector<T1>& in, std::vector<T2>& out)
{
	out.resize(in.size());
	for(std::size_t k=0; k<in.size(); k++) {
		convert(in[k], out[k]);
	}
}

ebs::CheckerboardDense ebs::DetectCheckerboardDense(const slimage::Image3ub& u)
{
	ebs::CheckerboardDense cb;
	cb.raw = u;
	cb.rows = CHECKERBOARD_ROWS;
	cb.cols = CHECKERBOARD_COLS;
	cb.width = u.width();
	cb.height = u.height();
	// convert to OpenCV image
	cv::Mat view = slimage::ConvertToOpenCv(slimage::make_anonymous(u));
	// find corners
	std::vector<cv::Point2f> pointBuf;
	cb.success = cv::findChessboardCorners(
		view, gBoardSize, pointBuf,
		CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_FAST_CHECK | CV_CALIB_CB_NORMALIZE_IMAGE);
	// convert result
	if(cb.success) {
		convert(pointBuf, cb.corners);
	}
	return cb;
}

slimage::Image3ub ebs::PlotCheckerboard(const ebs::CheckerboardDense& cb)
{
	cv::Mat view = slimage::ConvertToOpenCv(slimage::make_anonymous(cb.raw));
	std::vector<cv::Point2f> pointBuf;
	convert(cb.corners, pointBuf);
	cv::drawChessboardCorners(view, gBoardSize, cv::Mat(pointBuf), cb.success);
	return slimage::anonymous_cast<unsigned char,3>(slimage::ConvertToSlimage(view));
}

std::vector<cv::Point3f> calcCheckerboardCornerPositions(cv::Size boardSize, float squareSize)
{
	std::vector<cv::Point3f> corners;
	corners.reserve(boardSize.width*boardSize.height);
	for(int i=0; i<boardSize.height; ++i) {
		for(int j=0; j<boardSize.width; ++j) {
			corners.emplace_back(j*squareSize, i*squareSize, 0);
		}
	}
	return corners;
}

Eigen::Matrix3d ebs::CreateCameraMatrix(unsigned w, unsigned h, float f)
{
	Eigen::Matrix3d cc;
	cc << f, 0, 0.5*w,
	      0, f, 0.5*h,
	      0, 0, 1.0;
	return cc;
}

template<typename It, typename It2, typename F>
It2 transform_n(It begin, It end, size_t n, It2 dst, F f)
{
	// TODO possibly not safe for n >> total due to possible overflow
	size_t total = std::distance(begin, end);
	for(size_t q=0; begin!=end; ++begin, q+=n) {
		if(q >= total) {
			*dst++ = f(*begin);
			q -= total;
		}
	}
	return dst;
}

std::vector<std::vector<cv::Point2f>> computeImagePoints(const std::vector<ebs::Checkerboard>& vcb, unsigned num)
{
	std::vector<std::vector<cv::Point2f>> imagePoints;
	imagePoints.reserve(vcb.size());
	transform_n(vcb.begin(), vcb.end(), num, std::back_inserter(imagePoints),
		[](const ebs::Checkerboard& cb) {
			std::vector<cv::Point2f> v;
			convert(cb.corners, v);
			return v;
		});
	return imagePoints;
}

ebs::CameraCalibration ebs::CalibrateCamera(const std::vector<ebs::Checkerboard>& vcb, float focal_guess)
{
	constexpr unsigned MAX_NUM = 40;
	ebs::CameraCalibration cc;
	// image points (take maximal number)
	std::vector<std::vector<cv::Point2f>> imagePoints = computeImagePoints(vcb, MAX_NUM);
	// prepare calibration plate corners
	std::vector<std::vector<cv::Point3f>> objectPoints(imagePoints.size(),
		calcCheckerboardCornerPositions(gBoardSize, gSquareSize));
	// image size
	cv::Size imageSize{static_cast<int>(vcb[0].width), static_cast<int>(vcb[0].height)};
	// prepare camera and distortion matrix
	cv::Mat cameraMatrix = cv::Mat::eye(3, 3, CV_64F);
	cameraMatrix.at<double>(0,0) = focal_guess;
	cameraMatrix.at<double>(1,1) = focal_guess;
	cameraMatrix.at<double>(0,2) = static_cast<float>(vcb[0].width) * 0.5f;
	cameraMatrix.at<double>(1,2) = static_cast<float>(vcb[0].height) * 0.5f;
	cv::Mat distCoeffs = cv::Mat::zeros(5, 1, CV_64F);
	// call calibrate
	std::vector<cv::Mat> rvecs, tvecs;
 	double rms = cv::calibrateCamera(
		objectPoints, imagePoints, imageSize,
		cameraMatrix, distCoeffs,
		rvecs, tvecs,
		  CV_CALIB_USE_INTRINSIC_GUESS
		//| CV_CALIB_FIX_ASPECT_RATIO
		//| CV_CALIB_ZERO_TANGENT_DIST
		);
	// return
	convert(cameraMatrix, cc.intrinsic);
	convert(distCoeffs, cc.distortion);
	std::cout << "cam=" << cameraMatrix << std::endl;
	std::cout << "dist=" << distCoeffs << std::endl;
	return cc;
}

ebs::StereoCalibration ebs::CalibrateStereo(
	const std::vector<ebs::Checkerboard>& vcb1, const ebs::CameraCalibration& cc1,
	const std::vector<ebs::Checkerboard>& vcb2, const ebs::CameraCalibration& cc2
)
{
	constexpr unsigned MAX_NUM = 40;
	ebs::StereoCalibration sc;
	// points
	std::vector<std::vector<cv::Point2f>> imagePoints1 = computeImagePoints(vcb1, MAX_NUM);
	std::vector<std::vector<cv::Point2f>> imagePoints2 = computeImagePoints(vcb2, MAX_NUM);
	std::vector<std::vector<cv::Point3f>> objectPoints(imagePoints1.size(),
		calcCheckerboardCornerPositions(gBoardSize, gSquareSize));
	// camera matrices
	cv::Mat cameraMatrix1, cameraMatrix2, distCoeffs1, distCoeffs2;
	convert(cc1.intrinsic, cameraMatrix1);
	convert(cc2.intrinsic, cameraMatrix2);
	convert(cc1.distortion, distCoeffs1);
	convert(cc2.distortion, distCoeffs2);
	// image size
	cv::Size imageSize{static_cast<int>(vcb1[0].width), static_cast<int>(vcb1[0].height)};
	// result
	cv::Mat R,T,E,F;
	// run
	double res = cv::stereoCalibrate(
		objectPoints, imagePoints1, imagePoints2,
		cameraMatrix1, distCoeffs1, cameraMatrix2, distCoeffs2,
		imageSize,
		R,T,E,F
	);
	std::cout << "Stereo Calibration Result: " << res << std::endl;
	convert(R, sc.R);
	convert(T, sc.T);
	convert(E, sc.E);
	convert(F, sc.F);
	return sc;
}

namespace boost {
namespace serialization {

	template<class Archive, typename T, int ROWS, int COLS>
	void save(Archive& ar, const Eigen::Matrix<T,ROWS,COLS>& m, const unsigned int version)
	{
		int rows = m.rows(), cols = m.cols();
		ar & rows & cols;
		for(unsigned i=0; i<m.rows(); i++) {
			for(unsigned j=0; j<m.cols(); j++) {
				ar & m(i,j);
			}
		}
	}

	template<class Archive, typename T, int ROWS, int COLS>
	void load(Archive& ar, Eigen::Matrix<T,ROWS,COLS>& m, const unsigned int version)
	{
		unsigned rows, cols;
		ar & rows & cols;
		m = Eigen::Matrix<T,ROWS,COLS>(rows,cols);
		for(unsigned i=0; i<m.rows(); i++) {
			for(unsigned j=0; j<m.cols(); j++) {
				ar & m(i,j);
			}
		}
	}

	template<class Archive, typename T, int ROWS, int COLS>
	void serialize(Archive& ar, Eigen::Matrix<T,ROWS,COLS>& m, const unsigned int version)
	{
		boost::serialization::split_free(ar, m, version);
	}

	template<class Archive>
	void serialize(Archive& ar, ebs::CameraCalibration& cc, const unsigned int version)
	{
		ar
			& cc.intrinsic
			& cc.distortion;
	}

	template<class Archive>
	void serialize(Archive& ar, ebs::StereoCalibration& sc, const unsigned int version)
	{
		ar
			& sc.R
			& sc.T
			& sc.E
			& sc.F;
	}

	template<class Archive>
	void serialize(Archive& ar, ebs::Calibration& calib, const unsigned int version)
	{
		ar
			& calib.dvs1
			& calib.dvs2
			& calib.kinect
			& calib.kinect_to_dvs1
			& calib.kinect_to_dvs2
			& calib.dvs1_to_dvs2;
	}

}}

void ebs::Save(const CameraCalibration& calib, const std::string& filename)
{
	std::ofstream ofs(filename);
	boost::archive::text_oarchive oa(ofs);
	oa << calib;
}

void ebs::Load(const std::string& filename, CameraCalibration& calib)
{
	std::ifstream ifs(filename);
	boost::archive::text_iarchive ia(ifs);
	ia >> calib;
}

void ebs::Save(const Calibration& calib, const std::string& filename)
{
	std::ofstream ofs(filename);
	boost::archive::text_oarchive oa(ofs);
	oa << calib;
}

void ebs::Load(const std::string& filename, Calibration& calib)
{
	std::ifstream ifs(filename);
	boost::archive::text_iarchive ia(ifs);
	ia >> calib;
}

constexpr float EDVS_FOCAL_GUESS = 150.0f;
constexpr float XTION_FOCAL_GUESS = 525.0f;

ebs::Calibration ebs::Calibrate(const ebs::CalibrationData& data)
{
	ebs::Calibration calib;
	if(data.frames.size() < 3) {
		return calib;
	}
	// calibrate dvs
	std::cout << "Calibrating DVS 1" << std::endl;
	std::vector<ebs::Checkerboard> cb_dvs1;
	for(const auto& q : data.frames) {
		cb_dvs1.push_back(q.dvs1);
	}
	calib.dvs1 = CalibrateCamera(cb_dvs1, EDVS_FOCAL_GUESS);
	// calibrate dvs
	std::cout << "Calibrating DVS 2" << std::endl;
	std::vector<ebs::Checkerboard> cb_dvs2;
	for(const auto& q : data.frames) {
		cb_dvs2.push_back(q.dvs2);
	}
	calib.dvs2 = CalibrateCamera(cb_dvs2, EDVS_FOCAL_GUESS);
	// calibrate kinect
	std::cout << "Calibrating KINECT" << std::endl;
	std::vector<ebs::Checkerboard> cb_kinect;
	for(const auto& q : data.frames) {
		cb_kinect.push_back(q.kinect);
	}
	calib.kinect = CalibrateCamera(cb_kinect, XTION_FOCAL_GUESS);
	// calibrate dvs -> kinect
	std::cout << "Stereo calibration DVS 1 -> KINECT" << std::endl;
	calib.kinect_to_dvs1 = CalibrateStereo(cb_kinect, calib.kinect, cb_dvs1, calib.dvs1);
	std::cout << "Stereo calibration DVS 2 -> KINECT" << std::endl;
	calib.kinect_to_dvs2 = CalibrateStereo(cb_kinect, calib.kinect, cb_dvs2, calib.dvs2);
	std::cout << "Stereo calibration DVS 1 -> DVS 2" << std::endl;
	calib.dvs1_to_dvs2 = CalibrateStereo(cb_dvs1, calib.dvs1, cb_dvs2, calib.dvs2);
	// ok
	return calib;
}

template<typename K>
struct ImagePoint
{
	K j, i;
	K d;
};

template<typename K>
std::ostream& operator<<(std::ostream& os, const ImagePoint<K>& p)
{
	os << "(" << p.j << "," << p.i << "," << p.d << ")";
	return os;
}

struct Reprojection
{
	static constexpr float Z_TO_D = 1000.0f;
	Reprojection(
		const ebs::CameraCalibration& c1,
		const ebs::CameraCalibration& c2,
		const ebs::StereoCalibration& sc)
	{
		a_ = c1.intrinsic.inverse().cast<float>();
		b_ = c2.intrinsic.cast<float>();
		t_ = (Eigen::Translation3d(sc.T) * sc.R).cast<float>();
	}
	ImagePoint<float> operator()(const ImagePoint<int>& ip1) const
	{
		float z1 = static_cast<float>(ip1.d) / Z_TO_D;
		Eigen::Vector3f t1{z1*static_cast<float>(ip1.j), z1*static_cast<float>(ip1.i), z1};
		Eigen::Vector3f p1 = a_ * t1;
		Eigen::Vector3f p2 = t_ * p1;
		Eigen::Vector3f t2 = b_ * p2;
		float z2 = t2[2];
		return {
			t2[0]/z2,
			t2[1]/z2,
			z2
		};
	}
private:
	Eigen::Matrix3f a_, b_;
	Eigen::Transform<float,3,Eigen::Affine> t_;
};

slimage::Image1f ebs::ReprojectDepth(const slimage::Image1ui16& depth,
	const ebs::CameraCalibration& calib_kinect, const ebs::CameraCalibration& calib_dvs, const ebs::StereoCalibration& calib_kinect_to_dvs)
{
	constexpr unsigned DVS_RES = 128; // FIXME
	Reprojection rp{calib_kinect, calib_dvs, calib_kinect_to_dvs};
	slimage::Image1f res{DVS_RES, DVS_RES};
	slimage::Fill(res, slimage::Pixel1f{0});
	for(unsigned i=0; i<depth.height(); i++) {
		for(unsigned j=0; j<depth.width(); j++) {
			uint16_t d1 = depth(j,i);
			if(d1 == 0) {
				continue;
			}
			ImagePoint<int> ip1{j,i,d1};
			ImagePoint<float> ip2 = rp(ip1);
			// if(j == 120) {
			// 	std::cout << ip1 << " -> " << ip2 << std::endl;
			// }
			int ti = static_cast<int>(ip2.i);
			int tj = static_cast<int>(ip2.j);
			float td = ip2.d;
			if(0 <= tj && tj < 128 && 0 <= ti && ti < 128) {
				float& cd = *res.pixel_pointer(tj,ti);
				if(cd == 0 || td < cd) {
					cd = td;
				}
			}
		}
	}
	return res;
}

std::vector<slimage::Image1f> ebs::DepthToDisparity(const slimage::Image1ui16& depth, const ebs::Calibration& calib)
{
	constexpr unsigned DVS_RES = 128; // FIXME
	Reprojection rp1{calib.kinect, calib.dvs1, calib.kinect_to_dvs1};
	Reprojection rp2{calib.kinect, calib.dvs2, calib.kinect_to_dvs2};
	slimage::Image1f res1{DVS_RES, DVS_RES};
	slimage::Fill(res1, slimage::Pixel1f{-1.0f});
	slimage::Image1f res2{DVS_RES, DVS_RES};
	slimage::Fill(res2, slimage::Pixel1f{-1.0f});
	for(unsigned i=0; i<depth.height(); i++) {
		for(unsigned j=0; j<depth.width(); j++) {
			uint16_t d1 = depth(j,i);
			if(d1 == 0) {
				continue;
			}
			ImagePoint<int> src{j,i,d1};
			ImagePoint<float> dst1 = rp1(src);
			ImagePoint<float> dst2 = rp2(src);
			float td = std::abs(dst2.j - dst1.j);
			// if(j == 120) {
			// 	std::cout << ip1 << " -> " << ip2 << std::endl;
			// }
			{
				int ti = static_cast<int>(dst1.i);
				int tj = static_cast<int>(dst1.j);
				if(0 <= tj && tj < 128 && 0 <= ti && ti < 128) {
					float& cd = *res1.pixel_pointer(tj, ti);
					cd = std::max(cd, td);
				}
			}
			{
				int ti = static_cast<int>(dst2.i);
				int tj = static_cast<int>(dst2.j);
				if(0 <= tj && tj < 128 && 0 <= ti && ti < 128) {
					float& cd = *res2.pixel_pointer(tj, ti);
					cd = std::max(cd, td);
				}
			}
		}
	}
	return {res1,res2};
}

struct DistortionImpl
{
	DistortionImpl(const ebs::CameraCalibration& cam)
	{
		k1 = cam.distortion[0];
		k2 = cam.distortion[1];
		p1 = cam.distortion[2];
		p2 = cam.distortion[3];
		k3 = cam.distortion[4]; // 2,3 are tangential params
		Eigen::Matrix3f camMat = cam.intrinsic.cast<float>();
		cxu = camMat(0,0);
		cxv = camMat(0,1);
		cx1 = camMat(0,2);
		cyu = camMat(1,0);
		cyv = camMat(1,1);
		cy1 = camMat(1,2);
		Eigen::Matrix3f camInv = cam.intrinsic.inverse().cast<float>();
		cux = camInv(0,0);
		cuy = camInv(0,1);
		cu1 = camInv(0,2);
		cvx = camInv(1,0);
		cvy = camInv(1,1);
		cv1 = camInv(1,2);
	}
	void operator()(float& x, float& y) {
		float u = cux*x + cuy*y + cu1;
		float v = cvx*x + cvy*y + cv1;
		float uu = u*u;
		float vv = v*v;
		float r2 = uu + vv;
		float uv2 = 2.0f*u*v;
		float lam = 1.0f / (1.0f + r2*(k1 + r2*(k2 + r2*k3)));
		float um = lam*u + p1*uv2 + p2*(r2 + 2.0f*uu);
		float vm = lam*v + p1*(r2 + 2.0f*vv) + p2*uv2;
		x = cxu*um + cxv*vm + cx1;
		y = cyu*um + cyv*vm + cy1;
	}
	float k1, k2, k3;
	float p1, p2;
	float cxu, cxv, cx1;
	float cyu, cyv, cy1;
	float cux, cuy, cu1;
	float cvx, cvy, cv1;
};

float rand_pix()
{
	static std::default_random_engine s_rnd;
	static std::uniform_real_distribution<> dis(0.0f, +1.0f);
	while(true) {
		float r = dis(s_rnd);
		if(r < 1.0f) {
			return r;
		}
	};
}

int PixelRound(float x)
{
	// i + [-.5,+.5[ -> i
	return static_cast<int>(x + 0.5f);
}

int PixelRandomRound(float x)
{
	static std::default_random_engine s_rnd;
	static std::uniform_real_distribution<> dis(0.0f, 1.0f);
	int i = PixelRound(x);
	float r = x - static_cast<float>(i);
	if(r < 0) {
		float p = 1.0f + r;
		if(dis(s_rnd) > p) {
			return i - 1;
		}
		else {
			return i;
		}
	}
	else {
		float p = 1.0f - r;
		if(dis(s_rnd) > p) {
			return i + 1;
		}
		else {
			return i;
		}
	}
}

void ebs::Undistort(std::vector<Edvs::Event>& events, const std::vector<CameraCalibration>& cams,
	bool randomize, std::vector<Eigen::Vector2f>* coordsf)
{
	std::vector<DistortionImpl> dist;
	for(const auto& cam : cams) {
		dist.emplace_back(cam);
	}
	if(coordsf) {
		coordsf->resize(0);
		coordsf->reserve(events.size());
	}
	for(auto& e : events) {
		float x = static_cast<float>(e.x);
		float y = static_cast<float>(e.y);
		dist[e.id](x,y);
		if(coordsf) {
			coordsf->push_back({x,y});
		}
		int ex = randomize ? PixelRandomRound(x) : PixelRound(x);
		int ey = randomize ? PixelRandomRound(y) : PixelRound(y);
		if(0 <= ex && ex < 128 && 0 <= ey && ey < 128) {
			e.x = ex;
			e.y = ey;
		}
		else {
			e.x = 0;
			e.y = 0;
			e.id = 255;
		}
	}
}

Eigen::Vector3f ebs::Triangulize(const ebs::StereoEvent& a)
{
	constexpr unsigned SIZE = 128; // FIXME
	constexpr float FOCAL_PX = 105.0f; // FIXME
	constexpr float BASELINE = 0.14f; // FIXME
	constexpr float DEPTH_MAX = 3.5f;
	constexpr float DISP_MIN = BASELINE * FOCAL_PX / DEPTH_MAX;
	float z = BASELINE * FOCAL_PX / static_cast<float>(std::max(DISP_MIN,a.disparity));
	float x = z*(static_cast<float>(a.x) - 0.5f*static_cast<float>(SIZE))/FOCAL_PX;
	float y = z*(static_cast<float>(a.y) - 0.5f*static_cast<float>(SIZE))/FOCAL_PX;
	return {x,y,z};
}

