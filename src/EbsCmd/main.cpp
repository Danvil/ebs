#include <Edvs/EventIO.hpp>
#include <boost/program_options.hpp>
#include <iostream>
#include <thread>

int main(int argc, char *argv[])
{
	std::vector<std::string> p_vuri;///dev/ttyUSB0?baudrate=4000000";

	namespace po = boost::program_options;
	// Declare the supported options.
	po::options_description desc("Allowed options");
	desc.add_options()
		("help", "produce help message")
		("uris", po::value(&p_vuri)->multitoken(), "URI(s) to event stream(s)")
	;

	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);

	if(vm.count("help")) {
		std::cout << desc << std::endl;
		return 1;
	}

	// open stream
	Edvs::EventStream stream(p_vuri);

	// core init
	ebs::Core ebs_core;

	// FIXME

	return 0;
}
