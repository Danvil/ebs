#ifndef EBSVIS_WDGTEBSCALIBRATE_H
#define EBSVIS_WDGTEBSCALIBRATE_H

#include "ui_WdgtEbsCalibrate.h"
#include <ebs/EventField.hpp>
#include <ebs/Visualization.hpp>
#include <ebs/Calibration.hpp>
#include <ebs/StereoEvent.hpp>
#include <Edvs/EventStream.hpp>
#include <qs/OpenNI.hpp>
#include <miv/WdgtMdiView.h>
#include <da/PropertyWidget.hpp>
#include <da/synchronized.hpp>
#include <QtGui/QWidget>
#include <QtGui/QImage>
#include <QtCore/QTimer>
#include <thread>
#include <atomic>
#include <map>
#include <string>

class WdgtEbsCalibrate : public QMainWindow
{
	Q_OBJECT

public:
	WdgtEbsCalibrate(const std::shared_ptr<Edvs::IEventStream>& stream, QWidget* parent = 0);
	~WdgtEbsCalibrate();

	void closeEvent(QCloseEvent* event);

protected Q_SLOTS:
	void TickCore();
	void TickVis();
	void ActCalibCollect();
	void ActCalibRun();
	void ActCalibSave();
	void ActCalibLoad();

private:
	void threadDvs();
	void threadOpenNi();
	void threadVisualize();
	void threadCheckerboard();

private:
	Ui::WdgtEbsCalibrateClass ui;

	std::unique_ptr<miv::WdgtMdiView> miv_;

	QTimer timer_core_;
	QTimer timer_vis_;

	std::shared_ptr<Edvs::IEventStream> stream_;
	std::shared_ptr<qs::OpenNISource> openni_;

	ebs::EventField tf_[2];

//	std::unique_ptr<VisualizationPipeline> ebs_gui_vis_;

//	std::unique_ptr<qs::Scheduler> g_scheduler;

	bool is_running_;
	std::thread thread_dvs_;
	std::thread thread_openni_;
	std::thread thread_vis_;
	std::thread thread_cb_;

	da::synchronized<std::vector<edvs_event_t>> events_;

	std::atomic<uint64_t> last_frame_ts_;
	da::synchronized<std::vector<ebs::StereoEvent>> events_vis_;

	bool is_collecting_calib_;

	// data
	da::synchronized<slimage::Image3ub> img_color_;
	da::synchronized<slimage::Image1f> img_depth_;
	da::synchronized<std::vector<slimage::Image1f>> img_dvs_depth_;
	da::synchronized<std::vector<slimage::Image1f>> img_dvs_disparity_;
	da::synchronized<ebs::CalibrationData> calib_data_;
	da::synchronized<ebs::Calibration> calib_;
	da::synchronized<std::map<std::string,QImage>> vis_;

};

#endif
