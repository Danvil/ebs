#include "WdgtEbsCalibrate.h"
#include <ebs/Disparity.hpp>
#include <ebs/Calibration.hpp>
#include <ebs/Tools.hpp>
#include <ebs/QtHelpers.hpp>
#include <Edvs/EventIO.hpp>
#include <boost/format.hpp>
#include <slimage/qt.hpp>
#include <slimage/io.hpp>
#include <QPainter>
#include <QMouseEvent>
#include <QFileDialog>
#include <sstream>
#include <chrono>
#include <thread>
#include <qs/OpenNI.hpp>

constexpr unsigned UPDATE_DT_CORE = 0;
constexpr unsigned UPDATE_DT_VIS = 50;
constexpr bool SHOW_3D_POINTS = true;
constexpr uint64_t VIS_DELTA = 200000;
constexpr uint64_t VIS_RENDER_DELTA = 500000;

slimage::Image3ub PlotRaw(const std::vector<Edvs::Event>& v, uint8_t id)
{ return ebs::PlotEventsById(v.begin(), v.end(), 128, 128, id, true); }

slimage::Image3ub PlotStereo(const std::vector<ebs::StereoEvent>& v, ebs::VisMode mode)
{ return ebs::PlotStereoEvents(v.begin(), v.end(), 128, 128, mode, true); }

slimage::Image3ub PlotField(const ebs::EventField& f, ebs::VisMode mode)
{ return ebs::PlotEventField(f, mode); }

WdgtEbsCalibrate::WdgtEbsCalibrate(const std::shared_ptr<Edvs::IEventStream>& stream, QWidget* parent)
: stream_(stream)
{
	ui.setupUi(this);

	showMaximized();

	miv_.reset(new miv::WdgtMdiView{});
	setCentralWidget(miv_.get());

	QObject::connect(&timer_core_, SIGNAL(timeout()), this, SLOT(TickCore()));
	timer_core_.setInterval(UPDATE_DT_CORE);
	timer_core_.start();

	QObject::connect(&timer_vis_, SIGNAL(timeout()), this, SLOT(TickVis()));
	timer_vis_.setInterval(UPDATE_DT_VIS);
	timer_vis_.start();

	QObject::connect(ui.actionCalibCollect, SIGNAL(triggered()), this, SLOT(ActCalibCollect()));
	QObject::connect(ui.actionCalibRun, SIGNAL(triggered()), this, SLOT(ActCalibRun()));
	QObject::connect(ui.actionCalibSave, SIGNAL(triggered()), this, SLOT(ActCalibSave()));
	QObject::connect(ui.actionCalibLoad, SIGNAL(triggered()), this, SLOT(ActCalibLoad()));

	// initialize OpenNI
	openni_ = std::make_shared<qs::OpenNISource>();
	std::this_thread::sleep_for(std::chrono::milliseconds(500));

	// start threads
	is_running_ = true;
	last_frame_ts_ = 0;
	is_collecting_calib_ = false;
	thread_dvs_ = std::thread(&WdgtEbsCalibrate::threadDvs, this);
	thread_openni_ = std::thread(&WdgtEbsCalibrate::threadOpenNi, this);
	thread_vis_ = std::thread(&WdgtEbsCalibrate::threadVisualize, this);
	thread_cb_ = std::thread(&WdgtEbsCalibrate::threadCheckerboard, this);
}

WdgtEbsCalibrate::~WdgtEbsCalibrate()
{
	is_running_ = false;
	thread_dvs_.join();
	thread_openni_.join();
	thread_vis_.join();
	thread_cb_.join();
}

void WdgtEbsCalibrate::closeEvent(QCloseEvent* event)
{
	miv_->close();
}

void WdgtEbsCalibrate::TickCore()
{
}

void WdgtEbsCalibrate::TickVis()
{
	for(const auto& p : *vis_.lock()) {
		miv_->miv()->set(p.first, p.second);
	}
}

void WdgtEbsCalibrate::ActCalibCollect()
{
	is_collecting_calib_ = ui.actionCalibCollect->isChecked();
	std::cout << "Collecting calibration data: " << is_collecting_calib_ << std::endl;
}

void WdgtEbsCalibrate::ActCalibRun()
{
	auto p = calib_.lock();
	*p = ebs::Calibrate(*calib_data_.lock());
	std::cout << *p << std::endl;
}

void WdgtEbsCalibrate::ActCalibSave()
{
	std::string fn = "/tmp/ebs_calib.tsv";
	std::cout << "Saving calibration to '" << fn << "'" << std::endl;
	ebs::Save(*calib_.lock(), fn);
}

void WdgtEbsCalibrate::ActCalibLoad()
{
	std::string fn = "/tmp/ebs_calib.tsv";
	std::cout << "Loading calibration from '" << fn << "'" << std::endl;
	auto p = calib_.lock();
	ebs::Load(fn, *p);
	std::cout << *p << std::endl;
}

void WdgtEbsCalibrate::threadDvs()
{
	while(is_running_) {
		// read events
		std::vector<edvs_event_t> events_new = stream_->read();
		// store events
		auto p = events_.lock();
		p->reserve(p->size() + events_new.size());
		for(auto e : events_new) {
			// transformation to match orientation in rig
			uint16_t ex = e.x;
			uint16_t ey = e.y;
			e.x = 127 - ey;
			e.y = 127 - ex;
			p->push_back(e);
			tf_[e.id].push(e);
		}
	}
}

void WdgtEbsCalibrate::threadOpenNi()
{
	while(is_running_) {
		// wait for next frame
		openni_->tick();
		bool prepare_frame = false;

		// get calibration
		ebs::Calibration calib = *calib_.lock();

		// process color
		if(openni_->color_changed) {
			openni_->color_changed = false;
			*img_color_.lock() = openni_->color_img;
			prepare_frame = true;
		}

		// process depth
		if(openni_->depth_changed) {
			openni_->depth_changed = false;
			// get depth image
			slimage::Image1ui16 depth = openni_->depth_img;
			// compute depth image
			{
				auto p = img_depth_.lock();
				p->resize(depth.width(), depth.height());
				for(size_t i=0; i<p->size(); i++) {
					(*p)[i] = static_cast<float>(depth[i])*0.001f;
				}
			}
			// compute dvs 0 depth
			*img_dvs_depth_.lock() = std::vector<slimage::Image1f> {
				ebs::ReprojectDepth(depth,
					calib.kinect, calib.dvs1, calib.kinect_to_dvs1),
				ebs::ReprojectDepth(depth,
					calib.kinect, calib.dvs2, calib.kinect_to_dvs2)
			};
			// compute dvs 0 disparity
			*img_dvs_disparity_.lock() = ebs::DepthToDisparity(depth, calib);
		}

		// process events
		if(prepare_frame) {
			// get frame events
			std::vector<Edvs::Event> events;
			{
				auto p = events_.lock();
				events = *p;
				p->resize(0);
			}
			if(!events.empty()) {

				// undistort events
				std::vector<ebs::CameraCalibration> cams{calib.dvs1, calib.dvs2};
				std::vector<Edvs::Event> events_orig = events;
	//				// FIXME re-enable undisort
	//				ebs::Undistort(events, cams);

				// get depth/disparity
				auto dvsdepth = *img_dvs_depth_.lock();
				// compute dvs 0 disparity
				auto dvsdisparity = *img_dvs_disparity_.lock();

				// annotate events
				std::vector<ebs::StereoEvent> events_frame;
				events_frame.reserve(events.size());
				for(size_t i=0; i<events.size(); i++) {
					const auto& e = events[i];
					if(e.id != 0 && e.id != 1) {
						continue;
					}
					ebs::StereoEvent ge;
					static_cast<Edvs::Event&>(ge) = e;
					ge.x = events_orig[i].x;
					ge.y = events_orig[i].y;
					ge.depth_gt = ebs::LookUpDepth(ge, dvsdepth[e.id]);
					ge.disparity_gt = ebs::LookUpDisparity(ge, dvsdisparity[e.id]);
					events_frame.push_back(ge);
				}

				// store events for visualization
				{
					auto p = events_vis_.lock();
					// add new
					p->insert(p->end(),
						events_frame.begin(), events_frame.end());
					// remove old
					*p = ebs::CloneEventRange(*p, VIS_DELTA);
				}

				// we have a new frame
				last_frame_ts_ = events.back().t;
			}
		}
	}
}

void WdgtEbsCalibrate::threadVisualize()
{
	uint64_t local_last_frame_ts = 0;
	while(is_running_) {
		if(local_last_frame_ts >= last_frame_ts_) {
			std::this_thread::sleep_for(std::chrono::milliseconds(10));
			continue;
		}
		local_last_frame_ts = last_frame_ts_;

		(*vis_.lock())["vis_color"] = ebs::QtConvert(
			*img_color_.lock());

		(*vis_.lock())["vis_depth"] = ebs::QtConvert(
			ebs::VisualizeDepthImage(*img_depth_.lock()));

		(*vis_.lock())["vis_dvs_depth"] = ebs::QtConvert(
			ebs::VisualizeDepthImage((*img_dvs_depth_.lock())[0]));

		(*vis_.lock())["vis_dvs_disparity"] = ebs::QtConvert(
			ebs::VisualizeDisparityImage((*img_dvs_disparity_.lock())[0]));

		auto events = *events_vis_.lock();

		(*vis_.lock())["vis_events_raw0"] = ebs::QtConvert(
			ebs::PlotEvents(events.begin(), events.end(), 128, 128,
				[](const ebs::StereoEvent& e) { return ebs::ColorizeParity(e.parity); },
				[](const ebs::StereoEvent& e) { return e.id == 0; },
				true));
		(*vis_.lock())["vis_events_raw1"] = ebs::QtConvert(
			ebs::PlotEvents(events.begin(), events.end(), 128, 128,
				[](const ebs::StereoEvent& e) { return ebs::ColorizeParity(e.parity); },
				[](const ebs::StereoEvent& e) { return e.id == 1; },
				true));
		(*vis_.lock())["vis_events_depth"] = ebs::QtConvert(
			ebs::PlotEvents(events.begin(), events.end(), 128, 128,
				[](const ebs::StereoEvent& e) { return ebs::ColorizeDepth(e.depth_gt); },
				[](const ebs::StereoEvent& e) { return e.id == 0; },
				true));
	}
}

void WdgtEbsCalibrate::threadCheckerboard()
{
	uint64_t local_last_frame_ts = 0;
	while(is_running_) {
		if(!is_collecting_calib_ || local_last_frame_ts >= last_frame_ts_) {
			std::this_thread::sleep_for(std::chrono::milliseconds(10));
			continue;
		}
		local_last_frame_ts = last_frame_ts_;

		// get data
		ebs::EventField fef1 = tf_[0];
		ebs::EventField fef2 = tf_[1];
		slimage::Image3ub fcol = *img_color_.lock();

		// detect checkerboard in dvs 1
		ebs::CheckerboardEvent cb_dvs1;
		if(fef1.num_events_ >= 100) {
			cb_dvs1 = ebs::DetectCheckerboardEvent(fef1);
		}
		// detect checkerboard in dvs 2
		ebs::CheckerboardEvent cb_dvs2;
		if(fef2.num_events_ >= 100) {
			cb_dvs2 = ebs::DetectCheckerboardEvent(fef2);
		}
		// detect checkerboard in openni
		ebs::CheckerboardDense cb_kinect;
		if(fcol.width() >= 320 && fcol.height() >= 240) {
			cb_kinect = ebs::DetectCheckerboardDense(fcol);
		}

		// append frame
		if(cb_dvs1.success && cb_dvs2.success && cb_kinect.success) {
			ebs::CalibrationFrame cb_frame;
			cb_frame.dvs1 = cb_dvs1;
			cb_frame.dvs2 = cb_dvs2;
			cb_frame.kinect = cb_kinect;
			auto p_cbdat = calib_data_.lock();
			p_cbdat->frames.push_back(cb_frame);
			if(p_cbdat->frames.size() % 10 == 0) {
				std::cout << "Calibration frames: " << p_cbdat->frames.size() << std::endl;
			}
		}

		// visualize
		(*vis_.lock())["vis_cb_dvs1"] = ebs::QtConvert(
			ebs::PlotCheckerboard(cb_dvs1));
		(*vis_.lock())["vis_cb_dvs2"] = ebs::QtConvert(
			ebs::PlotCheckerboard(cb_dvs2));
		// miv_->miv()->set("cb dvs cme", 
		// 	ebs::QtConvert(ebs::PlotCheckerboardCME(cb.dvs)));
		(*vis_.lock())["vis_cb_kinect"] = ebs::QtConvert(
			ebs::PlotCheckerboard(cb_kinect));

	}
}