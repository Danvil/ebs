#include <Edvs/EventIO.hpp>
#include <Edvs/EventStream.hpp>
#include <ebs/Calibration.hpp>
#include <Eigen/Dense>
#include <boost/program_options.hpp>
#include <iostream>

int main(int argc, char *argv[])
{
	std::vector<std::string> p_vuri;///dev/ttyUSB0?baudrate=4000000";
	std::string p_out = "/tmp/ebs_calib.txt";

	namespace po = boost::program_options;
	// Declare the supported options.
	po::options_description desc("Allowed options");
	desc.add_options()
		("help", "produce help message")
		("generic", "create generic calibration")
		("uris", po::value(&p_vuri)->multitoken(), "URI(s) to event stream(s)")
		("out", po::value(&p_out), "Filename where resulting calibration file is written")
	;

	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);

	if(vm.count("help")) {
		std::cout << desc << std::endl;
		return 0;
	}

	if(vm.count("generic")) {
		constexpr double BASELINE = 0.14;
		constexpr double H = 0.05;

		ebs::Calibration calib;

		calib.dvs1.intrinsic = ebs::CreateCameraMatrix(128,128,150);
		calib.dvs1.distortion = Eigen::VectorXd::Zero(5);

		calib.dvs2.intrinsic = ebs::CreateCameraMatrix(128,128,150);
		calib.dvs2.distortion = Eigen::VectorXd::Zero(5);

		calib.kinect.intrinsic = ebs::CreateCameraMatrix(320,240,520);
		calib.kinect.distortion = Eigen::VectorXd::Zero(5);

		calib.kinect_to_dvs1.R = Eigen::Matrix3d::Identity();
		calib.kinect_to_dvs1.T = Eigen::Vector3d{0,H,0};
		calib.kinect_to_dvs1.E = Eigen::Matrix3d::Identity();
		calib.kinect_to_dvs1.F = Eigen::Matrix3d::Identity();

		calib.kinect_to_dvs2.R = Eigen::Matrix3d::Identity();
		calib.kinect_to_dvs2.T = Eigen::Vector3d{BASELINE,H,0};
		calib.kinect_to_dvs2.E = Eigen::Matrix3d::Identity();
		calib.kinect_to_dvs2.F = Eigen::Matrix3d::Identity();

		calib.dvs1_to_dvs2.R = Eigen::Matrix3d::Identity();
		calib.dvs1_to_dvs2.T = Eigen::Vector3d{BASELINE,0,0};
		calib.dvs1_to_dvs2.E = Eigen::Matrix3d::Identity();
		calib.dvs1_to_dvs2.F = Eigen::Matrix3d::Identity();

		Save(calib, p_out);
		return 0;
	}

	// open stream
	auto stream = Edvs::OpenEventStream(p_vuri);

	// FIXME
	std::cerr << "NOT IMPLEMENTED - exiting" << std::endl;
	return 1;

	return 0;
}
