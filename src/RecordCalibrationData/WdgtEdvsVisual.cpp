#include "WdgtEdvsVisual.h"
#include <QFileDialog>
#include <QPainter>
#include <Edvs/EventIO.hpp>
#include <boost/interprocess/sync/scoped_lock.hpp>
#include <Eigen/Dense>
#include <iostream>
#include <fstream>
#include <time.h>

const unsigned int RetinaSize = 128;
const int cDecay = 24;
const int cDisplaySize = 512;
const int cUpdateInterval = 10;
const int cDisplayInterval = 20;

// event colors
const QRgb cColorMid = qRgb(96, 96, 96);
const QRgb cColor1 = qRgb(128, 0, 0);
const QRgb cColorFiltered1 = qRgb(255, 0, 0);
const QRgb cColor2 = qRgb(0, 64, 128);
const QRgb cColorFiltered2 = qRgb(0, 128, 255);

// // pulsing LED board
// const int DT_MUS = 1920;
// const int DT_MUS_TOL = 300;
// new LED boards
const int DT_MUS = 3731;
const int DT_MUS_TOL = 300;

struct RetinaLedTracker
{
private:
	static const uint32_t W = 128;
	uint8_t id;
	std::vector<uint64_t> mt1;
	std::vector<uint64_t> mt2;
	size_t num;
	Eigen::Vector2f pos;

public:
	RetinaLedTracker(uint8_t nid)
	: id(nid), mt1(W*W,0), mt2(W*W,0), num(0), pos(32.0f,32.0f) {}

	std::vector<Edvs::Event> update(const std::vector<Edvs::Event>& v) {
		std::vector<Edvs::Event> e = filter(v);
		track(e);
		return e;
	}

	const Eigen::Vector2f& position() const { return pos; }

	int px() const { return std::round(pos[0]); }
	int py() const { return std::round(pos[1]); }

	std::vector<Edvs::Event> filter(const std::vector<Edvs::Event>& v) {
		std::vector<Edvs::Event> filtered;
		filtered.reserve(v.size());
		for(const Edvs::Event& e : v) {
			if(e.id != id) continue;
			uint64_t& t1 = mt1[e.x + e.y*W];
			uint64_t& t2 = mt2[e.x + e.y*W];
			uint64_t dt1 = std::abs(static_cast<int64_t>(e.t - t1) - DT_MUS);
			uint64_t dt2 = std::abs(static_cast<int64_t>(t1 - t2) - DT_MUS);
			if(dt1 <= DT_MUS_TOL && dt2 <= DT_MUS_TOL) {
				filtered.push_back(e);
			}
			t2 = t1;
			t1 = e.t;
		}
		return filtered;
	}

	void track(const std::vector<Edvs::Event>& v) {
		const std::size_t n = v.size();
		if(n == 0) {
			return;
		}
		const float alpha0 = 0.05f;
		const float alpha1 = std::pow(1.0f - alpha0, n);
		const float alpha2 = 1.0f - alpha1;
		// std::cout << alpha0 << " " << alpha1 << " " << alpha2 << std::endl;
		Eigen::Vector2f mean = Eigen::Vector2f::Zero();
		for(const Edvs::Event& e : v) {
			mean[0] += static_cast<float>(e.x);
			mean[1] += static_cast<float>(e.y);
		}
		mean /= static_cast<float>(v.size());
		if(num == 0) {
			pos = mean;
		}
		// std::cout << pos.transpose() << std::endl;
		// std::cout << mean.transpose() << std::endl;
		pos = alpha1*pos + alpha2*mean;
		// std::cout << pos.transpose() << std::endl;
		num += n;
	}

};

RetinaLedTracker retina_led_tracker_1(0);
RetinaLedTracker retina_led_tracker_2(1);
struct CalibrationPoint {
	Eigen::Vector2f p1, p2;
};
std::vector<CalibrationPoint> calibration_points;

EdvsVisual::EdvsVisual(const Edvs::EventStream& dh1, const Edvs::EventStream& dh2, QWidget *parent)
    : QWidget(parent)
{
	ui.setupUi(this);

	image_ = QImage(RetinaSize, RetinaSize, QImage::Format_RGB32);

	connect(ui.pushButtonRecord, SIGNAL(clicked()), this, SLOT(OnButton()));
	is_recording_ = false;

	connect(&timer_update_, SIGNAL(timeout()), this, SLOT(Update()));
	timer_update_.setInterval(cUpdateInterval);
	timer_update_.start();

	connect(&timer_display_, SIGNAL(timeout()), this, SLOT(Display()));
	timer_display_.setInterval(cDisplayInterval);
	timer_display_.start();

	// start capture
	edvs_event_stream_1_ = dh1;
	edvs_event_capture_1_ = Edvs::EventCapture(edvs_event_stream_1_,
		boost::bind(&EdvsVisual::OnEvent, this, _1, 0));
	edvs_event_stream_2_ = dh2;
	edvs_event_capture_2_ = Edvs::EventCapture(edvs_event_stream_2_,
		boost::bind(&EdvsVisual::OnEvent, this, _1, 1));
}

EdvsVisual::~EdvsVisual()
{
}

void EdvsVisual::OnButton()
{
	if(ui.pushButtonRecord->isChecked()) {
		// start recording
		ui.pushButtonRecord->setText("Recording... (press to stop)");
		events_recorded_.clear();
		calibration_points.clear();
		is_recording_ = true;
	}
	else {
		// stop recording
		ui.pushButtonRecord->setText("Start recording");
		is_recording_ = false;
		// get filename
		QString fn = QFileDialog::getSaveFileName(this, "Select file to save recording");
		if(fn != "") {
			// save events
			Edvs::SaveEvents(fn.toStdString(), events_recorded_);
			std::cout << "Saved " << events_recorded_.size() << " events to file '" << fn.toStdString() << "'" << std::endl;
			// save calibration points
			{
				std::ofstream ofs(fn.toStdString() + "_path.tsv");
				for(const CalibrationPoint& c : calibration_points) {
					ofs << c.p1.x() << "\t" << c.p1.y() << "\t" << c.p2.x() << "\t" << c.p2.y() << std::endl;
				}
			}
			std::cout << "Saved " << calibration_points.size() << " calibration points" << std::endl;
		}
		events_recorded_.clear();
		calibration_points.clear();
	}
}

void EdvsVisual::OnEvent(const std::vector<Edvs::Event>& newevents, uint8_t id)
{
	std::vector<Edvs::Event> events_with_id = newevents;
	if(id > 0) {
		// change id
		for(Edvs::Event& e : events_with_id) {
			e.id = id;
		}
	}

	// filter
	auto ef1 = retina_led_tracker_1.update(events_with_id);
	auto ef2 = retina_led_tracker_2.update(events_with_id);

	// protect common vector with a mutex to avoid race conditions
	{
		boost::interprocess::scoped_lock<boost::mutex> lock(events_mtx_);
		events_.insert(events_.end(), events_with_id.begin(), events_with_id.end());
		events_filtered_.insert(events_filtered_.end(), ef1.begin(), ef1.end());
		events_filtered_.insert(events_filtered_.end(), ef2.begin(), ef2.end());
		if(is_recording_) {
			events_recorded_.insert(events_recorded_.end(), events_with_id.begin(), events_with_id.end());
		}
		// print time information
		if(!newevents.empty()) {
			static uint64_t last_time = 0;
			uint64_t current_time = newevents.back().t;
			if(current_time >= last_time + 1000000) {
				std::cout << static_cast<float>(current_time)/1000000.0f << std::endl;
				last_time = current_time;
			}
		}
	}
}

int DecayComponent(int current, int target, int decay)
{
	if(current-decay >= target) {
		return current - decay;
	}
	if(current+decay <= target) {
		return current + decay;
	}
	return target;
}

QRgb DecayColor(QRgb color, QRgb target, int decay)
{
	return qRgb(
		DecayComponent(qRed(color), qRed(target), cDecay),
		DecayComponent(qGreen(color), qGreen(target), cDecay),
		DecayComponent(qBlue(color), qBlue(target), cDecay)
	);
}

void EdvsVisual::Update()
{
	// write events
	{
		boost::interprocess::scoped_lock<boost::mutex> lock(events_mtx_);
		for(const Edvs::Event& e : events_) {
			image_.setPixel(e.x, 127-e.y,
				(e.id == 0) ? cColor1 : cColor2
			);
		}
		events_.clear();
		for(const Edvs::Event& e : events_filtered_) {
			image_.setPixel(e.x, 127-e.y,
				(e.id == 0) ? cColorFiltered1 : cColorFiltered2
			);
		}
		events_filtered_.clear();
	}
	if(is_recording_) {
		CalibrationPoint pe;
		pe.p1 = retina_led_tracker_1.position();
		pe.p2 = retina_led_tracker_2.position();
		calibration_points.push_back(pe);
	}
}

void EdvsVisual::Display()
{
	constexpr int scl = cDisplaySize / RetinaSize;
	static QPen pen1(Qt::black,scl);
	static QPen pen2(Qt::black,scl);
	// apply decay
	unsigned int* bits = (unsigned int*)image_.bits();
	const unsigned int N = image_.height() * image_.width();
	for(int i=0; i<N; i++, bits++) {
		*bits = DecayColor(*bits, cColorMid, cDisplayInterval*cDecay);
	}
	// rescale so that we see more :) and display
	QImage img = image_.scaled(cDisplaySize, cDisplaySize);
	QPainter pnt(&img);
	pnt.setPen(pen1);
	pnt.drawEllipse(scl*(retina_led_tracker_1.px()-1), scl*(127-retina_led_tracker_1.py()-1), scl*3, scl*3);
	pnt.setPen(pen2);
	pnt.drawEllipse(scl*(retina_led_tracker_2.px()-1), scl*(127-retina_led_tracker_2.py()-1), scl*3, scl*3);
	ui.label->setPixmap(QPixmap::fromImage(img));
}
