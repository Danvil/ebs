#include "WdgtEdvsCalibrate.h"
#include <Edvs/EventIO.hpp>
#include <QtGui>
#include <QApplication>
#include <boost/program_options.hpp>
#include <iostream>

int main(int argc, char *argv[])
{
	std::vector<std::string> p_vuri;///dev/ttyUSB0?baudrate=4000000";

	// get command line parameters
	namespace po = boost::program_options;
	po::options_description desc("Allowed options");
	desc.add_options()
		("help", "produce help message")
		("uris", po::value(&p_vuri)->multitoken(), "URI(s) to event stream(s)")
	;
	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);
	if(vm.count("help")) {
		std::cout << desc << std::endl;
		return 1;
	}

	// load events
	auto stream = Edvs::OpenEventStream(p_vuri);

	// start gui
	QApplication a(argc, argv);
	WdgtEdvsCalibrate w(stream);
	w.show();

	return a.exec();
}
