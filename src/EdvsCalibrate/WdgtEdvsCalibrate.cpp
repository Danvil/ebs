#include "WdgtEdvsCalibrate.h"
#include <ebs/Flow.hpp>
#include <ebs/Calibration.hpp>
#include <ebs/Tools.hpp>
#include <ebs/QtHelpers.hpp>
#include <Edvs/EventIO.hpp>
#include <boost/format.hpp>
#include <slimage/qt.hpp>
#include <slimage/io.hpp>
#include <QFileDialog>
#include <sstream>
#include <chrono>
#include <thread>

//#define DEBUG_VIS

constexpr unsigned UPDATE_DT_VIS = 50;
constexpr bool SHOW_3D_POINTS = true;
constexpr uint64_t VIS_DELTA = 200000;
constexpr float EDVS_FOCAL_GUESS = 150.0f;
constexpr unsigned FLOW_RADIUS = 2;
constexpr float FLOW_SIGMA = 0.1f;

slimage::Image3ub PlotRaw(const std::vector<Edvs::Event>& v, uint8_t id)
{ return ebs::PlotEventsById(v.begin(), v.end(), 128, 128, id, true); }

slimage::Image3ub PlotStereo(const std::vector<ebs::StereoEvent>& v, ebs::VisMode mode)
{ return ebs::PlotStereoEvents(v.begin(), v.end(), 128, 128, mode, true); }

slimage::Image3ub PlotField(const ebs::EventField& f, ebs::VisMode mode)
{ return ebs::PlotEventField(f, mode); }

WdgtEdvsCalibrate::WdgtEdvsCalibrate(const std::shared_ptr<Edvs::IEventStream>& stream, QWidget* parent)
: stream_(stream)
{
	ui.setupUi(this);

	ui.toolBar->setVisible(false);

	//showMaximized();
	this->resize(1200,600);

	miv_.reset(new miv::WdgtMdiView{});
	setCentralWidget(miv_.get());

	QObject::connect(&timer_vis_, SIGNAL(timeout()), this, SLOT(TickVis()));
	timer_vis_.setInterval(UPDATE_DT_VIS);
	timer_vis_.start();

	QObject::connect(ui.actionCalibRun, SIGNAL(triggered()), this, SLOT(ActCalibRun()));
	QObject::connect(ui.actionCalibSave, SIGNAL(triggered()), this, SLOT(ActCalibSave()));
	QObject::connect(ui.actionCalibLoad, SIGNAL(triggered()), this, SLOT(ActCalibLoad()));

	// start threads
	has_calib_ = false;
	is_running_ = true;
	thread_dvs_ = std::thread(&WdgtEdvsCalibrate::threadDvs, this);
	thread_vis_ = std::thread(&WdgtEdvsCalibrate::threadVisualize, this);
	thread_cb_ = std::thread(&WdgtEdvsCalibrate::threadCheckerboard, this);
}

WdgtEdvsCalibrate::~WdgtEdvsCalibrate()
{
	is_running_ = false;
	thread_dvs_.join();
	thread_vis_.join();
	thread_cb_.join();
}

void WdgtEdvsCalibrate::closeEvent(QCloseEvent* event)
{
	miv_->close();
}

void WdgtEdvsCalibrate::TickVis()
{
	for(const auto& p : *vis_.lock()) {
		miv_->miv()->set(p.first, p.second);
	}
}

void WdgtEdvsCalibrate::ActCalibRun()
{
	auto cc = ebs::CalibrateCamera(*frames_.lock(), EDVS_FOCAL_GUESS);
	*calib_.lock() = cc;
	has_calib_ = true;
	std::cout << cc << std::endl;
}

void WdgtEdvsCalibrate::ActCalibSave()
{
	std::string fn = "/tmp/edvs_calib.tsv";
	std::cout << "Saving calibration to '" << fn << "'" << std::endl;
	ebs::Save(*calib_.lock(), fn);
}

void WdgtEdvsCalibrate::ActCalibLoad()
{
	std::string fn = "/tmp/edvs_calib.tsv";
	std::cout << "Loading calibration from '" << fn << "'" << std::endl;
	auto p = calib_.lock();
	ebs::Load(fn, *p);
	has_calib_ = true;
	std::cout << *p << std::endl;
}

void WdgtEdvsCalibrate::threadDvs()
{
	std::vector<edvs_event_t> events;
	std::vector<UndistortedEvent> events_all;

	while(is_running_) {
		// read events
		std::vector<edvs_event_t> events_new = stream_->read();
		if(events_new.size() == 0) {
			std::this_thread::sleep_for(std::chrono::microseconds(100));
			continue;
		}
		// store events
		events.reserve(events.size() + events_new.size());
		for(auto e : events_new) {
			// ignore everything except id=0
			if(e.id != 0) {
				continue;
			}
			// transformation to match orientation in rig
			uint16_t ex = e.x;
			uint16_t ey = e.y;
			e.x = 127 - ey;
			e.y = 127 - ex;
			events.push_back(e);
			tf_.push(e);
			// // flow
			// tf_.at(e.x,e.y).flow = ComputeEventFlow(e, tf_, FLOW_RADIUS, FLOW_SIGMA);
		}

		// undistort events
		std::vector<ebs::CameraCalibration> cams{*calib_.lock()};
		std::vector<Edvs::Event> events_undist = events;
		if(has_calib_) {
			ebs::Undistort(events_undist, cams, true);
		}

		std::vector<Edvs::Event> events_nornd = events;
		if(has_calib_) {
			ebs::Undistort(events_nornd, cams, false);
		}

		// annotate events
		std::vector<UndistortedEvent> events_frame;
		events_frame.reserve(events.size());
		for(size_t i=0; i<events.size(); i++) {
			UndistortedEvent ge;
			static_cast<Edvs::Event&>(ge) = events[i];
			ge.xm = events_undist[i].x;
			ge.ym = events_undist[i].y;
			ge.xm_nornd = events_nornd[i].x;
			ge.ym_nornd = events_nornd[i].y;
			events_frame.push_back(ge);
		}
		events.resize(0);

		events_all.insert(events_all.end(), events_frame.begin(), events_frame.end());

		// store events for visualization
		{
			auto p = events_vis_.lock();
			// add new
			p->insert(p->end(),
				events_frame.begin(), events_frame.end());
			// remove old
			*p = ebs::CloneEventRange(*p, VIS_DELTA);
		}
	}

#ifdef DEBUG_VIS
	// save all events
	std::ofstream ofs("/tmp/edvs_events.tsv");
	for(const auto& e : events_all) {
		ofs << e.t
			<< "\t" << e.x  << "\t" << e.y
			<< "\t" << (int)e.parity
			<< "\t" << (int)e.id
			<< "\t" << e.xm << "\t" << e.ym
			<< "\t" << e.xm_nornd << "\t" << e.ym_nornd
			<< "\n";
	}
#endif
}

void WdgtEdvsCalibrate::threadVisualize()
{
	slimage::Image1ub img_nornd_mask(128,128);
	img_nornd_mask.fill(slimage::Pixel1ub{1});

	while(is_running_) {
		std::this_thread::sleep_for(std::chrono::milliseconds(20));

		auto events = *events_vis_.lock();

		(*vis_.lock())["Events"] = ebs::QtConvert(
			ebs::PlotEvents(events.begin(), events.end(), 128, 128,
				[](const edvs_event_t& e) { return ebs::ColorizeParity(e.parity); },
				[](const edvs_event_t& e) { return true; },
				true));

		{
			auto events_m = events;
			for(auto& e : events_m) {
				e.x = e.xm;
				e.y = e.ym;
			}
			(*vis_.lock())["Events (undist)"] = ebs::QtConvert(
				ebs::PlotEvents(events_m.begin(), events_m.end(), 128, 128,
					[](const UndistortedEvent& e) { return ebs::ColorizeParity(e.parity); },
					[](const UndistortedEvent& e) { return e.id == 0; },
					true));
		}

		// {
		// 	auto events_m = events;
		// 	for(auto& e : events_m) {
		// 		e.x = e.xm_nornd;
		// 		e.y = e.ym_nornd;
		// 	}
		// 	auto img = ebs::PlotEvents(events_m.begin(), events_m.end(), 128, 128,
		// 			[](const UndistortedEvent& e) { return ebs::ColorizeParity(e.parity); },
		// 			[](const UndistortedEvent& e) { return e.id == 0; },
		// 			true);
		// 	for(size_t i=0; i<img.size(); i++) {
		// 		slimage::Pixel3ub col = img[i];
		// 		if(col[0] != 128) {
		// 			img_nornd_mask[i] = slimage::Pixel1ub{0};
		// 		}
		// 		if(img_nornd_mask[i] == 1) {
		// 			img[i] = slimage::Pixel3ub{255,0,0};
		// 		}
		// 	}
		// 	(*vis_.lock())["vis_events_m_nornd"] = ebs::QtConvert(img);
		// }

		(*vis_.lock())["Event Time Field"] = ebs::QtConvert(
			ebs::PlotEventField(tf_, ebs::VisMode::Time));
		(*vis_.lock())["Event Polarity Field"] = ebs::QtConvert(
			ebs::PlotEventField(tf_, ebs::VisMode::Parity));
	}
}

void SaveMatrix(const std::string& fn, const Eigen::MatrixXf& m)
{
	std::ofstream ofs(fn);
	for(unsigned i=0; i<m.rows(); i++) {
		for(unsigned j=0; j<m.cols(); j++) {
			ofs << m(i,j);
			if(j+1 < m.cols()) {
				ofs << "\t";
			}
			else {
				ofs<< "\n";
			}
		}
	}
}

void WdgtEdvsCalibrate::threadCheckerboard()
{
	std::ofstream ofs("/tmp/edvs_corners.tsv");
	boost::format fmt("/tmp/cme/%09d.mat");
	boost::format fmt2("/tmp/cme/%09d_%02d.mat");
	auto last_frame_add = std::chrono::system_clock::now();

	while(is_running_) {
//		std::this_thread::sleep_for(std::chrono::milliseconds(20));

		// get event field
		ebs::EventField fef = tf_;
		auto events = *events_vis_.lock();
		if(fef.num_events_ <= 100 || events.empty()) {
			continue;
		}
		uint64_t event_time = events.back().t;

		// detect checkerboard in dvs
		ebs::CheckerboardEvent cb_dvs = ebs::DetectCheckerboardEvent(fef,
#ifdef DEBUG_VIS
			true);
#else
			false);
#endif
		if(cb_dvs.success) {
			auto current = std::chrono::system_clock::now();
			if(current - last_frame_add > std::chrono::duration<double>(0.5)) {
				last_frame_add = current;
				*frame_last_.lock() = cb_dvs;
				auto p = frames_.lock();
				p->push_back(cb_dvs);
				if(p->size() % 10 == 0) {
					std::cout << "Calibration frames: " << p->size() << std::endl;
				}
			}
#ifdef DEBUG_VIS
			// store corner points
			ofs << event_time;
			for(const auto& p : cb_dvs.corners) {
				ofs << "\t" << p[0] << "\t" << p[1];
			}
			ofs << "\n";
			// export corner prob
			SaveMatrix((fmt % event_time).str(), cb_dvs.dbg_cme);
			for(unsigned i=0; i<cb_dvs.dbg.size(); i++) {
				SaveMatrix((fmt2 % event_time % i).str(), cb_dvs.dbg[i]);
			}
#endif
		}

		// visualize
		auto vis_events_cb = ebs::PlotEvents(events.begin(), events.end(), 128, 128,
				[](const edvs_event_t& e) { return ebs::ColorizeParity(e.parity); },
				[](const edvs_event_t& e) { return true; },
				true);
		ebs::PlotCheckerboard(vis_events_cb, cb_dvs);
		(*vis_.lock())["Checkerboard"] = ebs::QtConvert(vis_events_cb);

		// (*vis_.lock())["vis_cb"] = ebs::QtConvert(
		// 	ebs::PlotCheckerboard(cb_dvs));

		(*vis_.lock())["Corner Detector"] = ebs::QtConvert(
			ebs::PlotCheckerboardCME(cb_dvs));
	}
}