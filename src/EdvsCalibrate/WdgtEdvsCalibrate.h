#ifndef EBSVIS_WDGTEDVSCALIBRATE_H
#define EBSVIS_WDGTEDVSCALIBRATE_H

#include "ui_WdgtEdvsCalibrate.h"
#include <ebs/EventField.hpp>
#include <ebs/Visualization.hpp>
#include <ebs/Calibration.hpp>
#include <Edvs/EventStream.hpp>
#include <miv/WdgtMdiView.h>
#include <da/synchronized.hpp>
#include <QtGui/QWidget>
#include <QtGui/QImage>
#include <QtCore/QTimer>
#include <thread>
#include <atomic>
#include <map>
#include <string>

class WdgtEdvsCalibrate : public QMainWindow
{
	Q_OBJECT

public:
	WdgtEdvsCalibrate(const std::shared_ptr<Edvs::IEventStream>& stream, QWidget* parent = 0);
	~WdgtEdvsCalibrate();

	void closeEvent(QCloseEvent* event);

protected Q_SLOTS:
	void TickVis();
	void ActCalibRun();
	void ActCalibSave();
	void ActCalibLoad();

private:
	void threadDvs();
	void threadVisualize();
	void threadCheckerboard();

private:
	Ui::WdgtEdvsCalibrateClass ui;

	std::unique_ptr<miv::WdgtMdiView> miv_;

	QTimer timer_vis_;

	std::shared_ptr<Edvs::IEventStream> stream_;

	ebs::EventField tf_;

//	std::unique_ptr<VisualizationPipeline> ebs_gui_vis_;

//	std::unique_ptr<qs::Scheduler> g_scheduler;

	bool is_running_;
	std::thread thread_dvs_;
	std::thread thread_vis_;
	std::thread thread_cb_;

	struct UndistortedEvent : public Edvs::Event
	{
		uint16_t xm, ym;
		uint16_t xm_nornd, ym_nornd;
	};

	da::synchronized<std::vector<UndistortedEvent>> events_vis_;

	// data
	da::synchronized<ebs::CheckerboardEvent> frame_last_;
	da::synchronized<std::vector<ebs::Checkerboard>> frames_;
	da::synchronized<ebs::CameraCalibration> calib_;
	std::atomic<bool> has_calib_;

	da::synchronized<std::map<std::string,QImage>> vis_;

};

#endif
