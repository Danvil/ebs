#include <qs/OpenNI.hpp>
#include <iostream>

namespace qs {

OpenNISource::OpenNISource()
{
	const char* deviceURI = openni::ANY_DEVICE;
	openni::Status rc = openni::STATUS_OK;

	rc = openni::OpenNI::initialize();

	printf("After initialization:\n%s\n", openni::OpenNI::getExtendedError());

	rc = device.open(deviceURI);
	if (rc != openni::STATUS_OK)
	{
		printf("SimpleViewer: Device open failed:\n%s\n", openni::OpenNI::getExtendedError());
		openni::OpenNI::shutdown();
		throw 1;
	}

	rc = depth.create(device, openni::SENSOR_DEPTH);
	if (rc == openni::STATUS_OK)
	{
		rc = depth.start();
		if (rc != openni::STATUS_OK)
		{
			printf("SimpleViewer: Couldn't start depth stream:\n%s\n", openni::OpenNI::getExtendedError());
			depth.destroy();
		}
	}
	else
	{
		printf("SimpleViewer: Couldn't find depth stream:\n%s\n", openni::OpenNI::getExtendedError());
	}

	rc = color.create(device, openni::SENSOR_COLOR);
	if (rc == openni::STATUS_OK)
	{
		rc = color.start();
		if (rc != openni::STATUS_OK)
		{
			printf("SimpleViewer: Couldn't start color stream:\n%s\n", openni::OpenNI::getExtendedError());
			color.destroy();
		}
	}
	else
	{
		printf("SimpleViewer: Couldn't find color stream:\n%s\n", openni::OpenNI::getExtendedError());
	}

	if (!depth.isValid() || !color.isValid())
	{
		printf("SimpleViewer: No valid streams. Exiting\n");
		openni::OpenNI::shutdown();
		throw 2;
	}

	openni::VideoMode depthVideoMode;
	openni::VideoMode colorVideoMode;
	int m_width, m_height;

	if (depth.isValid() && color.isValid())
	{
		depthVideoMode = depth.getVideoMode();
		colorVideoMode = color.getVideoMode();

		int depthWidth = depthVideoMode.getResolutionX();
		int depthHeight = depthVideoMode.getResolutionY();
		int colorWidth = colorVideoMode.getResolutionX();
		int colorHeight = colorVideoMode.getResolutionY();

		if (depthWidth == colorWidth &&
			depthHeight == colorHeight)
		{
			m_width = depthWidth;
			m_height = depthHeight;
		}
		else
		{
			printf("Error - expect color and depth to be in same resolution: D: %dx%d, C: %dx%d\n",
				depthWidth, depthHeight,
				colorWidth, colorHeight);
			throw openni::STATUS_ERROR;
		}
	}
	else if (depth.isValid())
	{
		depthVideoMode = depth.getVideoMode();
		m_width = depthVideoMode.getResolutionX();
		m_height = depthVideoMode.getResolutionY();
	}
	else if (color.isValid())
	{
		colorVideoMode = color.getVideoMode();
		m_width = colorVideoMode.getResolutionX();
		m_height = colorVideoMode.getResolutionY();
	}
	else
	{
		printf("Error - expects at least one of the streams to be valid...\n");
		throw openni::STATUS_ERROR;
	}

	{
		openni::Status status = device.setImageRegistrationMode(openni::IMAGE_REGISTRATION_DEPTH_TO_COLOR);
		if(status != openni::STATUS_OK) {
			std::cout << "Failed to set Depth->Color registration" << std::endl;
		}
	}

	{
		openni::Status status = device.setDepthColorSyncEnabled(true);
		if(status != openni::STATUS_OK) {
			std::cout << "Failed to set Depth/Color synchronization" << std::endl;
		}
	}



	color_changed = false;
	depth_changed = false;
}

void OpenNISource::tick()
{
	std::vector<openni::VideoStream*> m_streams{
		&depth, &color
	};

	int changedIndex;
	openni::Status rc = openni::OpenNI::waitForAnyStream(m_streams.data(), 2, &changedIndex);
	if (rc != openni::STATUS_OK)
	{
		printf("Wait failed\n");
		return;
	}

	if(changedIndex == 0) {
		openni::VideoFrameRef m_depthFrame;
		depth.readFrame(&m_depthFrame);
		if(m_depthFrame.isValid())
			forwardDepth(m_depthFrame);
	}
	else if(changedIndex == 1) {
		openni::VideoFrameRef m_colorFrame;
		color.readFrame(&m_colorFrame);
		if(m_colorFrame.isValid())
			forwardColor(m_colorFrame);
	}
	else {
		printf("Error in wait\n");
	}

}

OpenNISource::~OpenNISource()
{
//	openni::OpenNI::shutdown();
}

void OpenNISource::forwardDepth(const openni::VideoFrameRef& m_depthFrame)
{
	int m_nTexMapX = m_depthFrame.getWidth();
	int m_nTexMapY = m_depthFrame.getHeight();

	depth_img = slimage::Image1ui16{m_nTexMapX, m_nTexMapY};
	uint16_t* m_pTexMap = depth_img.buffer().begin();
	memset(m_pTexMap, 0, m_nTexMapX*m_nTexMapY*sizeof(uint16_t));

	const openni::DepthPixel* pDepthRow = (const openni::DepthPixel*)m_depthFrame.getData();
	uint16_t* pTexRow = m_pTexMap + m_depthFrame.getCropOriginY() * m_nTexMapX;
	int rowSize = m_depthFrame.getStrideInBytes() / sizeof(openni::DepthPixel);

	for (int y = 0; y < m_depthFrame.getHeight(); ++y)
	{
		const openni::DepthPixel* pDepth = pDepthRow;
		uint16_t* pTex = pTexRow + m_depthFrame.getCropOriginX();

		for (int x = 0; x < m_depthFrame.getWidth(); ++x, ++pDepth, ++pTex)
		{
			*pTex = *pDepth;
		}

		pDepthRow += rowSize;
		pTexRow += m_nTexMapX;
	}
	
	depth_changed = true;
}

void OpenNISource::forwardColor(const openni::VideoFrameRef& m_colorFrame)
{
	int m_nTexMapX = m_colorFrame.getWidth();
	int m_nTexMapY = m_colorFrame.getHeight();

	color_img = slimage::Image3ub{m_nTexMapX, m_nTexMapY};
	openni::RGB888Pixel* m_pTexMap = reinterpret_cast<openni::RGB888Pixel*>(color_img.buffer().begin());
	memset(m_pTexMap, 0, m_nTexMapX*m_nTexMapY*sizeof(openni::RGB888Pixel));

	const openni::RGB888Pixel* pImageRow = (const openni::RGB888Pixel*)m_colorFrame.getData();
	openni::RGB888Pixel* pTexRow = m_pTexMap + m_colorFrame.getCropOriginY() * m_nTexMapX;
	int rowSize = m_colorFrame.getStrideInBytes() / sizeof(openni::RGB888Pixel);

	for (int y = 0; y < m_nTexMapY; ++y)
	{
		const openni::RGB888Pixel* pImage = pImageRow;
		openni::RGB888Pixel* pTex = pTexRow + m_colorFrame.getCropOriginX();

		for (int x = 0; x < m_nTexMapX; ++x, ++pImage, ++pTex)
		{
			*pTex = *pImage;
		}

		pImageRow += rowSize;
		pTexRow += m_nTexMapX;
	}

	color_changed = true;
}

}
