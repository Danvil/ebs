
#include "tri.h"

float Power(float x, int q) {
	if(q == 2) {
		return x*x;
	}
	else if(q == 4) {
		x *= x;
		return x*x;
	}
	else {
		// ERROR
		return 0.0f;
	}
}

void tri_triangulate(
	const tri_params_t* p,
	float uu1, float vv1, float uu2, float vv2,
	float* rx, float* ry, float* rz)
{
	float f1 = p->f1;
	float cx1 = p->cx1;
	float cy1 = p->cy1;
	float f2 = p->f2;
	float cx2 = p->cx2;
	float cy2 = p->cy2;
	float tx = p->tx;
	float ty = p->ty;
	float tz = p->tz;
	float r11 = p->r11;
	float r12 = p->r12;
	float r13 = p->r13;
	float r21 = p->r21;
	float r22 = p->r22;
	float r23 = p->r23;
	float r31 = p->r31;
	float r32 = p->r32;
	float r33 = p->r33;

	// the following code are template for mathematica
	// use splice to replace them

	float lam = -((-(((2*cx2*Power(f1,2)*f2*r13 + 2*cy2*Power(f1,2)*f2*r23 - 
              2*Power(f1,2)*Power(f2,2)*r33 + 
              2*f1*Power(f2,2)*r31*(cx1 - uu1) - 
              2*f1*f2*r11*(cx1 - uu1)*(cx2 - uu2) - 
              2*Power(f1,2)*f2*r13*uu2 + 2*f1*Power(f2,2)*r32*(cy1 - vv1) - 
              2*f1*f2*r12*(cx2 - uu2)*(cy1 - vv1) - 
              2*f1*f2*r21*(cx1 - uu1)*(cy2 - vv2) - 
              2*f1*f2*r22*(cy1 - vv1)*(cy2 - vv2) - 2*Power(f1,2)*f2*r23*vv2)*
            (2*cx2*Power(f1,2)*f2*tx + 2*cy2*Power(f1,2)*f2*ty - 
              2*Power(f1,2)*Power(f2,2)*tz - 2*Power(f1,2)*f2*tx*uu2 - 
              2*Power(f1,2)*f2*ty*vv2))/(Power(f1,4)*Power(f2,4))) + 
       (2*(2*Power(f1,2)*Power(f2,2)*r13*tx + 
            2*Power(f1,2)*Power(f2,2)*r23*ty + 
            2*Power(f1,2)*Power(f2,2)*r33*tz - 
            2*f1*Power(f2,2)*r11*tx*(cx1 - uu1) - 
            2*f1*Power(f2,2)*r21*ty*(cx1 - uu1) - 
            2*f1*Power(f2,2)*r31*tz*(cx1 - uu1) - 
            2*f1*Power(f2,2)*r12*tx*(cy1 - vv1) - 
            2*f1*Power(f2,2)*r22*ty*(cy1 - vv1) - 
            2*f1*Power(f2,2)*r32*tz*(cy1 - vv1))*
          (Power(f1,2)*Power(f2,2) + 
            Power(f1,2)*(Power(cx2,2) + Power(cy2,2) - 2*cx2*uu2 + 
               Power(uu2,2) - 2*cy2*vv2 + Power(vv2,2))))/
        (Power(f1,4)*Power(f2,4)))/
     (-(Power(2*cx2*Power(f1,2)*f2*r13 + 2*cy2*Power(f1,2)*f2*r23 - 
            2*Power(f1,2)*Power(f2,2)*r33 + 
            2*f1*Power(f2,2)*r31*(cx1 - uu1) - 
            2*f1*f2*r11*(cx1 - uu1)*(cx2 - uu2) - 2*Power(f1,2)*f2*r13*uu2 + 
            2*f1*Power(f2,2)*r32*(cy1 - vv1) - 
            2*f1*f2*r12*(cx2 - uu2)*(cy1 - vv1) - 
            2*f1*f2*r21*(cx1 - uu1)*(cy2 - vv2) - 
            2*f1*f2*r22*(cy1 - vv1)*(cy2 - vv2) - 2*Power(f1,2)*f2*r23*vv2,2)/
          (Power(f1,4)*Power(f2,4))) + 
       (4*(Power(f1,2)*Power(f2,2) + 
            Power(f2,2)*(Power(cx1,2) + Power(cy1,2) - 2*cx1*uu1 + 
               Power(uu1,2) - 2*cy1*vv1 + Power(vv1,2)))*
          (Power(f1,2)*Power(f2,2) + 
            Power(f1,2)*(Power(cx2,2) + Power(cy2,2) - 2*cx2*uu2 + 
               Power(uu2,2) - 2*cy2*vv2 + Power(vv2,2))))/
        (Power(f1,4)*Power(f2,4))));
	// printf("lam=%f\n", lam);

	float mu = -((cx2*f2*tx + cy2*f2*ty - Power(f2,2)*tz - f2*tx*uu2 - f2*ty*vv2)/
      (Power(cx2,2) + Power(cy2,2) + Power(f2,2) - 2*cx2*uu2 + Power(uu2,2) - 
        2*cy2*vv2 + Power(vv2,2))) + 
   ((2*cx2*Power(f1,2)*f2*r13 + 2*cy2*Power(f1,2)*f2*r23 - 
        2*Power(f1,2)*Power(f2,2)*r33 + 2*f1*Power(f2,2)*r31*(cx1 - uu1) - 
        2*f1*f2*r11*(cx1 - uu1)*(cx2 - uu2) - 2*Power(f1,2)*f2*r13*uu2 + 
        2*f1*Power(f2,2)*r32*(cy1 - vv1) - 
        2*f1*f2*r12*(cx2 - uu2)*(cy1 - vv1) - 
        2*f1*f2*r21*(cx1 - uu1)*(cy2 - vv2) - 
        2*f1*f2*r22*(cy1 - vv1)*(cy2 - vv2) - 2*Power(f1,2)*f2*r23*vv2)*
      (-(((2*cx2*Power(f1,2)*f2*r13 + 2*cy2*Power(f1,2)*f2*r23 - 
               2*Power(f1,2)*Power(f2,2)*r33 + 
               2*f1*Power(f2,2)*r31*(cx1 - uu1) - 
               2*f1*f2*r11*(cx1 - uu1)*(cx2 - uu2) - 
               2*Power(f1,2)*f2*r13*uu2 + 2*f1*Power(f2,2)*r32*(cy1 - vv1) - 
               2*f1*f2*r12*(cx2 - uu2)*(cy1 - vv1) - 
               2*f1*f2*r21*(cx1 - uu1)*(cy2 - vv2) - 
               2*f1*f2*r22*(cy1 - vv1)*(cy2 - vv2) - 2*Power(f1,2)*f2*r23*vv2)
              *(2*cx2*Power(f1,2)*f2*tx + 2*cy2*Power(f1,2)*f2*ty - 
               2*Power(f1,2)*Power(f2,2)*tz - 2*Power(f1,2)*f2*tx*uu2 - 
               2*Power(f1,2)*f2*ty*vv2))/(Power(f1,4)*Power(f2,4))) + 
        (2*(2*Power(f1,2)*Power(f2,2)*r13*tx + 
             2*Power(f1,2)*Power(f2,2)*r23*ty + 
             2*Power(f1,2)*Power(f2,2)*r33*tz - 
             2*f1*Power(f2,2)*r11*tx*(cx1 - uu1) - 
             2*f1*Power(f2,2)*r21*ty*(cx1 - uu1) - 
             2*f1*Power(f2,2)*r31*tz*(cx1 - uu1) - 
             2*f1*Power(f2,2)*r12*tx*(cy1 - vv1) - 
             2*f1*Power(f2,2)*r22*ty*(cy1 - vv1) - 
             2*f1*Power(f2,2)*r32*tz*(cy1 - vv1))*
           (Power(f1,2)*Power(f2,2) + 
             Power(f1,2)*(Power(cx2,2) + Power(cy2,2) - 2*cx2*uu2 + 
                Power(uu2,2) - 2*cy2*vv2 + Power(vv2,2))))/
         (Power(f1,4)*Power(f2,4))))/
    (2.*(Power(f1,2)*Power(f2,2) + 
        Power(f1,2)*(Power(cx2,2) + Power(cy2,2) - 2*cx2*uu2 + Power(uu2,2) - 
           2*cy2*vv2 + Power(vv2,2)))*
      (-(Power(2*cx2*Power(f1,2)*f2*r13 + 2*cy2*Power(f1,2)*f2*r23 - 
             2*Power(f1,2)*Power(f2,2)*r33 + 
             2*f1*Power(f2,2)*r31*(cx1 - uu1) - 
             2*f1*f2*r11*(cx1 - uu1)*(cx2 - uu2) - 2*Power(f1,2)*f2*r13*uu2 + 
             2*f1*Power(f2,2)*r32*(cy1 - vv1) - 
             2*f1*f2*r12*(cx2 - uu2)*(cy1 - vv1) - 
             2*f1*f2*r21*(cx1 - uu1)*(cy2 - vv2) - 
             2*f1*f2*r22*(cy1 - vv1)*(cy2 - vv2) - 2*Power(f1,2)*f2*r23*vv2,2)
            /(Power(f1,4)*Power(f2,4))) + 
        (4*(Power(f1,2)*Power(f2,2) + 
             Power(f2,2)*(Power(cx1,2) + Power(cy1,2) - 2*cx1*uu1 + 
                Power(uu1,2) - 2*cy1*vv1 + Power(vv1,2)))*
           (Power(f1,2)*Power(f2,2) + 
             Power(f1,2)*(Power(cx2,2) + Power(cy2,2) - 2*cx2*uu2 + 
                Power(uu2,2) - 2*cy2*vv2 + Power(vv2,2))))/
         (Power(f1,4)*Power(f2,4))));
	// printf("mu=%f\n", mu);

	float p1x = -((cx1*lam - lam*uu1)/f1);
	float p1y = -((cy1*lam - lam*vv1)/f1);
	float p1z = lam;
	// printf("p1=(%f,%f,%f)\n", p1x,p1y,p1z);

	float p2x = -((-(f2*mu*r13*r22) + f2*mu*r12*r23 - cy2*mu*r13*r32 + cx2*mu*r23*r32 + 
       cy2*mu*r12*r33 - cx2*mu*r22*r33 + f2*r23*r32*tx - f2*r22*r33*tx - 
       f2*r13*r32*ty + f2*r12*r33*ty + f2*r13*r22*tz - f2*r12*r23*tz - 
       mu*r23*r32*uu2 + mu*r22*r33*uu2 + mu*r13*r32*vv2 - mu*r12*r33*vv2)/
     (f2*(r13*r22*r31 - r12*r23*r31 - r13*r21*r32 + r11*r23*r32 + 
         r12*r21*r33 - r11*r22*r33)));
	float p2y = -((f2*mu*r13*r21 - f2*mu*r11*r23 + cy2*mu*r13*r31 - cx2*mu*r23*r31 - 
       cy2*mu*r11*r33 + cx2*mu*r21*r33 - f2*r23*r31*tx + f2*r21*r33*tx + 
       f2*r13*r31*ty - f2*r11*r33*ty - f2*r13*r21*tz + f2*r11*r23*tz + 
       mu*r23*r31*uu2 - mu*r21*r33*uu2 - mu*r13*r31*vv2 + mu*r11*r33*vv2)/
     (f2*(r13*r22*r31 - r12*r23*r31 - r13*r21*r32 + r11*r23*r32 + 
         r12*r21*r33 - r11*r22*r33)));
	float p2z = -((-(f2*mu*r12*r21) + f2*mu*r11*r22 - cy2*mu*r12*r31 + cx2*mu*r22*r31 + 
       cy2*mu*r11*r32 - cx2*mu*r21*r32 + f2*r22*r31*tx - f2*r21*r32*tx - 
       f2*r12*r31*ty + f2*r11*r32*ty + f2*r12*r21*tz - f2*r11*r22*tz - 
       mu*r22*r31*uu2 + mu*r21*r32*uu2 + mu*r12*r31*vv2 - mu*r11*r32*vv2)/
     (f2*(r13*r22*r31 - r12*r23*r31 - r13*r21*r32 + r11*r23*r32 + 
         r12*r21*r33 - r11*r22*r33)));
	// printf("p2=(%f,%f,%f)\n", p2x,p2y,p2z);

	*rx = 0.5f*(p1x + p2x);
	*ry = 0.5f*(p1y + p2y);
	*rz = 0.5f*(p1z + p2z);

}


// #include <stdio.h>

// int main(int argc, char** argv)
// {
// 	float uu1 = 103.60272;
// 	float vv1 = 70.35158;
// 	float uu2 = 93.31733;
// 	float vv2 = 66.94085;

// 	tri_params_t p;
// 	p.f1 = 120;
// 	p.cx1 = 64;
// 	p.cy1 = 64;
// 	p.f2 = 110;
// 	p.cx2 = 64;
// 	p.cy2 = 64;
// 	p.tx = -0.15502;
// 	p.ty = 0.00653578;
// 	p.tz = 0.003;
// 	p.r11 = 0.996195;
// 	p.r12 = 0.0871557;
// 	p.r13 = 0;
// 	p.r21 = -0.0871557;
// 	p.r22 = 0.996195;
// 	p.r23 = 0;
// 	p.r31 = 0;
// 	p.r32 = 0;
// 	p.r33 = 1;

// 	float rx, ry, rz;
// 	triangulate(&p, uu1,vv1, uu2,v22, &rx, &ry, &rz);

// 	printf("%f, %f, %f\n", rx, ry, rz);

// 	return 1;
// }
