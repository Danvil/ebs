#ifndef INCLUDED_EBS_HPP
#define INCLUDED_EBS_HPP

#include "tri.h"
#include <Eigen/Dense>
#include <vector>
#include <cstdint>

namespace ebs
{

template<typename K>
static K clamp(K x, K a, K b) {
	return std::min<K>(std::max<K>(a, x), b);
}

typedef Eigen::VectorXf hist_t;

void PrintHistogram(std::ostream& os, const hist_t& h);

template<int W>
inline float mipmap_at(const std::vector<Eigen::MatrixXf>& mm, int x, int y, unsigned level) {
	const unsigned s = (W >> level);
	x = clamp<int>(x, 0, s-1);
	y = clamp<int>(y, 0, s-1);
	return mm[level](x,y);
}

template<int W>
inline float& mipmap_at(std::vector<Eigen::MatrixXf>& mm, int x, int y, unsigned level) {
	const unsigned s = (W >> level);
	x = clamp<int>(x, 0, s-1);
	y = clamp<int>(y, 0, s-1);
	return mm[level](x,y);
}

struct DescriptorPyramid
{
	static constexpr unsigned int W = 128;
	static constexpr unsigned int LEVELS = 7;

	std::vector<Eigen::MatrixXf> mmt;

	float mm_total;

	float local_time;

	DescriptorPyramid();

	unsigned int level_width(unsigned int level) const {
		return (W >> level);
	}

	void add(int x, int y, uint64_t t);

	float& at_raw(int x, int y, unsigned int level) {
		return mipmap_at<W>(mmt, x, y, level);
	}

	float at_raw(int x, int y, unsigned int level) const {
		return mipmap_at<W>(mmt, x, y, level);
	}

	float at_val(int x, int y, unsigned int level) const;

	float interpolate(float x, float y, int level) const;

	hist_t hist(int x, int y) const {
		return hist(static_cast<float>(x), static_cast<float>(y));
	}

	hist_t hist(float x, float y) const;

	static float dist(const hist_t& a, const hist_t& b);
	static float dist(const hist_t& a, const hist_t& b, int level);

	float match(const hist_t& h, int x, int y) const;

	float match(const hist_t& h, int x, int y, int level) const;

};

/** Matches a histogram against all points in the descriptor pyramid */
Eigen::MatrixXf match_all(const DescriptorPyramid& pd, const hist_t& h);
Eigen::MatrixXf match_all(const DescriptorPyramid& pd, const hist_t& h, int level);

/** Matches a histogram against all points on the epipolar line in the descriptor pyramid */
Eigen::MatrixXf match_epipolar(const DescriptorPyramid& pd, const hist_t& h, const Eigen::Vector2f& p, const Eigen::Matrix3f& f);

/** Improves histogram matching (winner takes it all) */
void match_improve(Eigen::MatrixXf& m);

/** Finds optimal matching points */
Eigen::Vector2f match_find_best(const Eigen::MatrixXf& m, float* score=0);

/** Compute 3D points from matching image points (a,b) using fundamental matrix f */
Eigen::Vector3f triangulate(const Eigen::Vector2f& a, const Eigen::Vector2f& b, const tri_params_t& tp);

struct result_t
{
	Eigen::Vector2f in;
	Eigen::Vector2f match;
	float score;
	Eigen::Vector3f out;
	hist_t h_in;
	Eigen::MatrixXf sim;
};

/** Computes 3D point for event */
result_t match_and_triangulate(
	int x, int y,
	const DescriptorPyramid& dp1, const DescriptorPyramid& dp2,
	const Eigen::Matrix3f& f, const tri_params_t& tp);

}

#endif
