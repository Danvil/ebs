#include "ebs.hpp"
#include <iostream>

namespace ebs
{

DescriptorPyramid::DescriptorPyramid()
{
	mmt.resize(LEVELS);
	for(unsigned int l=0; l<LEVELS; l++) {
		unsigned int w = level_width(l);
		mmt[l] = Eigen::MatrixXf::Zero(w,w);
	}
	local_time = 0;
}

namespace exp_average
{
	// level 0: finest grid (pixel level)
	// level num: coarsest grid

	constexpr float BETA = 0.864665f; // 1 - 1/e^2 (chosen such that averages are 0.5)

	std::vector<float> actual(int num) {
		std::cout << "Exponential running average weights: " << std::endl;
		std::vector<float> res(num);
		for(int i=0; i<num; i++) {
			//if(i == 0) {
			//	res[i] = 1;
			//}
			//else {
				res[i] = 1.0f - std::pow(1.0f - BETA, 1.0f / std::pow(2.0f, i));
			//}
			std::cout << "\t" << i << ": " << res[i] << std::endl;
		}	
		return res;
	}

	std::vector<float> averages(int num) {
		// FIXME assert num == 7
		// averages depend on level. pre-computed for fixed BETA=1-1/e^2
		return {0.156518, 0.581977, 1.54149, 3.52081, 7.51041, 15.5052, 31.5026};
	}
} 

void DescriptorPyramid::add(int x, int y, uint64_t t)
{
	static std::vector<float> TAB_LAM = exp_average::actual(LEVELS);

	float tf = static_cast<float>(t) / 1000000.0f;
	for(unsigned int l=0; l<LEVELS; l++) {
		float& q = mipmap_at<W>(mmt, x>>l, y>>l, l);
		// exponential decay
		q += TAB_LAM[l]*(tf - q);
	}
	local_time = tf;
}

inline float border_at(const Eigen::MatrixXf& m, int x, int y) {
	if(0 <= x && x < m.rows() && 0 <= y && y < m.cols()) {
		return m(x,y);
	}
	else {
		return 0.0f;
	}
}

/** computes integer i and p \in [0,1] s.t. x = i + p */
inline void interpolate_idx(float x, int& i, float& p) {
	i = (int)x;
	if(x < 0) {
		i--;
	}
	p = x - (float)i;
}

inline float interpolate_linear(float a, float b, float p) {
	return (1.0f - p)*a + p*b;
}

float DescriptorPyramid::interpolate(float x, float y, int level) const
{
	// mipmap cells have their achor at the lower left corner!
	// need to shift to the center
	x -= 0.5f;
	y -= 0.5f;

	int ix, iy;
	float px, py;
	interpolate_idx(x, ix, px);
	interpolate_idx(y, iy, py);
	const Eigen::MatrixXf& m = mmt[level];
	return interpolate_linear(
		interpolate_linear(border_at(m, ix, iy  ), border_at(m, ix+1, iy  ), px),
		interpolate_linear(border_at(m, ix, iy+1), border_at(m, ix+1, iy+1), px),
		py);
}

// void interpolate8_slow(const Eigen::MatrixXf& m, float u, float v, float w, float* result) {
// 	result[0] = w*interpolate(m, u-1, v-1);
// 	result[1] = w*interpolate(m, u  , v-1);
// 	result[2] = w*interpolate(m, u+1, v-1);
// 	result[3] = w*interpolate(m, u-1, v  );
// 	result[4] = w*interpolate(m, u+1, v  );
// 	result[5] = w*interpolate(m, u-1, v+1);
// 	result[6] = w*interpolate(m, u  , v+1);
// 	result[7] = w*interpolate(m, u+1, v+1);
// }

/** Interpolates values at the 8 surrounding positions */
void interpolate8(const Eigen::MatrixXf& m, float x, float y, float* result)
{
	// mipmap cells have their achor at the lower left corner!
	// need to shift to the center
	x -= 0.5f;
	y -= 0.5f;

	int ix, iy;
	float px, py;
	interpolate_idx(x, ix, px);
	interpolate_idx(y, iy, py);

	// grid points
	float m11 = border_at(m, ix-1,iy-1);
	float m12 = border_at(m, ix-1,iy  );
	float m13 = border_at(m, ix-1,iy+1);
	float m14 = border_at(m, ix-1,iy+2);
	float m21 = border_at(m, ix  ,iy-1);
	float m22 = border_at(m, ix  ,iy  );
	float m23 = border_at(m, ix  ,iy+1);
	float m24 = border_at(m, ix  ,iy+2);
	float m31 = border_at(m, ix+1,iy-1);
	float m32 = border_at(m, ix+1,iy  );
	float m33 = border_at(m, ix+1,iy+1);
	float m34 = border_at(m, ix+1,iy+2);
	float m41 = border_at(m, ix+2,iy-1);
	float m42 = border_at(m, ix+2,iy  );
	float m43 = border_at(m, ix+2,iy+1);
	float m44 = border_at(m, ix+2,iy+2);

	// linear interpolation
	float v11, v12, v13,
	      v21,      v23,
	      v31, v32, v33;
//	float v22; // skip middle
	float p00 = (1.0f - px)*(1.0f - py);
	float p01 = (1.0f - px)*py;
	float p10 = px*(1.0f - py);
	float p11 = px*py;
	v11 = p00*m11 + p01*m12 + p10*m21 + p11*m22;
	v12 = p00*m12 + p01*m13 + p10*m22 + p11*m23;
	v13 = p00*m13 + p01*m14 + p10*m23 + p11*m24;
	v21 = p00*m21 + p01*m22 + p10*m31 + p11*m32;
//	v22 = p00*m22 + p01*m23 + p10*m32 + p11*m33; // skip middle
	v23 = p00*m23 + p01*m24 + p10*m33 + p11*m34;
	v31 = p00*m31 + p01*m32 + p10*m41 + p11*m42;
	v32 = p00*m32 + p01*m33 + p10*m42 + p11*m43;
	v33 = p00*m33 + p01*m34 + p10*m43 + p11*m44;

	// write out results + weighting
	result[0] = v11;
	result[1] = v21;
	result[2] = v31;
	result[3] = v12;
//				v22 // skip middle
	result[4] = v32;
	result[5] = v13;
	result[6] = v23;
	result[7] = v33;
}

namespace hist_weights
{
	// level 0: finest grid (pixel level)
	// level num: coarsest grid

	std::vector<float> exp_pass(int num, float alpha) {
		std::vector<float> res(num);
		float w = 1.0f;
		for(int i=0; i<num; i++) {
			res[i] = w;
			w *= alpha;
		}	
		return res;
	}

	std::vector<float> gauss_pass(int num, float mu, float sigma) {
		std::vector<float> res(num);
		for(int i=0; i<num; i++) {
			float d = (static_cast<float>(i) - mu) / sigma;
			res[i] = std::exp(-0.5f*d*d);
		}	
		return res;	
	}

	std::vector<float> unit(int num, int i) {
		std::vector<float> res(num, 0.0f);
		res[i] = 1.0f;
		return res;	
	}

	std::vector<float> actual(int num) {
		std::vector<float> v;
		//v = exp_pass(num, 1.0f/2.3f); // good mixture of low and high frequencies
		v = exp_pass(num, 2.3f); // other pass
		//v = exp_pass(num, 1.0f/8.0f); // more high-pass
		//v = exp_pass(num, 1.0f); // const
		//v = gauss_pass(num, 2, 1); // gauss curve

		float sum = 0.0f;
		for(int i=0; i<num; i++)
			sum += v[i];
		for(int i=0; i<num; i++)
			v[i] /= sum;

		std::cout << "Histogram frequency weights: " << std::endl;
		for(int i=0; i<num; i++) {
			std::cout << i << ": " << v[i] << std::endl;
		}
		return v;
	}
}

float compute_hist_value(float t, float tnow, unsigned level)
{
//	static std::vector<float> av = exp_average::averages(DescriptorPyramid::LEVELS);
	// dt is the difference between now and stored time (in s)
	float dt = tnow - t;
//	// a is the lag in number of events 
//	float a = av[level];
	// normalize old
	if(dt == 0.0f) {
		dt = 1.0f / 1000000.0f;
	}
	return 1.0f / dt;
//	// normalize new
//	return dt / a;
}

float DescriptorPyramid::at_val(int x, int y, unsigned int level) const
{

	float v = at_raw(x, y, level);
	return compute_hist_value(v, local_time, level);
}

hist_t DescriptorPyramid::hist(float u, float v) const
{
	hist_t h(8*LEVELS);
	float* hp = h.data();

	// float tsum = 0.0f;

	for(unsigned int l=0; l<LEVELS; l++) {
		// interpolate in mipmap		
		interpolate8(mmt[l], u, v, hp + 8*l);

		float sum = 0.0f;
		for(int i=0; i<8; i++) {
			float v = compute_hist_value(local_time, hp[8*l+i], l);
			hp[8*l+i] = v;
			sum += v;
		}

		// tsum += sum;

		if(sum != 0.0f) {
			// normalize each frequency individually
			for(int i=0; i<8; i++) {
				hp[8*l + i] /= sum;
			}
		}
		
		// next frequency
		u *= 0.5f;
		v *= 0.5f;
	}

	// for(int i=0; i<8*LEVELS; i++) {
	// 	hp[i] /= tsum;
	// }

	return h;
}

void PrintHistogram(std::ostream& os, const hist_t& h)
{
	for(unsigned l=0; l<DescriptorPyramid::LEVELS; l++) {
		os << l << ": ";
		for(unsigned i=0; i<8; i++) {
			//os.width(4);
			os << h[8*l+i] << " ";
		}
		os << std::endl;
	}	
}

float DescriptorPyramid::dist(const hist_t& h1, const hist_t& h2)
{
	static std::vector<float> hw = hist_weights::actual(LEVELS);
	// static std::vector<float> hw = hist_weights::unit(LEVELS, 6);

	// return h2[8*4 + 4];

	// ASSUME h1.size() == h2.size() == 8*LEVELS
	float sum = 0.0f;
	for(int k=0; k<LEVELS; k++) {
		float lvl_sum = 0.0f;
		for(unsigned int i=0; i<8; i++) {
			float v1 = h1[k*8 + i];
			float v2 = h2[k*8 + i];
			float a = v1 + v2;
			if(a == 0.0f) a = 1.0f;
			float b = v1 - v2;
			lvl_sum += (b*b) / a;
		}
		sum += hw[k] * lvl_sum;
	}
	// 0 is perfect match, 1 is worst match
	return 1.0f - 0.5f * sum;
}

float DescriptorPyramid::dist(const hist_t& h1, const hist_t& h2, int level)
{
	// ASSUME h1.size() == h2.size() == 8*LEVELS
	float lvl_sum = 0.0f;
	for(unsigned int i=0; i<8; i++) {
		float v1 = h1[level*8 + i];
		float v2 = h2[level*8 + i];
		float a = v1 + v2;
		if(a == 0.0f) a = 1.0f;
		float b = v1 - v2;
		lvl_sum += (b*b) / a;
	}
	return 1.0f - 0.5f * lvl_sum;
}

float DescriptorPyramid::match(const hist_t& h0, int x, int y) const
{
	return ebs::DescriptorPyramid::dist(h0, hist(x,y));
}

float DescriptorPyramid::match(const hist_t& h0, int x, int y, int level) const
{
	return ebs::DescriptorPyramid::dist(h0, hist(x,y), level);
}

Eigen::MatrixXf match_all(const DescriptorPyramid& pd, const hist_t& h0)
{
	Eigen::MatrixXf hd(pd.W, pd.W);
	for(int y=0; y<hd.rows(); y++) {
		for(int x=0; x<hd.cols(); x++) {
			hd(x,y) = pd.match(h0, x,y);
		}
	}
	return hd;
}

Eigen::MatrixXf match_all(const DescriptorPyramid& pd, const hist_t& h0, int level)
{
	Eigen::MatrixXf hd(pd.W, pd.W);
	for(int y=0; y<hd.rows(); y++) {
		for(int x=0; x<hd.cols(); x++) {
			hd(x,y) = pd.match(h0, x,y, level);
		}
	}
	return hd;
}

struct MinimalSlopeLine
{
	Eigen::Vector3f implicit;
	Eigen::Vector2f p1, p2;
	bool use_x;
};

MinimalSlopeLine epipolar_line_endpoints(const Eigen::Matrix3f& f, const Eigen::Vector2f& p, int Wi)
{
	const float W = static_cast<float>(Wi);
	MinimalSlopeLine res;
	// with (a1,a2,a3) = f.p, the set of points on the line is {(x,y) | a1*x + a2*y + a3 = 0}
	res.implicit = f * Eigen::Vector3f(p.x(), p.y(), 1.0f);
	float a1 = res.implicit.x();
	float a2 = res.implicit.y();
	float a3 = res.implicit.z();
	// choose smaller slope to find end points
	if(std::abs(a1) > std::abs(a2)) {
		res.p1 = Eigen::Vector2f((-a3 - a2*0)/a1, 0);
		res.p2 = Eigen::Vector2f((-a3 - a2*W)/a1, W);
		res.use_x = false;
	}
	else {
		res.p1 = Eigen::Vector2f(0, (-a3 - a1*0)/a2);
		res.p2 = Eigen::Vector2f(W, (-a3 - a1*W)/a2);
		res.use_x = true;
	}
	return res;
}

Eigen::MatrixXf match_epipolar(const DescriptorPyramid& pd, const hist_t& h0, const Eigen::Vector2f& p, const Eigen::Matrix3f& f)
{
	const int W = pd.W;
	const int TOLERANCE = 8;
	// compute line
	MinimalSlopeLine msl = epipolar_line_endpoints(f, p, W);
	// match by walking on line
	Eigen::MatrixXf hd = Eigen::MatrixXf::Zero(W, W);
	for(int u=0; u<W; u++) {
		float p = static_cast<float>(u) / static_cast<float>(W-1);
		Eigen::Vector2f s = (1.0f - p)*msl.p1 + p*msl.p2;
		int sx0 = s.x();
		int sy0 = s.y();
		for(int r=-TOLERANCE; r<=+TOLERANCE; r++) {
			int sx = sx0;
			int sy = sy0;
			if(msl.use_x) {
				sy += r;
			}
			else {
				sx += r;
			}
			if(0 <= sx && sx < W && 0 <= sy && sy < W) {
				hd(sx, sy) = pd.match(h0, sx,sy);	
			}
		}
	}
	return hd;
}

void normalize_range(Eigen::MatrixXf& m) {
	float hdmin = m.minCoeff();
	float hdmax = m.maxCoeff();
	m.array() -= hdmin;
	m /= (hdmax - hdmin);
}

void match_improve(Eigen::MatrixXf& m)
{
	m = m.array().pow(16.0f).matrix();
	normalize_range(m);

//	//normalize_range(m);
//	m = (1.0f - (1.0f - m.array()).pow(16.0f)).matrix();
//	normalize_range(m);
}

// find best match by max
Eigen::Vector2f match_find_best_max(const Eigen::MatrixXf& m, float* score)
{
	Eigen::Vector2f best_pos = Eigen::Vector2f::Zero();
	float best_score = m(0,0);
	for(int y=0; y<128; y++) {
		for(int x=0; x<128; x++) {
			float v = m(x,y);
			if(v > best_score) {
				best_pos = Eigen::Vector2f(x,y);
				best_score = v;
			}
		}
	}
	if(score) {
		*score = best_score;
	}
	return best_pos;
}

// find best match by mean
Eigen::Vector2f match_find_best_mean(const Eigen::MatrixXf& m, float threshold, float* score)
{
	Eigen::Vector2f sum_pos = Eigen::Vector2f::Zero();
	float sum_score = 0.0f;
	unsigned num = 0;
	for(int y=0; y<128; y++) {
		for(int x=0; x<128; x++) {
			float v = m(x,y);
			if(v > threshold) {
				sum_pos += v * Eigen::Vector2f{x,y};
				sum_score += v;
				num++;
			}
		}
	}
	if(score) {
		*score = sum_score / static_cast<float>(num);
	}
	return sum_pos / sum_score;
}

// find best match by using best 10 %
Eigen::Vector2f match_find_best_winners(const Eigen::MatrixXf& m, float winner, float* score)
{
	// create score histogram to find threshold
	constexpr unsigned NUM_BUCKETS = 140;
	constexpr float MIN_SCORE = 0.25f;
	std::vector<unsigned> buckets(NUM_BUCKETS, 0.0f);
	unsigned num = 0;
	for(int y=0; y<128; y++) {
		for(int x=0; x<128; x++) {
			float v = m(x,y);
			if(v < MIN_SCORE) {
				continue;
			}
			unsigned b = std::min(NUM_BUCKETS-1,
				static_cast<unsigned>(static_cast<float>(NUM_BUCKETS)*v));
			buckets[b] ++;
			num ++;
		}
	}
	// compute threshold
	unsigned b;
	float sum = 0.0f;
	float last_d = 0.0f;
	winner = 1.0f - winner;
	for(b=0; b<NUM_BUCKETS; b++) {
		last_d = static_cast<float>(buckets[b]) / static_cast<float>(num);
		sum += last_d;
//		std::cout << winner << " " << b << " " << sum << " " << last_d << std::endl;
		if(sum >= winner) {
			break;
		}
	}
	float threshold_p = (winner + last_d - sum) / last_d;
	float threshold = (static_cast<float>(b) + threshold_p) / static_cast<float>(NUM_BUCKETS);
//	std::cout << " => " << threshold << std::endl;
	// build mean of values with high enough score
	return match_find_best_mean(m, threshold, score);
}

Eigen::Vector2f match_find_best(const Eigen::MatrixXf& m, float* score)
{
	// return match_find_best_max(m, score);
	return match_find_best_mean(m, 0.5f, score); // mean of all
	//return match_find_best_winners(m, 0.1f, score); // best 10%
}

/** Computes approximation to the intersection of two 3D lines using the midpoint method */
Eigen::Vector3f line_intersection_3d(
	const Eigen::Vector3f& a, const Eigen::Vector3f& u,
	const Eigen::Vector3f& b, const Eigen::Vector3f& v
) {
	float uu = u.dot(u);
	float uv = u.dot(v);
	float vv = v.dot(v);
	float abu = (a - b).dot(u);
	float abv = (a - b).dot(v);
	Eigen::Matrix2f m;
	m << uu, -uv, uv, -vv;
	Eigen::Vector2f d(abu, abv);
	Eigen::Vector2f s = m.colPivHouseholderQr().solve(d);
	Eigen::Vector3f p1 = a + s.x()*u;
	Eigen::Vector3f p2 = b + s.y()*v;
	return 0.5f*(p1 + p2);
}

Eigen::Vector3f triangulate(const Eigen::Vector2f& p1, const Eigen::Vector2f& p2, const tri_params_t& tp)
{
	float rx, ry, rz;
	tri_triangulate(&tp,
		p1.x(), p1.y(),
		p2.x(), p2.y(),
		&rx, &ry, &rz);
	return Eigen::Vector3f{ rx, ry, rz };
}

result_t match_and_triangulate(
	int x, int y,
	const DescriptorPyramid& dp1, const DescriptorPyramid& dp2,
	const Eigen::Matrix3f& calib_f, const tri_params_t& calib_tp
)
{
	result_t result;

	constexpr bool MATCH_EPIPOLAR = true;
	constexpr bool MATCH_IMPROVE = true;

	result.in = Eigen::Vector2f{x,y};
	result.h_in = dp1.hist(x,y);

	if(MATCH_EPIPOLAR) {
		result.sim = match_epipolar(dp2, result.h_in, result.in, calib_f);
	} else {
		result.sim = match_all(dp2, result.h_in);
	}
	if(MATCH_IMPROVE) {
		match_improve(result.sim);
	}
	result.match = match_find_best(result.sim, &result.score);

	result.out = triangulate(result.in, result.match, calib_tp);

	return result;
}

}
