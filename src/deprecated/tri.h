#ifndef SEDVS_TRI_H
#define SEDVS_TRI_H

#ifdef __cplusplus
extern "C" {
#endif

typedef struct
{
	float f1, cx1, cy1;
	float f2, cx2, cy2;
	float tx, ty, tz;
	float r11, r12, r13,
	      r21, r22, r23,
	      r31, r32, r33;
} tri_params_t;

void tri_triangulate(
	const tri_params_t* p,
	float uu1, float vv1, float uu2, float vv2,
	float* rx, float* ry, float* rz);

#ifdef __cplusplus
}
#endif

#endif
