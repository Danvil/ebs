#ifndef EBSVIS_WDGTEBSRECORD_H
#define EBSVIS_WDGTEBSRECORD_H

#include "ui_WdgtEbsRecord.h"
#include <ebs/Core.hpp>
#include <ebs/Visualization.hpp>
#include <ebs/Calibration.hpp>
#include <Edvs/EventStream.hpp>
#include <qs/OpenNI.hpp>
#include <da/synchronized.hpp>
#include <QtGui/QWidget>
#include <QtGui/QImage>
#include <QtCore/QTimer>
#include <thread>
#include <atomic>
#include <string>

struct GtEvent : Edvs::Event
{
	float xm, ym;
	float depth;
	float disparity;
};

class WdgtEbsRecord : public QWidget
{
	Q_OBJECT

public:
	WdgtEbsRecord(const std::shared_ptr<Edvs::IEventStream>& stream, const ebs::Calibration& calib, QWidget* parent = 0);
	~WdgtEbsRecord();

	void closeEvent(QCloseEvent* event);

protected Q_SLOTS:
	void Tick();
	void OnRecord();

private:
	void threadDvs();
	void threadOpenNi();

private:
	Ui::WdgtEbsRecordClass ui;

	std::shared_ptr<Edvs::IEventStream> stream_;
	std::shared_ptr<qs::OpenNISource> openni_;

	ebs::Calibration calib_;

	QTimer timer_;

	bool is_running_;
	std::thread thread_dvs_;
	std::thread thread_openni_;

	da::synchronized<std::vector<edvs_event_t>> events_;

	std::atomic<bool> is_new_frame_;	
	da::synchronized<std::vector<GtEvent>> events_vis_;
	da::synchronized<slimage::Image3ub> img_depth_;
	da::synchronized<slimage::Image3ub> img_disparity_;
	da::synchronized<slimage::Image3ub> img_color_;

	bool is_recording_;
	bool is_recording_depth_;
	bool is_recording_disparity_;
	bool is_recording_color_;
	std::string rec_tag_;
	unsigned rec_id_;
	std::vector<GtEvent> events_recorded_;

};

#endif
