#include "WdgtEbsRecord.h"
#include <ebs/Tools.hpp>
#include <ebs/Calibration.hpp>
#include <ebs/Visualization.hpp>
#include <ebs/QtHelpers.hpp>
#include <qs/OpenNI.hpp>
#include <Edvs/EventIO.hpp>
#include <boost/format.hpp>
#include <slimage/io.hpp>
#include <QPainter>
#include <QMouseEvent>
#include <QMessageBox>
#include <QDir>
#include <sstream>
#include <chrono>
#include <thread>
#include <iterator>

constexpr uint64_t VIS_DELTA = 200000;

WdgtEbsRecord::WdgtEbsRecord(const std::shared_ptr<Edvs::IEventStream>& stream, const ebs::Calibration& calib, QWidget* parent)
:	stream_(stream),
	calib_(calib)
{
	ui.setupUi(this);

	is_recording_ = false;
	QObject::connect(ui.pushButtonRecord, SIGNAL(clicked()), this, SLOT(OnRecord()));

	QObject::connect(&timer_, SIGNAL(timeout()), this, SLOT(Tick()));
	timer_.setInterval(10);
	timer_.start();

	// initialize OpenNI
	openni_ = std::make_shared<qs::OpenNISource>();
	std::this_thread::sleep_for(std::chrono::milliseconds(500));

	// start threads
	is_running_ = true;
	thread_dvs_ = std::thread(&WdgtEbsRecord::threadDvs, this);
	thread_openni_ = std::thread(&WdgtEbsRecord::threadOpenNi, this);

}

WdgtEbsRecord::~WdgtEbsRecord()
{
	is_running_ = false;
	thread_dvs_.join();
	thread_openni_.join();
}

void WdgtEbsRecord::closeEvent(QCloseEvent* event)
{
	// FIXME
}

void WdgtEbsRecord::Tick()
{
	if(is_new_frame_) {
		is_new_frame_ = false;
		// events
		auto events = *events_vis_.lock();
		// id 0 events with disparity
		ebs::QtDisplay(ui.labelEvents0,
			ebs::PlotEvents(events.begin(), events.end(), 128, 128,
				[](const GtEvent& e) { return ebs::ColorizeDisparity(e.disparity); },
				[](const GtEvent& e) { return e.id == 0; },
				true));
		// id 1 events with parity
		ebs::QtDisplay(ui.labelEvents1,
			ebs::PlotEvents(events.begin(), events.end(), 128, 128,
//				[](const GtEvent& e) { return ebs::ColorizeParity(e.parity); },
				[](const GtEvent& e) { return ebs::ColorizeDisparity(e.disparity); },
				[](const GtEvent& e) { return e.id == 1; },
				true));
		// for(auto& e : events) {
		// 	e.x = e.x0;
		// 	e.y = e.y0;
		// }
		// display(ui.labelEvents1,
		// 	ebs::PlotEventsById(events.begin(), events.end(), 128, 128, 0, true));
		// disparity
		ebs::QtDisplay(ui.labelDisparity, *img_disparity_.lock());
		// // // depth
		ebs::QtDisplay(ui.labelDepth, *img_color_.lock());
	}
}

boost::format fmt_dir_			("/tmp/%s-%03d");
boost::format fmt_dir_depth		("/tmp/%s-%03d/depth");
boost::format fmt_dir_disparity	("/tmp/%s-%03d/disparity");
boost::format fmt_dir_color		("/tmp/%s-%03d/color");
boost::format fmt_fn_events		("/tmp/%s-%03d/events-gt.tsv");
boost::format fmt_fn_depth		("/tmp/%s-%03d/depth/%012d.tsv");
boost::format fmt_fn_disparity	("/tmp/%s-%03d/disparity/%012d.tsv");
boost::format fmt_fn_color		("/tmp/%s-%03d/color/%012d.png");

bool removeDir(const QString & dirName)
{
    bool result = true;
    QDir dir(dirName);

    if (dir.exists(dirName)) {
        Q_FOREACH(QFileInfo info, dir.entryInfoList(QDir::NoDotAndDotDot | QDir::System | QDir::Hidden  | QDir::AllDirs | QDir::Files, QDir::DirsFirst)) {
            if (info.isDir()) {
                result = removeDir(info.absoluteFilePath());
            }
            else {
                result = QFile::remove(info.absoluteFilePath());
            }

            if (!result) {
                return result;
            }
        }
        result = dir.rmdir(dirName);
    }
    return result;
}

void WdgtEbsRecord::OnRecord()
{
	if(ui.pushButtonRecord->isChecked()) {
		// prepare recording
		is_recording_depth_ = ui.checkBoxRecordDepth->isChecked();
		is_recording_disparity_ = ui.checkBoxRecordDisparity->isChecked();
		is_recording_color_ = ui.checkBoxRecordColor->isChecked();
		rec_tag_ = ui.lineEditTag->text().toStdString();
		rec_id_ = ui.spinBoxId->value();
		QString dir_name = QString::fromStdString((fmt_dir_ % rec_tag_ % rec_id_).str());
		QDir qdir(dir_name);
		if(qdir.exists()) {
			// directory exists -> ask
			QMessageBox::StandardButton reply = QMessageBox::question(this,
				"Overwrite?", "Recording already exists. Do you want to overwrite?", QMessageBox::Yes|QMessageBox::No);
			if(reply == QMessageBox::Yes) {
				removeDir(dir_name);
			}
			else {
				// abort
				return;
			}
		}
		// update gui
		ui.pushButtonRecord->setText("Stop");
		ui.checkBoxRecordDepth->setEnabled(false);
		ui.checkBoxRecordDisparity->setEnabled(false);
		ui.checkBoxRecordColor->setEnabled(false);
		ui.lineEditTag->setEnabled(false);
		ui.spinBoxId->setEnabled(false);
		// create directories
		qdir.mkpath(".");
		if(is_recording_depth_) {
			QDir(QString::fromStdString((fmt_dir_depth % rec_tag_ % rec_id_).str())).mkpath(".");
		}
		if(is_recording_disparity_) {
			QDir(QString::fromStdString((fmt_dir_disparity % rec_tag_ % rec_id_).str())).mkpath(".");
		}
		if(is_recording_color_) {
			QDir(QString::fromStdString((fmt_dir_color % rec_tag_ % rec_id_).str())).mkpath(".");
		}
		// start recording
		is_recording_ = true;
	}
	else {
		// stop recording
		is_recording_ = false;
		// check if we want to save
		QMessageBox::StandardButton reply = QMessageBox::question(this,
			"Save?", "Do you want to save this recording?", QMessageBox::Yes|QMessageBox::No);
		if(reply == QMessageBox::Yes) {
			std::string fn_events = (fmt_fn_events % rec_tag_ % rec_id_).str();
			// print annotated
			std::ofstream ofs(fn_events);
			std::string sep = "\t";
			ofs.precision(3);
			ofs << std::fixed;
			for(const auto& e : events_recorded_) {
				ofs << e.t << sep
					<< e.xm << sep
					<< e.ym << sep
					<< static_cast<unsigned>(e.parity) << sep
					<< static_cast<unsigned>(e.id) << sep
					<< e.depth << sep
					<< e.disparity << "\n";
			}
			// FIXME
//			Edvs::SaveEvents(fn.toStdString(), events_recorded_);
			std::cout << "Saved " << events_recorded_.size() << " events to file '" << fn_events << "'" << std::endl;
			// increment id
			ui.spinBoxId->setValue(rec_id_ + 1);
		}
		else {
			// delete contents of folder
			QString qdir = QString::fromStdString((fmt_dir_ % rec_tag_ % rec_id_).str());
			removeDir(qdir);
		}
		events_recorded_.clear();
		// update gui
		ui.pushButtonRecord->setText("Record");
		ui.checkBoxRecordDepth->setEnabled(true);
		ui.checkBoxRecordDisparity->setEnabled(true);
		ui.checkBoxRecordColor->setEnabled(true);
		ui.lineEditTag->setEnabled(true);
		ui.spinBoxId->setEnabled(true);
	}
}

void WdgtEbsRecord::threadDvs()
{
	while(is_running_) {
		// read events
		std::vector<edvs_event_t> events_new = stream_->read();
		// store events
		auto p = events_.lock();
		p->reserve(p->size() + events_new.size());
		for(auto e : events_new) {
			// transformation to match orientation in rig
			uint16_t ex = e.x;
			uint16_t ey = e.y;
			e.x = 127 - ey;
			e.y = 127 - ex;
			p->push_back(e);
		}
	}
}

void SaveMatrix(const std::string& fn, const slimage::Image1f& m)
{
	std::ofstream ofs(fn);
	for(unsigned i=0; i<m.height(); i++) {
		for(unsigned j=0; j<m.width(); j++) {
			ofs << m(j,i);
			if(j+1 < m.width()) {
				ofs << "\t";
			}
			else {
				ofs<< "\n";
			}
		}
	}
}


void WdgtEbsRecord::threadOpenNi()
{
	while(is_running_) {
		// wait for next frame
		openni_->tick();
		// process color
		if(openni_->color_changed) {
			openni_->color_changed = false;
			*img_color_.lock() = openni_->color_img;
			// save color image
			if(is_recording_color_ && is_recording_) {
				uint64_t last_event_ts = 0;
				{
					auto p = events_.lock();
					if(!p->empty()) {
						last_event_ts = p->back().t;
					}
				}
				std::string fn = (fmt_fn_color % rec_tag_ % rec_id_ % last_event_ts).str();
				slimage::Save(openni_->color_img, fn);
			}
		}
		// process depth
		if(openni_->depth_changed) {
			openni_->depth_changed = false;
			// get frame events
			std::vector<Edvs::Event> events;
			{
				auto p = events_.lock();
				events = *p;
				p->resize(0);
			}
			// get depth image
			slimage::Image1ui16 depth = openni_->depth_img;
			// compute dvs 0 depth
			std::vector<slimage::Image1f> dvsdepth = {
				ebs::ReprojectDepth(depth,
					calib_.kinect, calib_.dvs1, calib_.kinect_to_dvs1),
				ebs::ReprojectDepth(depth,
					calib_.kinect, calib_.dvs2, calib_.kinect_to_dvs2)
			};
			// compute dvs 0 disparity
			std::vector<slimage::Image1f> dvsdisparity = ebs::DepthToDisparity(depth, calib_);
			// undistort events
			std::vector<ebs::CameraCalibration> cams{calib_.dvs1, calib_.dvs2};
			std::vector<Edvs::Event> events_orig = events;

			// save depth image
			if(is_recording_ && is_recording_depth_ && !events.empty()) {
				std::string fn = (fmt_fn_depth % rec_tag_ % rec_id_ % events.back().t).str();
				SaveMatrix(fn, dvsdepth[0]);
			}
			if(is_recording_ && is_recording_disparity_ && !events.empty()) {
				std::string fn = (fmt_fn_disparity % rec_tag_ % rec_id_ % events.back().t).str();
				SaveMatrix(fn, dvsdisparity[0]);
			}

			// undistort
			std::vector<Eigen::Vector2f> coordsf;
			ebs::Undistort(events, cams, true, &coordsf);

			// annotate events
			std::vector<GtEvent> events_frame;
			events_frame.reserve(events.size());
			for(size_t i=0; i<events.size(); i++) {
				const auto& e = events[i];
				if(e.id != 0 && e.id != 1) {
					continue;
				}
				GtEvent ge;
				static_cast<Edvs::Event&>(ge) = e;
				ge.xm = coordsf[i][0];
				ge.ym = coordsf[i][1];
				ge.depth = ebs::LookUpDepth<GtEvent,1>(ge, dvsdepth[e.id]);
				ge.disparity = ebs::LookUpDisparity<GtEvent,1>(ge, dvsdisparity[e.id]);
				events_frame.push_back(ge);
			}				
			// recording
			if(is_recording_) {
				events_recorded_.insert(events_recorded_.end(),
					events_frame.begin(), events_frame.end());
			}
			// visualize depth image
			*img_depth_.lock() = ebs::VisualizeDepthImage(dvsdepth[0]);
			// visualize disparity image
			*img_disparity_.lock() = ebs::VisualizeDisparityImage(dvsdisparity[0]);

			// // DEBUGGING
			// {
			// 	slimage::Image1f res{320, 240};
			// 	res.fill(slimage::Pixel1f{0});
			// 	for(size_t i=0; i<res.size(); i++) {
			// 		res[i] = static_cast<float>(depth[i])*0.001f;
			// 	}
			// 	*img_disparity_.lock() = ebs::VisualizeDepthImage(res);
			// }

			// visualize events
			{
				auto p = events_vis_.lock();
				// add new
				p->insert(p->end(),
					events_frame.begin(), events_frame.end());
				// remove old
				*p = ebs::CloneEventRange(*p, VIS_DELTA);
			}
			// we have a new frame
			is_new_frame_ = true;
		}
	}
}
