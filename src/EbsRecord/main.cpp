#include "WdgtEbsRecord.h"
#include <Edvs/EventStream.hpp>
#include <QtGui>
#include <QApplication>
#include <boost/program_options.hpp>
#include <iostream>

int main(int argc, char *argv[])
{
	std::vector<std::string> p_vuri;///dev/ttyUSB0?baudrate=4000000";
	std::string p_calib_fn;

	// get command line parameters
	namespace po = boost::program_options;
	po::options_description desc("Allowed options");
	desc.add_options()
		("help", "produce help message")
		("uris", po::value(&p_vuri)->multitoken(), "URI(s) to event stream(s)")
		("calib", po::value(&p_calib_fn), "Filename of stereo/kinect calibration")
	;
	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);
	if(vm.count("help")) {
		std::cout << desc << std::endl;
		return 1;
	}

	// load calibration
	ebs::Calibration calib;
	std::cout << "Loading calibration from '" << p_calib_fn << "'" << std::endl;
	ebs::Load(p_calib_fn, calib);
	std::cout << calib << std::endl;

	// load events
	auto stream = Edvs::OpenEventStream(p_vuri);

	// start gui
	QApplication a(argc, argv);
	WdgtEbsRecord w(stream, calib);
	w.show();
	return a.exec();
}
