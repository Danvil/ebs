#ifndef INCLUDED_EBS_QTHELPERS_HPP
#define INCLUDED_EBS_QTHELPERS_HPP

#include "Visualization.hpp"
#include <slimage/image.hpp>
#include <slimage/qt.hpp>
#include <slimage/io.hpp>
#include <da/synchronized.hpp>
#include <QImage>
#include <QLabel>
#include <cstdint>
#include <map>
#include <string>

namespace ebs
{

	QImage QtConvert(const slimage::Image3ub& img, unsigned size=256)
	{
		QImage p = slimage::ConvertToQt(img);
		if(p.isNull()) {
			return p;
		}
		return p.scaledToWidth(size);
	}

	void QtDisplay(QLabel* label, const QImage& img)
	{ label->setPixmap(QPixmap::fromImage(img)); }

	void QtDisplay(QLabel* label, const slimage::Image3ub& img, unsigned size=256)
	{ QtDisplay(label, QtConvert(img,size)); }

	class Visualization
	{
	private:
		struct Vis
		{
			QImage img;
			QLabel* label;
		};

	public:
		void add(const std::string& tag, QLabel* label)
		{ (*items_.lock())[tag] = Vis{QImage(), label}; }

		void plot(const std::string& tag, const QImage& img)
		{ (*items_.lock())[tag].img = img; }

		void plot(const std::string& tag, const slimage::Image3ub& img)
		{ (*items_.lock())[tag].img = QtConvert(img); }

		void plotEvents(const std::string& tag, const std::vector<Edvs::Event>& v, uint8_t id)
		{ return plot(tag, ebs::PlotEventsById(v.begin(), v.end(), 128, 128, id, true)); }

		void plotStereoEvents(const std::string& tag, const std::vector<ebs::StereoEvent>& v, ebs::VisMode mode)
		{ return plot(tag, ebs::PlotStereoEvents(v.begin(), v.end(), 128, 128, mode, true)); }

		void plotEventField(const std::string& tag, const ebs::EventField& f, ebs::VisMode mode)
		{ return plot(tag, ebs::PlotEventField(f, mode)); }

		void display()
		{
			for(const auto& p : *items_.lock()) {
				QtDisplay(p.second.label, p.second.img);
			}
		}

	private:
		da::synchronized<std::map<std::string, Vis>> items_;
	};

}

#endif
