#ifndef INCLUDED_EBS_STEREOEVENT_HPP
#define INCLUDED_EBS_STEREOEVENT_HPP

#include <Eigen/Dense>
#include <Edvs/Event.hpp>

namespace ebs
{

/** An stereo data augmented event */ 
struct StereoEvent
: public Edvs::Event
{
	/** Gradient of tempo-spatial time field
	 * gradient = (fx,fy) corresponds to plane dt = fx*dx + fy*dy
	 * where (dx,dy) is the difference in pixel coordinates
	 * and dt is the corresponding time difference.
	 */
	Eigen::Vector2f gradient;

	/** Disparity from local patch-based matching */
	float disparity_local;

	/** Confidence of local patch-based matching */
	float confidence_local;

	/** Disparity after global optimization */
	float disparity;

	/** Indicates if the event is valid */
	bool valid;

	/** Ground truth depth */
	float depth_gt;

	/** Ground truth disparity */
	float disparity_gt;
};

}

#endif
