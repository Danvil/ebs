#ifndef INCLUDED_EBS_FLOW_HPP
#define INCLUDED_EBS_FLOW_HPP

#include <vector>
#include <iostream>
#include <Eigen/Dense>
#include <da/LookUp.hpp>
#include <slimage/image.hpp>
#include <Edvs/Event.hpp>
#include <ebs/EventField.hpp>
#include <ebs/WeightedPlaneFitLattice.hpp>

namespace ebs
{

inline
float Gauss(float e, float s)
{
	float q = e / s;
	return std::exp(-0.5f*q*q);
}

inline
float GaussSq(float e2, float s2)
{
	return std::exp(-0.5f*e2/s2);
}

inline
float MatrixMedian(Eigen::MatrixXf a) // by-value because we need a copy
{
	// TODO nth_element copies elements around which we do not need
	unsigned n = a.rows()*a.cols();
	std::nth_element(a.data(), a.data() + n/2, a.data() + n);
	return *(a.data() + n/2);
}

//#define EBS_DEBUG_FLOW 1

/** @brief Computes the event flow 
 * See documentation for details.
 */
inline
Eigen::Vector2f ComputeEventFlow(const Edvs::Event& e, const EventField& event_field, unsigned RAD, float SIGMA)
{
	// A lookup table to increase computation of Gaussian probabilities.
	static const auto GaussLookup = da::Gauss<float>();

	const int RADi = RAD;
	const unsigned SIZE = 2*RAD + 1;

	// If we are too close to the border we simply return 0.
	// TODO(davidw) Perhaps we can do better here?
	if(e.x < RAD || event_field.cols() <= e.x+RAD || e.y < RAD || event_field.rows() <= e.y+RAD) {
		return Eigen::Vector2f::Zero();
	}

	// Get the local patch in the event field.
	const auto src = event_field.patch(e.x, e.y, RAD);

	// Current event time (do not use e.t to get float instead of uint64_t).
	const float t0 = src(RAD,RAD).time;

	// Get a patch with time differences.
	const auto delta_time_patch = src.view([t0](const EventFieldElement& a) { return a.time - t0; });

	// Compute the median of all matrix elements.
	const auto delta_time_matrix = delta_time_patch.to_matrix();
	const float time_threshold = MatrixMedian(delta_time_matrix);

	// Compute 0/1 weights by selecting only pixels which are nearer in time than the threshold.
	// Eigen::MatrixXf weights{SIZE,SIZE};
	// for(int i=0; i<weights.rows(); i++) {
	// 	for(int j=0; j<weights.cols(); j++) {
	// 		weights(i,j) = (delta_time_patch(i,j) < time_threshold) ? 1.0f : 0.0f;
	// 	}
	// }

	// Compute weights using the already existing flow values.
 	Eigen::MatrixXf weights{SIZE,SIZE};
#ifdef EBS_DEBUG_FLOW
	Eigen::MatrixXf dbg1{SIZE,SIZE};
	Eigen::MatrixXf dbg2{SIZE,SIZE};
	Eigen::MatrixXf dbg3{SIZE,SIZE};
#endif
	iterate(src, [&weights,t0,RADi,SIGMA](int i, int j, const EventFieldElement& a) {
		Eigen::Vector2f dp{j-RADi,i-RADi};
		float dt = a.time - t0;
		float mu = a.flow.dot(dp);
#ifdef EBS_DEBUG_FLOW
		dbg3(i,j) = a.time;
		dbg1(i,j) = dt;
		dbg2(i,j) = mu;
#endif
		weights(i,j) = GaussLookup((dt - mu)/SIGMA);
	});

	// Compute weights by using the 50% nearest event times.
	// TODO this is a simple 50/50 selection - can we do better?
	// float mw = MatrixMedian(weights);
	// DEBUG_BOUND_CHECK {
	// 	std::cout << weights << std::endl;
	// 	std::cout << "MEDIAN = " << mw << std::endl;
	// }
	// for(int i=0; i<weights.rows(); i++) {
	// 	for(int j=0; j<weights.cols(); j++) {
	// 		float& w = weights(i,j);
	// 		w = w > mw ? 1 : 0;
	// 	}
	// }

	// Compute gradient of tempo-spatial height field (using weights).
	Eigen::Vector3f sol = WeightedPlaneFitLattice(delta_time_patch, weights);

	// The result of the plane fit are coefficients (a,b,c) of the plane equation a + b*x + c*y.
	// For the gradient we are only interest in the slopes b and c.
	return { sol[1], sol[2] };
}

}

#endif
