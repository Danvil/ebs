#ifndef INCLUDED_EBS_DISPARITY_HPP
#define INCLUDED_EBS_DISPARITY_HPP

#include <Edvs/Event.hpp>
#include <ebs/EventField.hpp>

namespace ebs
{

struct ParametersLocalDisparity
{
	// radius of local patch used for cross-correlation
	unsigned patch_rad = 3;
	
	// maximum searched disparity
	unsigned disp_max = 32;
	
	// rejects pixel with non-horizontal orientation
	// this is the cosine of the angle where 0 means horizontal and 1 means vertical (no limitation)
	double inclination_limit_cos = 0.30;
	
	// time tolerance for optical flow error
	double time_stddev = 0.05;
	
	// maximum error for optical flow (this is multiplied by time_stddev)
	double time_stddev_mult_max = 5.0;
	
	// probability that the parity of a pixel is wrong
	double parity_error_prop = 0.20;

	// percentage of the region with trash pixels (50% is reasonable for edges)
	double patch_trash_share = 0.50;
};

float ComputeDisparity(const Edvs::Event& e,
	const EventField& tf1, const EventField& tf2,
	const ParametersLocalDisparity& params,
	float* conf);

float ComputeDisparitySubpixel(const Edvs::Event& e,
	const EventField& tf1, const EventField& tf2,
	const ParametersLocalDisparity& params);

float ComputeDisparityGlobal(const Edvs::Event& e,
	const EventField& tf1,
	unsigned RAD);


}

#endif
