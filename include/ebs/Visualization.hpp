#ifndef INCLUDED_EBS_VIS_HPP
#define INCLUDED_EBS_VIS_HPP

#include "EventField.hpp"
#include "StereoEvent.hpp"
#include "Tools.hpp"
#include <Edvs/Event.hpp>
#include <slimage/image.hpp>
#include <Eigen/Dense>

namespace ebs
{

constexpr slimage::Pixel3ub BackgroundColor{128,128,128}; 

/** Applies color ramp to value */
template<bool WRAP, unsigned N>
Eigen::Vector3f ColorScheme(const Eigen::Vector3f colors[N], float a) {
	static_assert(N >= 2, "Number of colors must be at least 2");
	constexpr unsigned Q = WRAP ? N : N-1;
	const float v = static_cast<float>(Q) * std::min(std::max(0.0f, a), 1.0f);
	const unsigned i1 = std::min(static_cast<unsigned>(v), Q-1);
	const unsigned i2 = WRAP ? (i1 + 1) % N : i1 + 1;
	const float p = v - static_cast<float>(i1);
	return (1.0f-p)*colors[i1] + p*colors[i2];
}

/** Color scheme: blue - red */
inline
Eigen::Vector3f ColorSchemeBlueRed(float p)
{
	constexpr unsigned N = 2;
	static const Eigen::Vector3f colors[N] = {
		{0,0,1}, {1,0,0}
	};
	return ColorScheme<false,N>(colors, p);
}

/** Color scheme: red - yellow - green - cyan - blue - magenta - red */
inline
Eigen::Vector3f ColorSchemeHue(float p)
{
	constexpr unsigned N = 6;
	static const Eigen::Vector3f colors[N] = {
		{1,0,0}, {1,1,0},
		{0,1,0}, {0,1,1},
		{0,0,1}, {1,0,1}
	};
	return ColorScheme<true,N>(colors, p);
}

/** Color scheme: white -> black */
inline
Eigen::Vector3f ColorSchemeGreyscale(float p)
{
	constexpr unsigned N = 2;
	static const Eigen::Vector3f colors[N] = {
		{0,0,0}, {1,1,1}
	};
	return ColorScheme<false,2>(colors, p);
}

/** Color scheme: dark blue - dark green - dark yellow- dark red */
inline
Eigen::Vector3f ColorSchemeDarkRainbow(float p)
{
	constexpr unsigned N = 11;
	static const Eigen::Vector3f colors[N] = {
		{0.237736, 0.340215, 0.575113},
		{0.253651, 0.344893, 0.558151},
		{0.264425, 0.423024, 0.3849},
		{0.291469, 0.47717, 0.271411},
		{0.416394, 0.555345, 0.24182},
		{0.624866, 0.673302, 0.264296},
		{0.813033, 0.766292, 0.303458},
		{0.877875, 0.731045, 0.326896},
		{0.812807, 0.518694, 0.303459},
		{0.72987, 0.239399, 0.230961},
		{0.72987, 0.239399, 0.230961}};
	return ColorScheme<false,N>(colors, p);
}

/** Decays color to background color */
inline
Eigen::Vector3f Decay(const Eigen::Vector3f& a, float p)
{
	const Eigen::Vector3f target{
			static_cast<float>(BackgroundColor[0])/255.0f,
			static_cast<float>(BackgroundColor[1])/255.0f,
			static_cast<float>(BackgroundColor[2])/255.0f};
	return (1.0f-p)*a + p*target;
}

/** Converts Eigen::Vector3f to slimage::Pixel3ub */
inline
slimage::Pixel3ub Color(const Eigen::Vector3f& col)
{
	return slimage::Pixel3ub{
		static_cast<unsigned char>(255.0f*col[0]),
		static_cast<unsigned char>(255.0f*col[1]),
		static_cast<unsigned char>(255.0f*col[2])
	};
}

/** Plots events colored by CF which are selected by SF into an image */
template<typename Iterator, typename CF, typename SF>
slimage::Image3ub PlotEvents(
	Iterator begin, Iterator end,
	unsigned rows, unsigned cols,
	CF cf, SF sf,
	bool decay
) {
	slimage::Image3ub img{cols, rows, BackgroundColor};
	if(begin == end) {
		return img;
	}
	uint64_t t0 = begin->t;
	uint64_t t1 = std::prev(end)->t;
	uint64_t delta = DeltaT(t0, t1);
	for(auto it=begin; it!=end; ++it) {
		if(sf(*it)) {
			img(it->x, it->y) = Color(decay
				? Decay(cf(*it), 
					std::min(1.0f,static_cast<float>(DeltaT(it->t,t1))/static_cast<float>(delta)))
				: cf(*it)
			);
		}
	}
	img(0,0) = {255,0,0};
	img(127,0) = {0,255,0};
	return img;
}

inline
float Rescale(float a, float min, float max)
{
	return (a - min) / (max - min);
}

inline
Eigen::Vector3f ColorizeParity(bool a)
{
	return a ? Eigen::Vector3f{1,1,1} : Eigen::Vector3f{0,0,0};
}

inline
Eigen::Vector3f ColorizeTime(uint64_t t, uint64_t tnow)
{
	float p = std::exp(- static_cast<float>(DeltaT(t, tnow)) / 1000000.0f);
	return ColorSchemeGreyscale(p);
}

inline
Eigen::Vector3f ColorizeTime(float t, float tnow)
{
	float p = std::exp(- (tnow - t));
	return ColorSchemeGreyscale(p);
}

inline
Eigen::Vector3f ColorizeDisparity(float d)
{
	if(d == -1) {
		return {0,0,0};
	}
	if(d < -1) {
		return {1,0,1};
	}
	constexpr unsigned DISP_MAX = 32; // FIXME
	float p = d / static_cast<float>(DISP_MAX-1);
	return ColorSchemeDarkRainbow(p);
}

inline
Eigen::Vector3f ColorizeConfidence(float c)
{
	return ColorSchemeBlueRed(c);
}

inline
Eigen::Vector3f ColorizeGradient(const Eigen::Vector2f& a)
{
	// gradient angle normalized to [0,1]
	float dir = 0.5f*std::atan2(a[1], a[0]) / 3.1415f;
	if(dir < 0) dir += 1.0f;
	// gradient length normalized to [0,1]
	constexpr float SCL = 0.15f; // FIXME
	float len = std::min(1.0f, a.norm()/SCL);
	// hue to color
	Eigen::Vector3f c = ColorSchemeHue(dir);
	// len = 0 => white
	return len * c + (1.0f - len) * Eigen::Vector3f::Ones();
}

inline
Eigen::Vector3f ColorizeGradientAbs(const Eigen::Vector2f& a)
{
	constexpr float SCL = 0.15f; // FIXME
	return ColorSchemeBlueRed(a.norm()/SCL);
}

inline
Eigen::Vector3f ColorizeGradientAbsInv(const Eigen::Vector2f& a)
{
	constexpr float PX_MAX = 40; // FIXME
	float p = 1.0 / (PX_MAX*std::max(a.norm(),1.0f/PX_MAX));
	return ColorSchemeBlueRed(p);
}

inline
Eigen::Vector3f ColorizeGradientArg(const Eigen::Vector2f& a)
{
	float arg = 0.5f*std::atan2(a[1], a[0]) / 3.1415f;
	if(arg < 0) arg += 1.0f;
	return ColorSchemeHue(arg);
}

inline
Eigen::Vector3f ColorizeDepth(float a)
{
	constexpr float DEPTH_MIN = 0.5f;
	constexpr float DEPTH_MAX = 3.0f;
	if(a == 0.0f) {
		return Eigen::Vector3f::Zero();
	}
	return ColorSchemeDarkRainbow(Rescale(a, DEPTH_MIN, DEPTH_MAX));
}

template<uint8_t ID>
bool CheckEventId(const Edvs::Event& e)
{ return e.id == ID; }

template<typename Iterator>
slimage::Image3ub PlotEventsById(
	Iterator begin, Iterator end,
	unsigned rows, unsigned cols,
	uint8_t id, bool decay)
{
	return PlotEvents(
		begin, end,
		rows, cols,
		[](const Edvs::Event& e) { return ColorizeParity(e.parity); },
		[id](const Edvs::Event& e) { return e.id == id; },
		decay);
}

enum class VisMode
{
	Parity,
	Time,
	Gradient,
	GradientAbs,
	GradientAbsInv,
	GradientArg,
	DisparityLocal,
	ConfidenceLocal,
	DisparityGlobal,
	DepthGt
};

inline
bool CheckValid(const ebs::StereoEvent& e)
{ return e.valid; }

template<typename Iterator>
slimage::Image3ub PlotStereoEvents(
	Iterator begin, Iterator end,
	unsigned rows, unsigned cols,
	VisMode mode, bool decay
) {
	switch(mode) {
	case VisMode::Parity:
		return PlotEvents(begin, end, rows, cols,
			[](const Edvs::Event& e) { return ColorizeParity(e.parity); },
			CheckValid, decay);
	case VisMode::Time: {
		uint64_t tnow = begin == end ? 0 : std::prev(end)->t;
		return PlotEvents(begin, end, rows, cols,
			[tnow](const Edvs::Event& e) { return ColorizeTime(e.t, tnow); },
			CheckValid, false);
	}
	case VisMode::DisparityLocal:
		return PlotEvents(begin, end, rows, cols,
			[](const ebs::StereoEvent& a) { return ColorizeDisparity(a.disparity_local); },
			CheckValid, decay);
	case VisMode::DisparityGlobal:
		return PlotEvents(begin, end, rows, cols,
			[](const ebs::StereoEvent& a) { return ColorizeDisparity(a.disparity); },
			CheckValid, decay);
	case VisMode::ConfidenceLocal:
		return PlotEvents(begin, end, rows, cols,
			[](const ebs::StereoEvent& a) { return ColorizeConfidence(a.confidence_local); },
			CheckValid, decay);
	case VisMode::Gradient:
		return PlotEvents(begin, end, rows, cols,
			[](const ebs::StereoEvent& a) { return ColorizeGradient(a.gradient); },
			CheckValid, false);
	case VisMode::GradientAbs:
		return PlotEvents(begin, end, rows, cols,
			[](const ebs::StereoEvent& a) { return ColorizeGradientAbs(a.gradient); },
			CheckValid, decay);
	case VisMode::GradientAbsInv:
		return PlotEvents(begin, end, rows, cols,
			[](const ebs::StereoEvent& a) { return ColorizeGradientAbsInv(a.gradient); },
			CheckValid, decay);
	case VisMode::GradientArg:
		return PlotEvents(begin, end, rows, cols,
			[](const ebs::StereoEvent& a) { return ColorizeGradientArg(a.gradient); },
			CheckValid, decay);
	case VisMode::DepthGt:
		return PlotEvents(begin, end, rows, cols,
			[](const ebs::StereoEvent& a) { return ColorizeDepth(a.depth_gt); },
			CheckValid, decay);
	default:
		return {};
	}
}

template<typename CF>
slimage::Image3ub PlotEventField(
	const EventField& f,
	CF cf
) {
	slimage::Image3ub img{f.cols(), f.rows()};
	for(unsigned i=0; i<f.rows(); i++) {
		for(unsigned j=0; j<f.cols(); j++) {
			img(j,i) = Color(cf(f.at(j,i)));
		}
	}
	return img;
}

inline
slimage::Image3ub PlotEventField(
	const EventField& f,
	VisMode mode
) {
	switch(mode) {
	case VisMode::Parity: {
		return PlotEventField(f,
			[](const EventFieldElement& a) { return ColorizeParity(a.parity); });
	}
	case VisMode::Time: {
		float tnow = f.at(f.last_.first,f.last_.second).time;
		return PlotEventField(f,
			[tnow](const EventFieldElement& a) { return ColorizeTime(a.time, tnow); });
	}
	case VisMode::DisparityLocal: {
		return PlotEventField(f,
			[](const EventFieldElement& a) { return ColorizeDisparity(a.disparity_local); });
	}
	case VisMode::DisparityGlobal: {
		return PlotEventField(f,
			[](const EventFieldElement& a) { return ColorizeDisparity(a.disparity); });
	}
	case VisMode::ConfidenceLocal: {
		return PlotEventField(f,
			[](const EventFieldElement& a) { return ColorizeConfidence(a.confidence_local); });
	}
	case VisMode::Gradient: {
		return PlotEventField(f,
			[](const EventFieldElement& a) { return ColorizeGradient(a.flow); });
	}
	case VisMode::GradientAbs: {
		return PlotEventField(f,
			[](const EventFieldElement& a) { return ColorizeGradientAbs(a.flow); });
	}
	case VisMode::GradientAbsInv: {
		return PlotEventField(f,
			[](const EventFieldElement& a) { return ColorizeGradientAbsInv(a.flow); });
	}
	case VisMode::GradientArg: {
		return PlotEventField(f,
			[](const EventFieldElement& a) { return ColorizeGradientArg(a.flow); });
	}
	default:
		return slimage::Image3ub{};
	}
}

inline
slimage::Image3ub VisualizeDepthImage(const slimage::Image1f& imgd)
{
	slimage::Image3ub imgcol{imgd.width(), imgd.height()};
	for(size_t i=0; i<imgd.size(); i++) {
		imgcol[i] = Color(ColorizeDepth(imgd[i]));
	}
	return imgcol;
}

inline
slimage::Image3ub VisualizeDisparityImage(const slimage::Image1f& disp)
{
	slimage::Image3ub imgcol{disp.width(), disp.height()};
	for(size_t i=0; i<disp.size(); i++) {
		imgcol[i] = Color(ColorizeDisparity(disp[i]));
	}
	return imgcol;
}


}

#endif
