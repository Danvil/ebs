#ifndef INCLUDED_EBS_CORE_HPP
#define INCLUDED_EBS_CORE_HPP

#include <ebs/StereoEvent.hpp>
#include <ebs/EventField.hpp>
#include <ebs/Disparity.hpp>
#include <da/Properties.hpp>
#include <vector>

namespace ebs
{

enum class Option
{
	GradientPatchRadius,
	GradientTimeSigma,

	LocalDisparityPatchRadius,
	LocalDisparityMax,
	LocalDisparityInclinationLimitCos,
	LocalDisparityTimeStddev,
	LocalDisparityTimeStddevMultMax,
	LocalDisparityParityErrorProp,
	LocalDisparityPatchTrashShare,

	DisparityGlobalPatchRadius
};

struct Properties
{
	int GradientPatchRadius = 3;
	double GradientTimeSigma = 0.1;

	ParametersLocalDisparity p_local_disp;

	int DisparityGlobalPatchRadius = 5;
};

Properties DefaultProperties();

da::PropertyTranslator<Option,Properties> CreatePropertyTranslator();

class Core
{
public:
	typedef std::function<void(std::vector<StereoEvent>&)> DepthGroundTruthFnc;

	Core(int idl=0, int idr=1);

	void SetDisparityComputation(bool a)
	{ is_disparity_computation_ = a; }

	void SetDepthGroundTruthCallback(DepthGroundTruthFnc f)
	{ depth_gt_fnc_ = f; }

	void SetEbsEventTransformation(bool x)
	{ ebs_event_transf_ = x; }

	void pushEvents(const std::vector<Edvs::Event>& events);

	const Properties& properties() const
	{ return properties_; }

	Properties& properties()
	{ return properties_; }

	const std::vector<Edvs::Event>& events() const
	{ return events_; }

	const std::vector<StereoEvent>& stereoEvents() const
	{ return stereo_events_; }

	const EventField& eventFieldLeft() const
	{ return tf_[0]; }

	const EventField& eventFieldRight() const
	{ return tf_[1]; }

private:
	bool ebs_event_transf_;
	bool is_disparity_computation_;

	DepthGroundTruthFnc depth_gt_fnc_;

	int id_left_;
	int id_right_;

	EventField tf_[2];

	std::vector<Edvs::Event> events_;

	std::vector<StereoEvent> stereo_events_;

	Properties properties_;

};

}

#endif
