#ifndef INCLUDED_EBS_TOOLS_HPP
#define INCLUDED_EBS_TOOLS_HPP

namespace ebs
{

	/** returns max(0,b-a) */
	inline
	uint64_t DeltaT(uint64_t a, uint64_t b)
	{ return std::max<int64_t>(0, static_cast<int64_t>(b) - static_cast<int64_t>(a)); }

	template<typename E>
	typename std::vector<E>::const_iterator FindEvent(const std::vector<E>& v, uint64_t delta)
	{
		if(v.empty()) {
			return v.end();
		}
		uint64_t t = DeltaT(delta, v.back().t);
		return std::upper_bound(v.begin(), v.end(), t,
			[](uint64_t t, const E& e) { return t < e.t; });
	}

	template<typename E>
	std::vector<E> CloneEventRange(const std::vector<E>& v, uint64_t delta)
	{ return std::vector<E>{ebs::FindEvent(v, delta), v.end()}; }


}

#endif
