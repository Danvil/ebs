#ifndef INCLUDED_EBS_EVENTFIELD_HPP
#define INCLUDED_EBS_EVENTFIELD_HPP

#include <cstdint>
#include <functional>
#include <Eigen/Dense>
#include <da/table.hpp>

namespace ebs
{

struct EventFieldElement
{
	float time;
	uint32_t parity;
	Eigen::Vector2f flow;
	float disparity_local;
	float confidence_local;
	float disparity;
};

constexpr unsigned ROWS = 128;
constexpr unsigned COLS = 128;

struct EventField 
{
	Eigen::Matrix<float,Eigen::Dynamic,Eigen::Dynamic,Eigen::RowMajor> ef_time_;
	Eigen::Matrix<float,Eigen::Dynamic,Eigen::Dynamic,Eigen::RowMajor> ef_parity_;
	da::Table<EventFieldElement> ef_ext_;

	std::pair<unsigned,unsigned> last_;

	std::size_t num_events_;

	EventField()
	: ef_time_(ROWS,COLS), ef_ext_(ROWS,COLS), num_events_(0) {}

	unsigned rows() const { return ROWS; }
	unsigned cols() const { return COLS; }

	template<typename E>
	void push(const E& e) {
		float tt = static_cast<double>(e.t) / static_cast<double>(1000000.0);
		ef_time_(e.y,e.x) = tt;
		ef_ext_(e.y,e.x) = { tt, e.parity, Eigen::Vector2f::Zero(), -1, 0, -1};
		last_ = {e.x, e.y};
		num_events_++;
	}

	const EventFieldElement& at(int x, int y) const
	{ return ef_ext_(y,x); }

	EventFieldElement& at(int x, int y)
	{ return ef_ext_(y,x); }

	const EventFieldElement& operator()(int x, int y) const
	{ return at(x,y); }

	EventFieldElement& operator()(int x, int y)
	{ return at(x,y); }

	const EventFieldElement& last() const
	{ return at(last_.first, last_.second); }

	EventFieldElement& last()
	{ return at(last_.first, last_.second); }

	auto patchTime(int x, int y, unsigned r) const
	-> decltype(ef_time_.block(0,0,0,0))
	{ return ef_time_.block(y-r,x-r,2*r+1,2*r+1); }
	
	auto patchParity(int x, int y, unsigned r) const
	-> decltype(ef_parity_.block(0,0,0,0))
	{ return ef_parity_.block(y-r,x-r,2*r+1,2*r+1); }
	
	auto patch(int x, int y, unsigned r) const
	-> decltype(ef_ext_.patch(0,0,0,0))
	{ return ef_ext_.patch(y-r,x-r,2*r+1,2*r+1); }
};

}

#endif
