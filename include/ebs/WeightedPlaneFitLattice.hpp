#ifndef INCLUDED_EBS_PLANEFIT_HPP
#define INCLUDED_EBS_PLANEFIT_HPP

#include <Eigen/Dense>

namespace ebs
{

/** @brief Fits a plane through a weighted height map
 * See documentation for details.
 */
template<typename MV, typename MW>
Eigen::Vector3f WeightedPlaneFitLattice(const MV& mv, const MW& mw)
{
	// Construct the linear system of equations.
	const int rows = mv.rows();
	const int cols = mv.cols();
	float sw=0, swx=0, swy=0, swxx=0, swxy=0, swyy=0, swt=0, swtx=0, swty=0;
	for(int i=0; i<rows; i++) {
		for(int j=0; j<cols; j++) {
			const float w = mw(i,j);
			const float x = static_cast<float>(j - cols/2);
			const float y = static_cast<float>(i - rows/2);
			const float t = mv(i,j);
			sw += w;
			swx += w*x;
			swy += w*y;
			swxx += w*x*x;
			swxy += w*x*y;
			swyy += w*y*y;
			swt += w*t;
			swtx += w*t*x;
			swty += w*t*y;
		}
	}
	Eigen::Matrix3f A;
	A << sw , swx , swy ,
	     swx, swxx, swxy,
	     swy, swxy, swyy;
	Eigen::Vector3f b;
	b << swt, swtx, swty;
	// Solve the linear system.
	Eigen::Vector3f sol = A.colPivHouseholderQr().solve(b);
	// There might be a degenerate condition which we reach for example when all weights are 0.
	// TODO(davidw) Investigate further under which conditions this might fail.
	if(std::isnan(sol[0]) || std::isnan(sol[1]) || std::isnan(sol[2])) {
		std::cerr << "Warning: WeightedPlaneFitLattice degenerated!" << std::endl;
		return Eigen::Vector3f::Zero();
	}
	return sol;
}

}

#endif
