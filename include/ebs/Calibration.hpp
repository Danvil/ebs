#ifndef INCLUDED_EBS_CALIBRATION_HPP
#define INCLUDED_EBS_CALIBRATION_HPP

#include <vector>
#include <Eigen/Dense>
#include <slimage/image.hpp>
#include <ebs/EventField.hpp>
#include <ebs/StereoEvent.hpp>
#include <Edvs/Event.hpp>

namespace ebs
{

	struct Checkerboard
	{
		std::vector<Eigen::Vector2f> corners;
		unsigned rows = 0, cols = 0;
		unsigned width = 0, height = 0;
		bool success = false;
	};


	struct CheckerboardEvent : public Checkerboard
	{
		float angle = 0.0f;
		Eigen::MatrixXf dbg_cme;
		std::vector<Eigen::MatrixXf> dbg;

		std::vector<float> corner_angles;
		std::vector<std::vector<unsigned>> corner_neighbours;
		std::vector<unsigned> sorted;
	};

	slimage::Image3ub PlotCheckerboard(const CheckerboardEvent& cb);
	void PlotCheckerboard(slimage::Image3ub& img, const ebs::CheckerboardEvent& cb);
	slimage::Image3ub PlotCheckerboardCME(const CheckerboardEvent& cb);


	CheckerboardEvent DetectCheckerboardEvent(const EventField& u, bool debug=false);

	struct CheckerboardDense : public Checkerboard
	{
		slimage::Image3ub raw;
	};

	CheckerboardDense DetectCheckerboardDense(const slimage::Image3ub& u);

	slimage::Image3ub PlotCheckerboard(const CheckerboardDense& cb);


	Eigen::Matrix3d CreateCameraMatrix(unsigned w, unsigned h, float focal);


	struct CameraCalibration
	{
		Eigen::Matrix3d intrinsic;
		Eigen::VectorXd distortion;
	};

	CameraCalibration CalibrateCamera(const std::vector<Checkerboard>& vcb, float focal_guess);


	struct StereoCalibration
	{
		Eigen::Matrix3d R;
		Eigen::Vector3d T;
		Eigen::Matrix3d E;
		Eigen::Matrix3d F;
	};

	StereoCalibration CalibrateStereo(
		const std::vector<Checkerboard>& vcb1, const CameraCalibration& cc1,
		const std::vector<Checkerboard>& vcb2, const CameraCalibration& cc2
	);


	struct CalibrationFrame
	{
		Checkerboard dvs1;
		Checkerboard dvs2;
		Checkerboard kinect;
	};

	struct CalibrationData
	{
		std::vector<CalibrationFrame> frames;
	};

	struct Calibration
	{
		CameraCalibration dvs1;
		CameraCalibration dvs2;
		CameraCalibration kinect;
		StereoCalibration kinect_to_dvs1;
		StereoCalibration kinect_to_dvs2;
		StereoCalibration dvs1_to_dvs2;
	};

	inline
	std::ostream& operator<<(std::ostream& os, const CameraCalibration& cc)
	{
		os << "[i=" << cc.intrinsic << ",\nd=" << cc.distortion.transpose() << "]\n";
		return os;
	}

	inline
	std::ostream& operator<<(std::ostream& os, const StereoCalibration& sc)
	{
		os << "[R=" << sc.R << "\n";
		os << " T=" << sc.T.transpose() << "\n";
		os << " E=" << sc.E << "\n";
		os << " F=" << sc.F << "\n]";
		return os;
	}

	inline
	std::ostream& operator<<(std::ostream& os, const Calibration& c)
	{
		os << "Calibration:\n";
		os << "dvs1:\n" << c.dvs1;
		os << "dvs1:\n" << c.dvs2;
		os << "kinect:\n" << c.kinect;
		os << "kinect_to_dvs1:\n" << c.kinect_to_dvs1;
		os << "kinect_to_dvs2:\n" << c.kinect_to_dvs2;
		os << "dvs1_to_dvs2:\n" << c.dvs1_to_dvs2;
		return os;
	}

	void Save(const CameraCalibration& calib, const std::string& filename);
	void Load(const std::string& filename, CameraCalibration& calib);

	void Save(const Calibration& calib, const std::string& filename);
	void Load(const std::string& filename, Calibration& calib);

	Calibration Calibrate(const CalibrationData& data);


	Eigen::Vector3f Triangulize(const ebs::StereoEvent& a);


	void Undistort(std::vector<Edvs::Event>& events, const std::vector<CameraCalibration>& cams,
		bool randomize=true,
		std::vector<Eigen::Vector2f>* coordsf = 0);


	slimage::Image1f ReprojectDepth(const slimage::Image1ui16& depth,
		const ebs::CameraCalibration& calib_kinect, const ebs::CameraCalibration& calib_dvs, const ebs::StereoCalibration& calib_kinect_to_dvs);

	std::vector<slimage::Image1f> DepthToDisparity(const slimage::Image1ui16& depth, const Calibration& calib);

	template<typename E, int R=1>
	float LookUpDepth(const E& e, const slimage::Image1f& depth)
	{
		int x1 = std::max(static_cast<int>(e.x) - R, 0);
		int x2 = std::min(static_cast<int>(e.x) + R, 127);
		int y1 = std::max(static_cast<int>(e.y) - R, 0);
		int y2 = std::min(static_cast<int>(e.y) + R, 127);
		float res = 0; // TODO
		for(int x=x1; x<=x2; x++) {
			for(int y=y1; y<=y2; y++) {
				float q = depth(x,y);
				if(res == 0 || (0 < q && q < res)) {
					res = q;
				}
			}
		}
		return res;
	}

	template<typename E, int R=1>
	float LookUpDisparity(const E& e, const slimage::Image1f& disparity)
	{
		int x1 = std::max(static_cast<int>(e.x) - R, 0);
		int x2 = std::min(static_cast<int>(e.x) + R, 127);
		int y1 = std::max(static_cast<int>(e.y) - R, 0);
		int y2 = std::min(static_cast<int>(e.y) + R, 127);
		float res = -1; // TODO
		for(int x=x1; x<=x2; x++) {
			for(int y=y1; y<=y2; y++) {
				res = std::max<float>(res, disparity(x,y));
			}
		}
		return res;
	}

}

#endif
