#ifndef INCLUDED_QS_PIPELINE_HPP
#define INCLUDED_QS_PIPELINE_HPP

#include <boost/signals2/signal.hpp>
#include <vector>
#include <functional>
#include <thread>

namespace qs {

template<typename T>
class Source
{
public:
	virtual ~Source() {}

	const T& current() const
	{ return current_; }

	void setCurrent(const T& c)
	{
		current_ = c;
		on_value_changed_();
	}

	template<typename S>
	void connect(S&& s)
	{ on_value_changed_.connect(std::forward<S>(s)); }

private:
	T current_;
	boost::signals2::signal<void()> on_value_changed_;

};

template<typename T>
class Thread
{
public:
	Thread()
	{

	}

private:
	std::thread t_;
};

}

#endif 