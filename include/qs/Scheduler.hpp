#ifndef INCLUDED_QS_SCHEDULER_HPP
#define INCLUDED_QS_SCHEDULER_HPP

#include <boost/any.hpp>
#include <da/synchronized.hpp>
#include <string>
#include <vector>
#include <map>
#include <atomic>
#include <future>
#include <thread>

namespace qs {

class Database
{
public:

	template<typename T>
	da::locked_ptr<T> at(const std::string& tag)
	{
		return (*data_.lock())[tag].lock_cast<T>([](boost::any* x) { return boost::any_cast<T>(x); });
	}

	template<typename T>
	da::locked_ptr<const T> at(const std::string& tag) const
	{
		if(!has(tag)) {
			std::cerr << "Invalid TAG: " << tag << std::endl;
		}
		return data_.lock()->at(tag).lock_cast<T>([](const boost::any* x) { return boost::any_cast<T>(x); });
	}

	template<typename T>
	void set(const std::string& tag, const T& value)
	{
		*raw(tag) = boost::any{value};
	}

	template<typename T>
	void get(const std::string& tag, T& value)
	{
		value = *at<T>(tag);
	}

	da::locked_ptr<boost::any> raw(const std::string& tag)
	{
		return (*data_.lock())[tag].lock();
	}

	da::locked_ptr<const boost::any> raw(const std::string& tag) const
	{
		if(!has(tag)) {
			std::cerr << "Invalid TAG: " << tag << std::endl;
		}
		return data_.lock()->at(tag).lock();
	}

	bool has(const std::string& tag) const
	{
		auto p = data_.lock();
		return p->find(tag) != p->end();
	}

private:
	da::synchronized<std::map<std::string,da::synchronized<boost::any>>> data_;
};

template<typename F>
double clock(F f)
{
	std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	f();
	std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
	uint64_t dt = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
	return static_cast<double>(dt) / 1000000.0;
}

class Scheduler
{
private:
	struct Node
	{
		std::string tag;
		std::atomic<uint64_t> ts_last_success;
		std::atomic<uint64_t> ts_last;
		bool run_in_thread;
		std::function<bool(Database& db)> action;
		bool running = false;
	};

	struct Task
	{
		uint64_t last;
		uint64_t dt;
		std::string source;
		std::string target;
	};

public:
	Scheduler()
	{ }

	~Scheduler()
	{
		for(const auto& p : threads_) {
			p.wait();
		}
	}

	Database& db()
	{ return db_; }

	const Database& db() const
	{ return db_; }

	void addNode(const std::string& tag, std::function<bool(Database& db)> action, bool threaded=false)
	{
		std::shared_ptr<Node> n{new Node{}};
		n->tag = tag;
		n->ts_last = 0;
		n->ts_last_success = 0;
		n->run_in_thread = threaded;
		n->action = action;
		nodes_[tag] = n;
	}

	template<typename T>
	void addNode(const std::string& tag, T&& initial, std::function<bool(Database& db)> action, bool threaded=false)
	{
		db_.set(tag, std::forward<T>(initial));
		addNode(tag, action, threaded);
	}

	void addReactionTask(const std::string& dst, const std::string& src, uint64_t delay_ms=0)
	{
		Task t;
		t.last = 0;
		t.dt = 1000*delay_ms;
		t.source = src;
		t.target = dst;
		tasks_.push_back(t);
	}

	void addRegularTask(const std::string& dst, uint64_t interval_ms=0)
	{
		Task t;
		t.last = 0;
		t.dt = 1000*interval_ms;
		t.source = dst;
		t.target = dst;
		tasks_.push_back(t);
	}

	void tick()
	{
		// clear threads
		for(auto it=threads_.begin(); it!=threads_.end(); ) {
			if(it->wait_for(std::chrono::milliseconds(0)) == std::future_status::ready) {
				it = threads_.erase(it);
			}
			else {
				++it;
			}
		}

		for(Task& t : tasks_) {
			if(nodes_.find(t.source) == nodes_.end()) {
				continue;
			}
			if(nodes_.find(t.target) == nodes_.end()) {
				continue;
			}
			// if(t.target == "checkerboard")
			// std::cout << now() << ": " << t.source << ", " << t.last << ", " << t.dt << std::endl;
			if(t.source == t.target) {
				if(!nodes_.at(t.source)->running && now() > t.last + t.dt) {
					t.last = now();
					run(t.target);
				}
			}
			else {
				uint64_t tt = nodes_.at(t.source)->ts_last_success + t.dt;
				if(t.last <= tt && now() > tt) {
					t.last = now();
					run(t.target);
				}
			}
		}
	}

	void run(const std::string& tag)
	{
//		std::cout << "RUNNING " << tag << std::endl;
		auto node = nodes_.at(tag);
		node->running = true;
		if(node->run_in_thread) {
			threads_.emplace_back(
				std::async(std::launch::async,
					[this,node] { runImpl(node); } ));
		}
		else {
			runImpl(node);
		}
	}

private:
	uint64_t now()
	{
		return
			std::chrono::duration_cast<std::chrono::microseconds>(
				std::chrono::system_clock::now()
				.time_since_epoch()
			).count();
	}

	void runImpl(std::shared_ptr<Node> node)
	{
		double dt = clock([&node,this]() {
			bool success = node->action(db_);
			uint64_t ts = now();
			if(success) {
				node->ts_last_success = ts;
			}
			node->ts_last = ts;
		});
		double dt_ms = static_cast<unsigned>(dt*10000.0)/10.0;
		if(dt_ms >= 100.0) {
			std::cout << "QS: Slow task: '" << node->tag << "' finished in " << dt_ms << "ms" << std::endl;
		}
		node->running = false;
	}

private:
	Database db_;
	std::map<std::string,std::shared_ptr<Node>> nodes_;
	std::vector<Task> tasks_;
	std::list<std::future<void>> threads_;
};

}

#endif
