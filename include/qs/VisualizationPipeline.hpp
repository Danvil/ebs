#ifndef INCLUDED_VISUALIZATIONPIPELINE_HPP
#define INCLUDED_VISUALIZATIONPIPELINE_HPP

#include <ebs/Core.hpp>
#include <da/synchronized.hpp>
#include <QImage>
#include <string>
#include <vector>
#include <functional>
#include <thread>
#include <atomic>
#include <mutex>

class VisualizationPipeline
{
public:
	struct Visualization
	{
		std::string tag;
		std::function<QImage(const da::locked_ptr<ebs::Core>&)> create_fnc;
		std::function<void(const QImage&)> display_fnc;
	};

private:
	struct Item : public Visualization
	{
		QImage current;
	};

	std::vector<Item> vis_;

	std::function<da::locked_ptr<ebs::Core>()> aquire_core_;

	std::thread images_update_thread_;
	std::atomic<bool> images_update_new_;
	std::atomic<bool> images_update_running_;

public:
	VisualizationPipeline(const std::vector<Visualization>& vis, std::function<da::locked_ptr<ebs::Core>()> f)
	: aquire_core_(f)
	{
		for(const auto& v : vis) {
			Item it;
			static_cast<Visualization&>(it) = v;
			vis_.push_back(it);
		}
		images_update_running_.store(true);
		images_update_new_.store(false);
		images_update_thread_ = std::thread(&VisualizationPipeline::run, this);
	}

	~VisualizationPipeline()
	{
		images_update_running_.store(false);
		images_update_thread_.join();
	}

	void display()
	{
		if(images_update_new_.load()) {
			for(const auto& p : vis_) {
				p.display_fnc(p.current);
			}
			images_update_new_.store(false);
		}
	}

private:
	void run()
	{
		while(images_update_running_.load()) {
			if(!images_update_new_.load()) {
				for(auto& p : vis_) {
					p.current = p.create_fnc(aquire_core_());
				}
				images_update_new_.store(true);
			}
		}
	}

};

#endif
