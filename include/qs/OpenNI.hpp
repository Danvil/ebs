#ifndef INCLUDED_QS_OPENNI_HPP
#define INCLUDED_QS_OPENNI_HPP

#include <slimage/image.hpp>
#include <OpenNI.h>

namespace qs {

class OpenNISource
{
private:
	openni::Device device;
	openni::VideoStream depth, color;
	openni::VideoFrameRef depthFrame;

public:
	slimage::Image3ub color_img;
	bool color_changed;
	slimage::Image1ui16 depth_img;
	bool depth_changed;

public:
	OpenNISource();

	~OpenNISource();

	void tick();

private:
	void forwardDepth(const openni::VideoFrameRef& frame);

	void forwardColor(const openni::VideoFrameRef& frame);

};

}

#endif
